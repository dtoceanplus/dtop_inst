@echo off
setlocal enableextensions
set TERM=

set inst_dir=%cd:\=/%
pushd "%~dp0"
chdir cygwin64/bin
bash --login -i -c "cd \"%inst_dir%\"; exec bash"
