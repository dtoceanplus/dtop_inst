#!/bin/bash
#
# Script Name: cleanup_unused_containers.sh
#
# Description:
#  The auxiliary script removes unused docker containers

# Run Information:
#  check that the file has executable permissions.
#  if no, set it
#  chmod +x ./cleanup_unused_containers.sh

echo "Cleaning / removing unused docker containers ... "

docker container rm $(docker container ls --filter "status=exited" -aq)
