#!/bin/bash
#
# Script Name: dtop_inst.sh
# Version: 0.1
# TermsOfService: 'https://www.dtoceanplus.eu/'
#
# Description:
#  The script provides Installation of DTOceanPlus (DTOP)
#  It uses the variables and functions of "./dtop_basic.sh".
#  The environment variables are read from "./dtop_inst.env".
#
# Run Information:
#  check that the file has executable permissions.
#  if no, set it
#  chmod +x ./dtop_inst.sh
#

# ------------------------------------------
# Set variables for installation directory.
# ------------------------------------------

__dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

INST_ROOT="${__dir}"

echo ""
echo INST_ROOT == "${INST_ROOT}"

source "${INST_ROOT}"/dtop_basic.sh

START_TIME=$(date +%s)

echo ""
echo "---------------------------------------------"
echo " DTOceanPlus (DTOP) installation log start :"

os=$(uname)
cygwin_substr='CYGWIN'

if [[ "$os" == *"$cygwin_substr"* ]]; then
  echo " DTOP installation is running on Windows"
  flag=n
else
  echo " DTOP installation is running on ${os}"
  flag=c
fi

echo ""

ping -${flag} 1 8.8.8.8 > /dev/null 2>&1

if [ $? -eq 1 ]; then
    echo " NOK - it looks like internet is not available right now";
    echo " please check it and re-try";
    exit 1
else
    echo " OK - it looks like the internet is available now";
fi


echo " Installation directory - ${INST_ROOT}"
echo ""

step_1

step_2

step_3

step_4

step_5

"${INST_ROOT}"/cleanup_unused_containers.sh

echo "---------------------------------------------"
echo " DTOceanPlus (DTOP) installation log finish. "
echo "---------------------------------------------"

END_TIME=$(date +%s)

echo " Installation process took $(($END_TIME - $START_TIME)) seconds"
echo ""
