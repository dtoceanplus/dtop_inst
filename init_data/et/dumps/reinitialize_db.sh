#/bin/bash

# Description:
#  The script reinitializes ET database
#  in running docker stack service container

#
# Run Information:
#  chmod u+x ./reinitialize_db.sh
#  ./reinitialize_db.sh

set -e

db_dump="et.db_initial"

if [ -f "${db_dump}" ]; then
    echo "  OK - copy file ${db_dump} exists."
else
    echo "  NOK - copy file ${db_dump} doesn't exist."
    exit 1
fi

echo ""

docker exec $(docker ps -q -f name=et_backend) rm /app/src/dtop_energytransf/Databases/ET_DB_test.db

docker cp ${db_dump} $(docker ps -q -f name=et_backend):/app/src/dtop_energytransf/Databases/ET_DB_test.db

echo "  The ET database is reinitialized using initial ${db_dump}"
