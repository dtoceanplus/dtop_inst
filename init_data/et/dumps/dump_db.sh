#/bin/bash

# Description:
#  The script makes a copy of ET database
#  in running docker stack service container

#
# Run Information:
#  chmod u+x ./dump_db.sh
#  ./dump_db.sh

set -e

docker cp $(docker ps -q -f name=et_backend):/app/src/dtop_energytransf/Databases/ET_DB_test.db ./et.db_$(date +_%Y_%m_%d_%H_%M_%S)

echo "  The ET database is copied to ./et.db_$(date +_%Y_%m_%d_%H_%M_%S)"
