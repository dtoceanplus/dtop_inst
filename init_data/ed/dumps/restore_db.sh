#/bin/bash

# Description:
#  The script restores ED database from a copy file
#  in running docker stack service container
#
# Run Information:
#  chmod u+x ./restore_db.sh
#  ./restore_db.sh

set -e

usage () {
    cat <<HELP_USAGE

    The script restores ED database from <copy file>.

    Usage :   $0 <copy file>

    example : $0 energydeliv.db__2021_05_23_06_03_38

HELP_USAGE
}

if [ $# -ne 1 ]
then
    usage
    exit 1
fi

if [[ ( $# == "--help") ||  $# == "-h" ]]
then
    usage
    exit 0
fi


db_dump="$1"

if [ -f "${db_dump}" ]; then
    echo "  OK - copy file ${db_dump} exists."
else
    echo "  NOK - copy file ${db_dump} doesn't exist."
    exit 1
fi

echo ""

docker exec $(docker ps -q -f name=ed_backend) rm /app/instance/energydeliv.db

docker cp ${db_dump} $(docker ps -q -f name=ed_backend):/app/instance/energydeliv.db

echo "  The ED database is restored using copy file - ${db_dump}"
