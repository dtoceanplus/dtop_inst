#/bin/bash

# Description:
#  The script makes a copy of ED database
#  in running docker stack service container

#
# Run Information:
#  chmod u+x ./dump_db.sh
#  ./dump_db.sh

set -e

docker cp $(docker ps -q -f name=ed_backend):/app/instance/energydeliv.db ./energydeliv.db_$(date +_%Y_%m_%d_%H_%M_%S)

echo "  The ED database is copied to ./energydeliv.db_$(date +_%Y_%m_%d_%H_%M_%S)"
