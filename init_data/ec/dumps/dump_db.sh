#/bin/bash

# Description:
#  The script makes a copy of EC database
#  in running docker stack service container

#
# Run Information:
#  chmod u+x ./dump_db.sh
#  ./dump_db.sh

set -e

docker cp $(docker ps -q -f name=ec_backend):/app/src/instance/energycapt.db ./energycapt.db_$(date +_%Y_%m_%d_%H_%M_%S)

echo "  The EC database is copied to ./energycapt.db_$(date +_%Y_%m_%d_%H_%M_%S)"
