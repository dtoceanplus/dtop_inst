#/bin/bash

# Description:
#  The script reinitializes SG database
#  in running docker stack service container

#
# Run Information:
#  chmod u+x ./reinitialize_db.sh
#  ./reinitialize_db.sh

set -e

db_dump="stagegate.db_initial"

if [ -f "${db_dump}" ]; then
    echo "  OK - copy file ${db_dump} exists."
else
    echo "  NOK - copy file ${db_dump} doesn't exist."
    exit 1
fi

echo ""

docker exec $(docker ps -q -f name=sg_backend) rm /app/src/instance/stagegate.db

docker cp ${db_dump} $(docker ps -q -f name=sg_backend):/app/src/instance/stagegate.db

echo "  The SG database is reinitialized using initial ${db_dump}"
