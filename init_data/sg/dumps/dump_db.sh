#/bin/bash

# Description:
#  The script makes a copy of SG database
#  in running docker stack service container

#
# Run Information:
#  chmod u+x ./dump_db.sh
#  ./dump_db.sh

set -e

docker cp $(docker ps -q -f name=sg_backend):/app/src/instance/stagegate.db ./stagegate.db_$(date +_%Y_%m_%d_%H_%M_%S)

echo "  The SG database is copied to ./stagegate.db_$(date +_%Y_%m_%d_%H_%M_%S)"
