#/bin/bash

# Description:
#  The script makes a copy of SLC database
#  in running docker stack service container

#
# Run Information:
#  chmod u+x ./dump_db.sh
#  ./dump_db.sh

set -e

docker cp $(docker ps -q -f name=slc_backend):/app/data/slc.db ./slc.db_$(date +_%Y_%m_%d_%H_%M_%S)

echo "  The SLC database is copied to ./slc.db_$(date +_%Y_%m_%d_%H_%M_%S)"
