#/bin/bash

# Description:
#  The script reinitializes SLC database
#  in running docker stack service container

#
# Run Information:
#  chmod u+x ./reinitialize_db.sh
#  ./reinitialize_db.sh

set -e

db_dump="slc.db_initial"

if [ -f "${db_dump}" ]; then
    echo "  OK - copy file ${db_dump} exists."
else
    echo "  NOK - copy file ${db_dump} doesn't exist."
    exit 1
fi

echo ""

docker exec $(docker ps -q -f name=slc_backend) rm /app/data/slc.db

docker cp ${db_dump} $(docker ps -q -f name=slc_backend):/app/data/slc.db

echo "  The SLC database is reinitialized using initial ${db_dump}"
