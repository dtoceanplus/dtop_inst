#/bin/bash

# Description:
#  The script restores SLC database from a copy file
#  in running docker stack service container
#
# Run Information:
#  chmod u+x ./restore_db.sh
#  ./restore_db.sh

set -e

usage () {
    cat <<HELP_USAGE

    The script restores SLC database from <copy file>.

    Usage :   $0 <copy file>

    example : $0 slc.db__2021_05_23_06_03_38

HELP_USAGE
}

if [ $# -ne 1 ]
then
    usage
    exit 1
fi

if [[ ( $# == "--help") ||  $# == "-h" ]]
then
    usage
    exit 0
fi


db_dump="$1"

if [ -f "${db_dump}" ]; then
    echo "  OK - copy file ${db_dump} exists."
else
    echo "  NOK - copy file ${db_dump} doesn't exist."
    exit 1
fi

echo ""

docker exec $(docker ps -q -f name=slc_backend) rm /app/data/slc.db

docker cp ${db_dump} $(docker ps -q -f name=slc_backend):/app/data/slc.db

echo "  The SLC database is restored using copy file - ${db_dump}"
