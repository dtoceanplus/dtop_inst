#/bin/bash

# Description:
#  The script makes a copy of SPEY database
#  in running docker stack service container

#
# Run Information:
#  chmod u+x ./dump_db.sh
#  ./dump_db.sh

set -e

docker cp $(docker ps -q -f name=spey_backend):/app/src/dtop_spey/databases/spey.db ./spey.db_$(date +_%Y_%m_%d_%H_%M_%S)

echo "  The SPEY database is copied to ./spey.db_$(date +_%Y_%m_%d_%H_%M_%S)"
