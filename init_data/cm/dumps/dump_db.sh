#/bin/bash

# Description:
#  The script makes a dump of CM database
#  in running docker stack service container

#
# Run Information:
#  chmod u+x ./dump_db.sh
#  ./dump_db.sh

set -e

docker exec $(docker ps -q -f name=cm_backend) bash -c "PGPASSWORD=postgres pg_dump --username=postgres --host=localhost --clean dtop-catalog" > ./cm_db_dump$(date +_%Y_%m_%d_%H_%M_%S)_sql

echo "  The CM database dump file ./cm_db_dump$(date +_%Y_%m_%d_%H_%M_%S)_sql is created"
