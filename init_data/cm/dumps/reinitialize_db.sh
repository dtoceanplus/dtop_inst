#/bin/bash

# Description:
#  The script reinitializes CM database
#  in running docker stack service container

#
# Run Information:
#  chmod u+x ./reinitialize_db.sh
#  ./reinitialize_db.sh

set -e

cat ../scripts/cm_postgres_1_db_reinitialize_sql | docker exec -i $(docker ps -q -f name=cm_backend) bash -c "PGPASSWORD=postgres psql --username=postgres --host=localhost dtop-catalog"

echo "  The CM database is reinitialized using the script ../scripts/cm_postgres_1_db_reinitialize_sql"
