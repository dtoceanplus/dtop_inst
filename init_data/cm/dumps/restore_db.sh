#/bin/bash

# Description:
#  The script restores CM database from a CM SQL dump file
#  in running docker stack service container
#
# Run Information:
#  chmod u+x ./restore_db.sh
#  ./restore_db.sh

set -e

usage () {
    cat <<HELP_USAGE

    The script restores CM database using CM SQL <dump file>.

    Usage :   $0 <dump file>

    example : $0 cm_db_dump_2021_05_23_16_23_09_sql

HELP_USAGE
}

if [ $# -ne 1 ]
then
    usage
    exit 1
fi

if [[ ( $# == "--help") ||  $# == "-h" ]]
then
    usage
    exit 0
fi


db_dump="$1"

if [ -f "${db_dump}" ]; then
    echo "  OK - dump file ${db_dump} exists."
else
    echo "  NOK - dump file ${db_dump} doesn't exist."
    exit 1
fi

echo ""


cat ${db_dump} | docker exec -i $(docker ps -q -f name=cm_backend) bash -c "PGPASSWORD=postgres psql --username=postgres --host=localhost dtop-catalog"

echo "  The CM database is restored using dump file - ${db_dump}"
