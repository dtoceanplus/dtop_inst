#/bin/bash

# Description:
#  The script makes a copy of LMO database
#  in running docker stack service container

#
# Run Information:
#  chmod u+x ./dump_db.sh
#  ./dump_db.sh

set -e

docker cp $(docker ps -q -f name=lmo_backend):/app/src/dtop_lmo/instance/lmo.db ./lmo.db_$(date +_%Y_%m_%d_%H_%M_%S)

echo "  The LMO database is copied to ./lmo.db_$(date +_%Y_%m_%d_%H_%M_%S)"
