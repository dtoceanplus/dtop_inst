#/bin/bash

# Description:
#  The script makes a dump archive of ESA data files
#  in running docker stack service container

#
# Run Information:
#  chmod u+x ./dump_db.sh
#  ./dump_db.sh

set -e

docker exec $(docker ps -q -f name=esa_backend) tar -czf - storage > ./esa_data_dump$(date +_%Y_%m_%d_%H_%M_%S).tar.gz

echo "  The ESA data files dump archive ./esa_data_dump$(date +_%Y_%m_%d_%H_%M_%S).tar.gz is created"
