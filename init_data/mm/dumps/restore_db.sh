#/bin/bash

# Description:
#  The script restores MM database from a MM SQL dump file
#  in running docker stack service container
#
# Run Information:
#  chmod u+x ./restore_db.sh
#  ./restore_db.sh

set -e

usage () {
    cat <<HELP_USAGE

    The script restores MM database using MM SQL <dump file>.

    Usage :   $0 <dump file>

    example : $0 mm_db_dump_2021_03_03_22_29_54_sql

HELP_USAGE
}

if [ $# -ne 1 ]
then
    usage
    exit 1
fi

if [[ ( $# == "--help") ||  $# == "-h" ]]
then
    usage
    exit 0
fi


db_dump="$1"

if [ -f "${db_dump}" ]; then
    echo "  OK - dump file ${db_dump} exists."
else
    echo "  NOK - dump file ${db_dump} doesn't exist."
    exit 1
fi

echo ""

cat ${db_dump} | docker exec -i $(docker ps -q -f name=mm_database) mysql -umm_user -pmm_pass --database=main_db

echo "  The MM database is restored using dump file - ${db_dump}"
