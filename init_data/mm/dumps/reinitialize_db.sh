#/bin/bash

# Description:
#  The script reinitializes MM database
#  in running docker stack service container

#
# Run Information:
#  chmod u+x ./reinitialize_db.sh
#  ./reinitialize_db.sh

set -e

cat ../scripts/mm_mysql_6_db_reinitialize_sql | docker exec -i $(docker ps -q -f name=mm_database) mysql -umm_user -pmm_pass --database=main_db

echo "  The MM database is reinitialized using the script ../scripts/mm_mysql_6_db_reinitialize_sql"
