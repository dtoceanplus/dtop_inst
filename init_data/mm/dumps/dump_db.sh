#/bin/bash

# Description:
#  The script makes a dump of MM database
#  in running docker stack service container

#
# Run Information:
#  chmod u+x ./dump_db.sh
#  ./dump_db.sh

set -e

docker exec $(docker ps -q -f name=mm_database) bash -c "mysqldump -uroot -pmysql main_db" > ./mm_db_dump$(date +_%Y_%m_%d_%H_%M_%S)_sql

echo "  The MM database dump file ./mm_db_dump$(date +_%Y_%m_%d_%H_%M_%S)_sql is created"
