USE main_db;

CREATE TABLE `project` (
  `id` int PRIMARY KEY AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` varchar(255),
  `created_at` timestamp,
  `logo` mediumtext,
  `technology` ENUM ('Wave', 'Tidal'),
  `type_of_device` ENUM ('Fixed', 'Floating')
);

CREATE TABLE `study` (
  `id` int PRIMARY KEY AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` varchar(255),
  `template` ENUM ('Design and assessment', 'Standalone'),
  `created_at` timestamp,
  `project_id` int NOT NULL
);

CREATE TABLE `digitalrep` (
  `id` int PRIMARY KEY AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` varchar(255),
  `created_at` timestamp,
  `data` longtext,
  `study_id` int NOT NULL
);

CREATE TABLE `module` (
  `id` int PRIMARY KEY AUTO_INCREMENT,
  `name` varchar(255),
  `nickname` varchar(255) NOT NULL,
  `description` varchar(255),
  `type` ENUM ('Default tools', 'Design tools', 'Assessment tools', 'System', 'Catalog')
);

CREATE TABLE `study_module` (
  `id` int PRIMARY KEY AUTO_INCREMENT,
  `study_id` int NOT NULL,
  `module_id` int NOT NULL,
  `entity_id` int,
  `entity_name` varchar(255),
  `entity_status` ENUM ('In Progress', 'Finished', 'Stopped', 'Failed')
);

CREATE TABLE `user` (
  `id` int PRIMARY KEY AUTO_INCREMENT,
  `name` varchar(255) UNIQUE NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `created_at` timestamp
);

CREATE TABLE `role` (
  `id` int PRIMARY KEY AUTO_INCREMENT,
  `name` varchar(255) UNIQUE NOT NULL,
  `created_at` timestamp
);

CREATE TABLE `role_user` (
  `id` int PRIMARY KEY AUTO_INCREMENT,
  `user_id` int NOT NULL,
  `role_id` int NOT NULL
);

CREATE TABLE `project_user` (
  `id` int PRIMARY KEY AUTO_INCREMENT,
  `project_id` int NOT NULL,
  `user_id` int NOT NULL
);

CREATE TABLE `study_user` (
  `id` int PRIMARY KEY AUTO_INCREMENT,
  `study_id` int NOT NULL,
  `user_id` int NOT NULL
);

CREATE TABLE `event` (
  `id` int PRIMARY KEY AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `created_at` timestamp
);

CREATE TABLE `event_user` (
  `id` int PRIMARY KEY AUTO_INCREMENT,
  `user_id` int NOT NULL,
  `event_id` int NOT NULL
);


ALTER TABLE `study` ADD FOREIGN KEY (`project_id`) REFERENCES `project` (`id`);

ALTER TABLE `digitalrep` ADD FOREIGN KEY (`study_id`) REFERENCES `study` (`id`);

ALTER TABLE `study_module` ADD FOREIGN KEY (`study_id`) REFERENCES `study` (`id`);

ALTER TABLE `study_module` ADD FOREIGN KEY (`module_id`) REFERENCES `module` (`id`);

ALTER TABLE `role_user` ADD FOREIGN KEY (`user_id`) REFERENCES `user` (`id`);

ALTER TABLE `role_user` ADD FOREIGN KEY (`role_id`) REFERENCES `role` (`id`);

ALTER TABLE `project_user` ADD FOREIGN KEY (`project_id`) REFERENCES `project` (`id`);

ALTER TABLE `project_user` ADD FOREIGN KEY (`user_id`) REFERENCES `user` (`id`);

ALTER TABLE `study_user` ADD FOREIGN KEY (`study_id`) REFERENCES `study` (`id`);

ALTER TABLE `study_user` ADD FOREIGN KEY (`user_id`) REFERENCES `user` (`id`);

ALTER TABLE `event_user` ADD FOREIGN KEY (`event_id`) REFERENCES `event` (`id`);

ALTER TABLE `event_user` ADD FOREIGN KEY (`user_id`) REFERENCES `user` (`id`);


CREATE INDEX `study_module_index_0` ON `study_module` (`study_id`, `module_id`);

CREATE INDEX `role_user_index_1` ON `role_user` (`role_id`, `user_id`);

CREATE INDEX `project_user_index_2` ON `project_user` (`project_id`, `user_id`);

CREATE INDEX `study_user_index_3` ON `study_user` (`study_id`, `user_id`);

CREATE INDEX `event_user_index_4` ON `event_user` (`event_id`, `user_id`);
