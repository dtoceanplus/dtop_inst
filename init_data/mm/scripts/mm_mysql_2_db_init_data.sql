USE main_db;

INSERT INTO `role` VALUES (1,'admin','2020-09-24 02:47:12'),(2,'user','2017-09-06 05:46:45');

INSERT INTO `module` VALUES (
1,'SC','sc','Site Characterisation','System'),
(2,'MC','mc','Machine Characterisation','System'),
(3,'EC','ec','Energy Capture','Design tools'),
(4,'ET','et','Energy Transformation','Design tools'),
(5,'ED','ed','Energy Delivery','Design tools'),
(6,'SK','sk','Station Keeping','Design tools'),
(7,'LMO','lmo','Logistics and Marine Operation','Design tools'),
(8,'SPEY','spey','System Performance and Energy Yield','Assessment tools'),
(9,'RAMS','rams','Reliability, Availability, Maintainability, Survivability','Assessment tools'),
(10,'SLC','slc','System Lifetime Costs','Assessment tools'),
(11,'ESA','esa','Environmental and Social Acceptance','Assessment tools'),
(12,'SI','si','Structured Innovation','Default tools'),
(13,'SG','sg','Stage Gate','Default tools'),
(14,'CM','cm','Catalog','Catalog'
);

INSERT INTO `user` (`id`, `name`, `password`, `email`, `created_at`) VALUES (1, 'admin', 'adminadmin', 'admin@dtop.com', '2020-12-30 21:17:04');

INSERT INTO `role_user` (`id`, `user_id`, `role_id`) VALUES (1, 1, 1);
