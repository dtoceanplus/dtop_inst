#/bin/bash

# Description:
#  The script restores SK data files from a dump archive
#  in running docker stack service container
#
# Run Information:
#  chmod u+x ./restore_db.sh
#  ./restore_db.sh

set -e

usage () {
    cat <<HELP_USAGE

    The script restores SK data files from a dump archive <dump archive>.

    Usage :   $0 <dump archive>

    example : $0 sk_data_dump_2021_05_23_02_58_27.tar.gz

HELP_USAGE
}

if [ $# -ne 1 ]
then
    usage
    exit 1
fi

if [[ ( $# == "--help") ||  $# == "-h" ]]
then
    usage
    exit 0
fi


db_dump="$1"

if [ -f "${db_dump}" ]; then
    echo "  OK - dump file ${db_dump} exists."
else
    echo "  NOK - dump file ${db_dump} doesn't exist."
    exit 1
fi

echo ""

docker exec $(docker ps -q -f name=sk_backend) bash -c "cd storage; rm -rf *"

docker cp ${db_dump} $(docker ps -q -f name=sk_backend):/app/sk_dump_storage_backup.tar.gz

docker exec $(docker ps -q -f name=sk_backend) bash -c "tar -xzvf sk_dump_storage_backup.tar.gz; rm sk_dump_storage_backup.tar.gz; ls -lart storage"

echo "  SK data files are restored using dump file - ${db_dump}"
