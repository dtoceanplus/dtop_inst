#/bin/bash

# Description:
#  The script reinitializes SK data files
#  in running docker stack service container

#
# Run Information:
#  chmod u+x ./reinitialize_db.sh
#  ./reinitialize_db.sh

set -e

db_dump="storage_backup.tar.gz_sk_initial"

if [ -f "${db_dump}" ]; then
    echo "  OK - copy file ${db_dump} exists."
else
    echo "  NOK - copy file ${db_dump} doesn't exist."
    exit 1
fi

echo ""

docker exec $(docker ps -q -f name=sk_backend) bash -c "cd storage; rm -rf *"

docker cp ${db_dump} $(docker ps -q -f name=sk_backend):/app/sk_ini_storage_backup.tar.gz

docker exec $(docker ps -q -f name=sk_backend) bash -c "tar -xzvf sk_ini_storage_backup.tar.gz; rm sk_ini_storage_backup.tar.gz; ls -lart storage"

echo "  SK data files are reinitialized using initial ${db_dump}"
