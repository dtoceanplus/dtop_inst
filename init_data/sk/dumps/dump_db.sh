#/bin/bash

# Description:
#  The script makes a dump archive of SK data files
#  in running docker stack service container

#
# Run Information:
#  chmod u+x ./dump_db.sh
#  ./dump_db.sh

set -e

docker exec $(docker ps -q -f name=sk_backend) tar -czf - storage > ./sk_data_dump$(date +_%Y_%m_%d_%H_%M_%S).tar.gz

echo "  The SK data files dump archive ./sk_data_dump$(date +_%Y_%m_%d_%H_%M_%S).tar.gz is created"
