#/bin/bash

# Description:
#  The script reinitializes SI database
#  in running docker stack service container

#
# Run Information:
#  chmod u+x ./reinitialize_db.sh
#  ./reinitialize_db.sh

set -e

db_dump="structinn.sqlite_initial"

if [ -f "${db_dump}" ]; then
    echo "  OK - copy file ${db_dump} exists."
else
    echo "  NOK - copy file ${db_dump} doesn't exist."
    exit 1
fi

echo ""

docker exec $(docker ps -q -f name=si_backend) rm /data/structinn.sqlite

docker cp ${db_dump} $(docker ps -q -f name=si_backend):/data/structinn.sqlite

echo "  The SI database is reinitialized using initial ${db_dump}"
