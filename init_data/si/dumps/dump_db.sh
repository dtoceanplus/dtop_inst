#/bin/bash

# Description:
#  The script makes a copy of SI database
#  in running docker stack service container

#
# Run Information:
#  chmod u+x ./dump_db.sh
#  ./dump_db.sh

set -e

docker cp $(docker ps -q -f name=si_backend):/data/structinn.sqlite ./structinn.sqlite_$(date +_%Y_%m_%d_%H_%M_%S)

echo "  The SI database is copied to ./structinn.sqlite_$(date +_%Y_%m_%d_%H_%M_%S)"
