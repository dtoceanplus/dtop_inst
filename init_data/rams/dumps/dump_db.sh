#/bin/bash

# Description:
#  The script makes a copy of RAMS database
#  in running docker stack service container

#
# Run Information:
#  chmod u+x ./dump_db.sh
#  ./dump_db.sh

set -e

docker cp $(docker ps -q -f name=rams_backend):/app/src/rams_db/rams.db ./rams.db_$(date +_%Y_%m_%d_%H_%M_%S)

echo "  The RAMS database is copied to ./rams.db_$(date +_%Y_%m_%d_%H_%M_%S)"
