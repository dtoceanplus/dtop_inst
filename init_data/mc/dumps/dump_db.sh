#/bin/bash

# Description:
#  The script makes a copy of MC database
#  in running docker stack service container

#
# Run Information:
#  chmod u+x ./dump_db.sh
#  ./dump_db.sh

set -e

docker cp $(docker ps -q -f name=mc_backend):/app/src/instance/machine.db ./machine.db_$(date +_%Y_%m_%d_%H_%M_%S)

echo "  The MC database is copied to ./machine.db_$(date +_%Y_%m_%d_%H_%M_%S)"
