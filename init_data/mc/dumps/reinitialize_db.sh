#/bin/bash

# Description:
#  The script reinitializes MC database
#  in running docker stack service container

#
# Run Information:
#  chmod u+x ./reinitialize_db.sh
#  ./reinitialize_db.sh

set -e

db_dump="machine.db_initial"

if [ -f "${db_dump}" ]; then
    echo "  OK - copy file ${db_dump} exists."
else
    echo "  NOK - copy file ${db_dump} doesn't exist."
    exit 1
fi

echo ""

docker exec $(docker ps -q -f name=mc_backend) rm /app/src/instance/machine.db

docker cp ${db_dump} $(docker ps -q -f name=mc_backend):/app/src/instance/machine.db

echo "  The MC database is reinitialized using initial ${db_dump}"
