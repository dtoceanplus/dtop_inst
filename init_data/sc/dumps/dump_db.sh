#/bin/bash

# Description:
#  The script makes a dump archive of SC data files
#  in running docker stack service container

#
# Run Information:
#  chmod u+x ./dump_db.sh
#  ./dump_db.sh

set -e

docker exec $(docker ps -q -f name=sc_backend) tar -czf - storage > ./sc_data_dump$(date +_%Y_%m_%d_%H_%M_%S).tar.gz

echo "  The SC data files dump archive ./sc_data_dump$(date +_%Y_%m_%d_%H_%M_%S).tar.gz is created"
