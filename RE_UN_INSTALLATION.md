# DTOP Modules' Re-Installation and Un-Installation Procedure

DTOP Installation documentation includes :

- [DTOP (DTOP) Project Installation Summary > > . . . ](./README.md)
- [DTOP Project Installation Prerequisite > > . . . ](./PREREQUISITES.md)
- [DTOP Modules' Installation Procedure > > . . . ](./INSTALLATION.md)
- [DTOP Modules' Re-Installation and Un-Installation Procedure > > . . . ](./RE_UN_INSTALLATION.md)

This document - `DTOP Modules' Re-Installation and Un-Installation Procedure`.

After installation and deployment of DTOP Modules, during operational usage it may be necessary
- to manage module database and data
- to Re-Install/Re-Deploy or Un-Install/Un-Deploy of one or all Module Stacks and Services.

These actions can be also performed with one of the following ways described in this document.

- Persistent Storage of the modules' results and databases
- Cleanup unused docker containers (optional)
- Use-Case : changing some module parameters defined in `dtop_inst.env`
- DTOP Modules' Re-Installation / Re-Deployment
  - using `dtop_inst.sh`
  - using `dtop_redeploy.sh`
  - using Portainer UI
- DTOP Modules' Un-Installation / Un-Deployment
  - using `dtop_uninst.sh`
  - using Docker CLI
  - using Portainer UI

## Persistent Storage of the modules' results and databases

The results produced during DTOP modules usage are stored into the module databases of the running docker containers.

DTOP installation procedure provides Persistent Storage of the current modules database on the host machine
(where installation is performed or on the mounted backuped disks) by means of usage of docker volumes for all modules.

All Modules' deployment stacks are configured in similar way and the directory `${DTOP_DATA}`
contains subdirectories for each module to share and stores the module's database and data.

The value of `${DTOP_DATA}` is defined in `dtop_inst.env`.
If it is not set the default location `${INST_ROOT}/_volume/data/` is used.

```
${INST_ROOT}/_volume/data/<module nickname>
```

For example, `D:\dtop\dtop_inst_windows_1.0.0\_volume\data\ed\energydeliv.db`.

Additionally to provide backuping/dumping, restoring and reinitialization of module data the next scripts
are developed and included in installation procedure and available in `${DTOP_DATA}/<module nickname>/dumps`:

- dump_db.sh
```
#  The script makes a dump of module database
#  in running docker stack service container
```
- restore_db.sh
```
#  The script restores module database from a SQL dump file
#  in running docker stack service container
```
- reinitialize_db.sh
```
#  The script reinitializes module database
#  in running docker stack service container
```

When it is necessary user can run manually the required scripts for the given modules.

The procedure of automatic backuping/dumping of modules' databases can be scheduled and executed
by means of using dedicated utilities and services, for example `cron`.

To run the scripts change the directory to `${DTOP_DATA}/<module nickname>/dumps` of required module, for example, for ED module :

```
cd D:\dtop\dtop_inst_windows_1.0.0\_volume\data\ed\dumps

```

## Cleanup unused docker containers (optional)

To cleanup / remove currently unused docker containers the auxiliary script `cleanup_unused_containers.sh` can be run :

```
./cleanup_unused_containers.sh

```
## Use-Case : changing module installation parameters`

A need to Re-Install/Re-Deploy or Un-Install/Un-Deploy of one or all Module Stacks and Services
can be raised when some module installation parameters defined in `dtop_inst.env` file are changed :

```
SI_CI_REGISTRY=registry.gitlab.com/dtoceanplus/dtop_si
SI_IMAGE_TAG=v1.1.1
```

**NOTE**: In all necessary use-cases, first of all, please update the correspondent module parameters in `dtop_inst.env`.

## DTOP Modules' Re-Installation / Re-Deployment

Possible optional ways for DTOP Modules' Re-Installation / Re-Deployment of necessary modules :
- using and restarting installation script - `dtop_inst.sh`
- using and starting the redeployment script - `dtop_redeploy.sh`
- using Portainer UI - `Stack` menu item from the right side menu

## DTOP Modules' Un-Installation / Un-Deployment :

DTOP Modules' Un-Installation / Un-Deployment possible optional ways :
- using `dtop_uninst.sh`
- using Docker CLI
- using Portainer UI - `Stack` menu item from the right side menu

### DTOP Modules' Un-Installation / Un-Deployment using `dtop_uninst.sh`

Run the script - `./dtop_uninst.sh`

The script
- un-deploys (removes) DTOP Modules' Docker Stacks - \<module nickname\>_stack.yml
- un-deploys (removes) Basic Docker Service Stacks - Portainer and Traefik
- cleans up DTOP Services infrastructure - Docker \"traefik-public\" network and SWARM mode
- removes all unused Docker containers, networks, images and volumes
previously deployed / initialized on the server / local workstation

This script provides two options
- uninstall all available DTOP Modules installed before.
- uninstall "latest_selected_modules.txt" created during the previous installation.

The file "latest_selected_modules.txt" can be edited manually if necessary.

### DTOP Modules' Un-Installation / Un-Deployment using Docker CLI

To undeploy (remove) one required DTOP Module Docker Stack the next docker command can be used:
```
docker stack rm \<module nickname\>
```
for example :
```
docker stack rm ed
```

All necessary actions, mentioned in the previous paragraph, can be also performed as docker commands from console.
You can see them in the script `dtop_uninst.sh`.

### DTOP Modules' Un-Installation / Un-Deployment using `Portainer UI`

In browser Open Portainer page with the list of deployed and run DTOP Stacks as shown on the image below.

Select the required Module Stack and then  click `Remove` button.

![](./images/port_stacks.png)
