# DTOceanPlus (DTOP) Project Installation PrerequisitesOB

DTOP Installation documentation includes :

- [DTOP (DTOP) Project Installation Summary > > . . . ](./README.md)
- [DTOP Project Installation Prerequisite > > . . . ](./PREREQUISITES.md)
- [DTOP Modules' Installation Procedure > > . . . ](./INSTALLATION.md)
- [DTOP Modules' Re-Installation and Un-Installation Procedure > > . . . ](./RE_UN_INSTALLATION.md)

This document - `DTOP Project Installation Prerequisites`.

It provides the necessary details for :

- HW prerequisites and system requirements
- SW installation prerequisites
  - Installation of Docker Engine
  - Installation of Bash Shell
- DTOP Installation Parameters
  - Check / Adjust the Parameters
- DTOP Modules domain names and URLs
  - For installation on a local workstation
  - For installation on a company server

**NOTE**: The final requirements and parameters, that provide the proper performance of all DTOP modules, have to be additionally estimated in the frame of careful functional tests by all DTOP module developers.

## HW prerequisites and system requirements

The installation procedure has been tested for Windows 10 Pro workstation and Debian 10 Buster (Virtual Machine under KVM Hypervisor) with the following parameters.

In accordance with the additional request the procedure is also tested on iMac macOS workstation.

Microsoft Windows 10 Pro :

```
OS Name:                   Microsoft Windows 10 Pro
OS Version:                10.0.18363 N/A Build 18363
Total Physical Memory:     32,688 MB

DeviceID  NumberOfCores  NumberOfLogicalProcessors
CPU0      6              12
```

Debian 10 Buster Virtual Machine (VM) :

```
PRETTY_NAME="Debian GNU/Linux 10 (buster)"
Architecture:        x86_64
RAM                  7978 MB
CPU(s):              4
Hypervisor vendor:   KVM
```

iMac (iMac18,3) :

```
OS Name:		macOS
OS Version: 		version: 11.1 (20C69)
Total Physical Memory:  32 GB
Total Number of Cores:	4
```


## SW installation prerequisites

- Docker Engine
- Bash Shell

DTOP Modules, Stacks and Services are provided as Docker production images.

DTOP installation procedure is based on bash scripts with using "whiptail" program for user-friendly UI.

The same installation code/scripts are used on tested Windows, Debian and iMac machines.

**NOTE**: installation of `Docker Engine` and `Cygwin` (for Windows 10) requires administrator permissions.

### Installation of Docker Engine

Install Docker Engine as described in [docker documentation](https://docs.docker.com/engine/install).

Docker Engine is available on a variety of Linux platforms, Windows 10 and iMac (through Docker Desktop), and as a static binary installation.

Docker Desktop includes Docker Engine, Docker CLI client, Docker Compose and some other components - it is recommended for installation on local workstation.

Docker Requirements and Recommendations :

- Available RAM 	Minimum:  12 Gb; Recommended: 16+ Gb
- CPU Count 	Minimum: 2; Recommended 4+
- Disk Space > 64 Gb

**NOTE**:

- Make sure that Docker Engine is running.
- Tests of installation have been performed for switched off WSL mode of Docker Desktop.
Make sure that the checkbox "Use the WSL 2 based engine (requires Win 10 build 19018+)" is unselected.
- Make sure that Docker Desktop configuration for "Resources -> FILE SHARING"
includes DTOP installation directory and DTOP_DATA or disk.
It allows to share and stores the module's database and data from the containers to local host.
See also comments in `dtop_inst.env`

After Docker Engine installation (described below) to display system wide information regarding the Docker installation you can type the command :

```
docker info
```

#### Microsoft Windows 10 Pro

```
Client: Docker Engine - Community
 Version:           19.03.8
 API version:       1.40

Server: Docker Engine - Community
 Engine:
  Version:          19.03.8
  API version:      1.40 (minimum version 1.12)
```

Following to [Install Docker Desktop on Windows](https://docs.docker.com/docker-for-windows/install/)
install Docker Desktop.

Set the necessary Docker parameters consulting [Docker Desktop for Windows user manual](https://docs.docker.com/docker-for-windows/), for example :

![](./images/docker_windows_settings_1.png)

![](./images/docker_windows_settings_2.png)

#### Debian 10 Buster VM

```
Client: Docker Engine - Community
 Version:           19.03.13

Server: Docker Engine - Community
 Engine:
  Version:          19.03.13
  API version:      1.40 (minimum version 1.12)
```

Following to [Install Docker Engine on Debian](https://docs.docker.com/engine/install/debian/)
install Docker.

#### iMac

```
Client: Docker Engine - Community
 Version:           20.10.2
 API version:       1.41

Server: Docker Engine - Community
 Engine:
  Version:          20.10.2
  API version:      1.41 (minimum version 1.12)
```

Following to [Install Docker Desktop on Mac](https://docs.docker.com/docker-for-mac/install/) install Docker Desktop.

Set the necessary Docker parameters consulting with [Docker Desktop for Mac user manual](https://docs.docker.com/docker-for-mac/), for example :

By default iMac Docker Desktop configuration for "Resources -> FILE SHARING" includes the directories /Users, /Volume, /private, /tmp and /var/folders.

![](./images/docker_imac_settings_1.png)

![](./images/docker_imac_settings_2.png)

### Installation of Bash Shell

DTOP installation procedure is based on bash scripts with using `whiptail` for user-friendly UI.

Debian uses Bash as the default interactive shell.

For Windows 10 the required Bash environment including `whiptail` is created by running  `cygwin_inst.bat` and `dtop_inst.bat`.
`Cygwin` is free software that provides a Unix-like environment and software tool set to users. Bash is default shell in Cygwin.

Bash is also used as shell on macOS.

#### whiptail

Whiptail is a program that allows shell scripts to display dialog boxes and to get input from the user in a friendly way.

Whiptail is included by default on Debian. Therefore no extra steps are required in this case.

`cygwin-whiptail` is Debian source complied for Cygwin. `cygwin-whiptail` binaries are included in the DTOP installation for Windows 10.

To install whiptail on macOS using brew (`https://brew.sh/`):
```
brew install newt
```

## DTOP Installation parameters

DTOP installation code GitLab repository is [dtop_inst](https://gitlab.com/dtoceanplus/dtop_inst/).
It includes two installation archives and the archive with SC data.

DTOP installation procedure is packaged and provides
- the archive `dtop_inst_windows_<VERSION>.zip` for Windows 10 (for example `dtop_inst_windows_1.0.0.zip`)
- the archive `dtop_inst_linux_<VERSION>.tar.gz` for Debian and Mac OS (for example `dtop_inst_linux_1.0.0.tar.gz`)

**NOTE**:

DTOP SC module needs and uses the set of own basic data files.
These SC data files are provided in the quite large archive `dtop_inst_sc_databases_v1.x.tar.gz`.
The archive should be downloaded separately from [DTOceanPlus Site Characterisation (SC) module databases repo](https://gitlab.com/dtoceanplus/dtop_sc_databases) and copied into DTOP installation directory to be used by the installation procedure.

The exact version of the used SC data files archive is defined in `dtop_inst.env` with the variable `SC_databases_archive_version`, for example :
```
SC_databases_archive_version=dtop_inst_sc_databases_v1.5.tar.gz
```

Only the required installation archive and SC archive are needed non the whole repository.

- Download and unarchive the required DTOP installation archive on a local workstation or on a company server from [InstallationPackage Registry](https://gitlab.com/dtoceanplus/dtop_inst/-/packages)

![](./images/archives_download.png)

- Create the directory for DTOP installation and extract the content of the archive to this directory, for example :

```
For Linux and MacOS :

tar -pxzf dtop_inst_linux_1.0.0.tar.gz -C ./dtop_inst_1.0.0

where ./dtop_inst_1.0.0 - the pre-created installation directory.

On Windows use standard provided tools.
```

- For Windows create the `cygwin` environment by running  `cygwin_inst.bat` and open its `bash` terminal by `dtop_inst.bat`.
- Check / Adjust the initial parameters in `dtop_inst.env` file if necessary (optional).
- Execute the installation script `./dtop_inst.sh` from a bash terminal.

**NOTE**:

- installation directory name should not contain spaces and special characters.
- Installation process of all modules from the scratch can take some time - please be patient.

Read and follow the related documentation files (`.md`).

### Check / Adjust the Parameters

In the necessary cases check / adjust the values of appropriate parameters in `dtop_inst.env` file :

```
# No changes are necessary for installation on local workstation

# Set DTOP_DOMAIN value :
#   dtop.localhost - for DTOP installation on the local workstation
#   dtop.<company domain name> - for DTOP installation on a server in intranet environment

DTOP_DOMAIN=dtop.localhost
#DTOP_DOMAIN=dtop.<company server domain name>

# Optional
# If DTOP_DATA is not defined here the default location `${INST_ROOT}/_volume/data/` is used.
#
# Set the directory DTOP_DATA what will contain subdirectories for each module
# - to share and stores the module's database and data
# - and "dumps" subdirectory for dumping, restoring and reinitialization
#   of module database and data using the provided scripts :
#   - dump_db.sh
#   - restore_db.sh
#   - reinitialize_db.sh
#
# It can be the directory on local disk or on the mounted disks
# that should be backuped regularly.
#
# "PREREQUISITES.md" :
# Make sure that Docker Desktop configuration for "Resources -> FILE SHARING"
# includes DTOP installation directory and DTOP_DATA or disk.
# It allows to share and stores the module's database and data from the containers to local host.
#
#DTOP_DATA=/mnt/dtop/test/data/

# For info :
# ALL_MODULES_NICKNAMES : si sg sc mc ec et ed sk lmo spey rams slc esa cm mm

# Mandatory
# Set the GitLab Registry and Tag where the module docker production images are stored

SI_CI_REGISTRY=registry.gitlab.com/dtoceanplus/dtop_si
SI_IMAGE_TAG=v1.0.1

...

```

### DTOP Modules domain names and URLs

#### For installation on a local workstation

For installation on a local workstation DTOP_DOMAIN=dtop.localhost is used.

**NOTE**: No additional settings of the `dtop_inst.env` file are required for installation in the local workstation, since the default configurations are created for this case.

`Chrome browser` provides resolving all local modules URLs `<module nickname>.dtop.localhost` by default.

**NOTE**: In `Firefox browser` to make available resolving `.localhost` domain
- type `about:config`
- find `network.dns.native-is-localhost`
- change its value from `false` to `true`

please note the following behavior: in Debian, after the modification above Firefox might not be able to connect to internet anylonger.
To re-establish the normal functionality of the browser, just reset the `network.dns.native-is-localhost` to `false`.

#### For installation on a company server

For installation on a company server the Module Domain names and URLs for accessing in company intranet are defined as presented on the image below.

Resolving of the Module Domain names and URLs for intranet workstations should be provided by company system administrators.

![](./images/dtop_domain_names.png)
