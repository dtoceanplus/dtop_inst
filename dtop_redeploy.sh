#!/bin/bash
#
# Script Name: dtop_redeploy.sh
# Version: 0.1
# TermsOfService: 'https://www.dtoceanplus.eu/'
#
# Description:
#  The script provides (re)deployment of the necessary DTOP modules
#  It uses the variables and functions of "./dtop_basic.sh".
#  The environment variables are read from "./dtop_inst.env".
#
# Run Information:
#  check that the file has executable permissions.
#  if no, set it
#  chmod +x ./dtop_redeploy.sh
#

# ------------------------------------------
# Set variables for installation directory.
# ------------------------------------------

__dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

INST_ROOT="${__dir}"

echo ""
echo INST_ROOT == "${INST_ROOT}"

source "${INST_ROOT}"/dtop_basic.sh

START_TIME=$(date +%s)

echo ""
echo "---------------------------------------------"
echo " DTOP modules (re)deployment log start :"
echo "---------------------------------------------"

echo " Installation directory - ${INST_ROOT}"
echo ""

export STEP_2_TITLE_1=" Step 1/3 - Selection of DTOP modules "
export STEP_2_TITLE_2=" \n   Please select the modules to be (re)deployed "
step_2

export STEP_4_TITLE_1=" Step 2/3 - DTOP Modules Docker Stacks (re)deployment "
step_4

export STEP_5_TITLE_1=" Step 3/3 - Checking (re)deployed infrastructure and DTOP Running "
step_5

"${INST_ROOT}"/cleanup_unused_containers.sh

echo "---------------------------------------------"
echo " DTOP modules (re)deployment log finish. "
echo "---------------------------------------------"

END_TIME=$(date +%s)

echo " Deployment process took $(($END_TIME - $START_TIME)) seconds"
echo ""
