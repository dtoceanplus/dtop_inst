#!/bin/bash
#
# Script Name: make_dtop_archives.sh
# Version: 0.1
# TermsOfService: 'https://www.dtoceanplus.eu/'
#
# Description:
#  The script makes the archives of DTOP installation 
#  for Windows (.zip) and Linux and MacOS (.tar.gz).
#
# Run Information:
#  check that the file has executable permissions.
#  if no, set it
#  chmod +x ./make_dtop_archives.sh
#

usage () {
    cat <<HELP_USAGE

    The script makes the package archives of DTOP installation
    using "VERSION" as parameter.

    Usage :   $0 <VERSION>

    example : $0 1.0.0

HELP_USAGE
}

if [ $# -ne 1 ]
then
    usage
    exit 1
fi

if [[ ( $# == "--help") ||  $# == "-h" ]]
then
    usage
    exit 0
fi

VERSION="$1"

echo ${VERSION}

cp dtop_win_inst/bin/dtop_win_inst.exe .
cp dtop_win_inst/bin/dtop_mon.exe .
cp dtop_win_inst/bin/ICSharpCode.SharpZipLib.dll .
cp dtop_win_inst/dtop_win_inst/LICENSE.txt ./License-win.txt

LINUX_FILES_LIST="images \
init_data \
stacks \
cleanup_unused_containers.sh \
dtop_basic.sh \
dtop_inst.env \
dtop_inst.sh \
dtop_redeploy.sh \
dtop_uninst.sh \
INSTALLATION.md \
LICENSE.txt \
PREREQUISITES.md \
RE_UN_INSTALLATION.md \
README.md \
DTOceanPlusLogo.ico"

WINDOWS_FILES_LIST="cygwin_pack cygwin_inst.bat dtop_inst.bat \
ICSharpCode.SharpZipLib.dll \
dtop_mon.exe \
${LINUX_FILES_LIST}"

WIN_INST_FILES_LIST="images \
init_data \
stacks \
dtop_inst.env \
License-win.txt \
INSTALLATION.md PREREQUISITES.md RE_UN_INSTALLATION.md README.md \
dtop_win_inst.exe \
ICSharpCode.SharpZipLib.dll \
dtop_mon.exe"

find . -type f -name '*.sh' -exec chmod a+x {} \;

echo ""
echo "creating dtop_inst linux / macos archive *.tar.gz ..."
tar -pczf dtop_inst_linux_${VERSION}.tar.gz ${LINUX_FILES_LIST}

echo ""
echo "creating dtop_inst windows archive *.zip ..."
zip -rq dtop_inst_windows_${VERSION}.zip ${WINDOWS_FILES_LIST}

echo ""
echo "creating dtop_inst win_inst archive *.zip ..."
zip -rq dtop_win_inst_${VERSION}.zip ${WIN_INST_FILES_LIST}


echo ""
