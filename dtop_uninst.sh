#!/bin/bash
#
# Script Name: dtop_uninst.sh
# Version: 0.1
# TermsOfService: 'https://www.dtoceanplus.eu/'
#
# Description:
#  The script provides Un-Installation of DTOceanPlus (DTOP)
#  It uses the file "latest_selected_modules.txt" created during the previous installation.

# Run Information:
#  check that the file has executable permissions.
#  if no, set it
#  chmod +x ./dtop_uninst.sh

# ------------------------------------------
# Set variables for installation directory
# ------------------------------------------

__dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

INST_ROOT="${__dir}"

source "${INST_ROOT}"/dtop_basic.sh

# -------------------------------------------------------------
# DTOP Modules' Docker Services and environment Un-Installation
# Functions and variables
# ------------------------------------------------------------

UN_INIST_TITLE=" DTOP Modules' Docker Services and environment Un-Installation"

basic_stack_undeploy() {

  echo ""

  docker stack ps portainer > /dev/null 2>&1

  if [ $? -eq 1 ]; then
    echo " The service \"portainer\" doesn't exist"
  else
    echo " The service \"portainer\" exists"
    docker stack rm portainer
  fi

  echo ""
  docker stack ps tra > /dev/null 2>&1

  if [ $? -eq 1 ]; then
    echo " The service \"traefik\" doesn't exist"
  else
    echo " The service \"traefik\" exists"
    docker stack rm tra
  fi

}

remove_traefik_network() {

  echo ""

  docker network inspect traefik-public > /dev/null 2>&1

  if [ $? -eq 1 ]; then
    echo " Docker network \"traefik-public\" doesn't exists"
  else
    echo " Docker network \"traefik-public\" exists"
    docker network rm traefik-public
  fi

}

remove_volume() {

  echo ""

  docker volume inspect $1 > /dev/null 2>&1

  if [ $? -eq 1 ]; then
    echo " Docker volume \"$1\" doesn't exists - is not being removed "
  else
    echo " Docker volume \"$1\" exists - is removing ... "
    docker volume rm $1
  fi

}

leave_swarm_mode() {

  echo ""

  if docker node ls > /dev/null 2>&1; then
    echo " The node is in a swarm mode"
    docker swarm leave --force
  else
    echo " The node is a in standalone mode"
  fi

}

START_TIME=$(date +%s)

echo ""
echo "-----------------------------------------------"
echo " DTOceanPlus (DTOP) Un-Installation log start :"
echo "-----------------------------------------------"

echo ""
echo "${UN_INIST_TITLE}"

echo " Installation directory - ${INST_ROOT}"
echo ""




if [ ! -f "${INST_ROOT}"/latest_selected_modules.txt ]; then

  echo " The file ${INST_ROOT}/latest_selected_modules.txt used for uninstallation is not found."
  echo " Make sure that DTOP Project was previously installed."
  echo " DTOP installation normally creates this file that can be edited manually."
  echo " This file contains a list of Modules nicknames, for example for ALL modules :"
  echo  \"si\" \"sg\" \"sc\" \"mc\" \"ec\" \"et\" \"ed\" \"sk\" \"lmo\" \"spey\" \"rams\" \"slc\" \"esa\" \"cm\" \"mm\"

else

  echo " The DTOP modules to be undeployed are read from the file - "
  echo " \"latest_selected_modules.txt\" (it can be edited manually)"

  MODULES_NICKNAMES=`cat ${INST_ROOT}/latest_selected_modules.txt`
  echo " - MODULES_NICKNAMES = ${MODULES_NICKNAMES}"

  MODULES=$(echo "${MODULES_NICKNAMES}" | tr '[:lower:]' '[:upper:]')

fi


for MODULE in ${MODULES}; do

  MODULE=$(echo "${MODULE}" | tr -d '"')

  echo ""
  echo " Un-Deploying MODULE = ${MODULE}"

  MODULE_NICKNAME=$(echo "${MODULE}" | tr '[:upper:]' '[:lower:]' | tr -d '"')

  if [ "${MODULE_NICKNAME}" = "mm" ]; then
    mm_db_cont_id=$(docker ps -f name=mm_mm_database --format "{{.ID}}")
    if [[ ! -z "$mm_db_cont_id" ]]
    then
      echo " Coping MM database data from docker volume to ${DTOP_DATA}/${MODULE_NICKNAME}/db/ ... " 
      (cd "${DTOP_DATA}"/${MODULE_NICKNAME} && docker cp $mm_db_cont_id:/var/lib/mysql ./db/)
    fi
  fi

  echo ""

  if [ "${MODULE_NICKNAME}" = "cm" ]; then
    cm_db_cont_id=$(docker ps -f name=cm_cm_backend --format "{{.ID}}")
    if [[ ! -z "$cm_db_cont_id" ]]
    then
      echo " Coping CM database data from docker volume to ${DTOP_DATA}/${MODULE_NICKNAME}/db/ ... " 
      (cd "${DTOP_DATA}"/${MODULE_NICKNAME} && docker cp $cm_db_cont_id:/var/lib/postgresql ./db/)
    fi
  fi

  docker stack ps ${MODULE_NICKNAME} > /dev/null 2>&1

  if [ $? -eq 1 ]; then
    echo " The service \"${MODULE_NICKNAME}\" doesn't exist"
  else
    docker stack rm ${MODULE_NICKNAME}
  fi

done

echo ""

basic_stack_undeploy

remove_traefik_network

leave_swarm_mode

remove_volume mm_mysql_data

remove_volume cm_pg_data

echo ""

echo "Removing all unused Docker containers, networks, images and volumes :"

echo ""

docker system prune -a --volumes

echo ""

echo "------------------------------------------------"
echo " DTOceanPlus (DTOP) Un-Installation log finish. "
echo "------------------------------------------------"

END_TIME=$(date +%s)

echo " Un-Installation process took $(($END_TIME - $START_TIME)) seconds"
echo ""
