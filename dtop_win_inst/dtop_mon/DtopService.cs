// This is DTOceanPlus Monitor Tool.
// This tool monitors the different components of the DTOceanPlus modules on Windows.
// Copyright (C) 2021 OPEN CASCADE
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
// License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dtop_mon
{
    public class DtopService
    {
        public string ID { get; private set; }
        public string Name { get; private set; }
        public string Mode { get; private set; }
        public string Replicas { get; private set; }
        public string Image { get; private set; }
        public string Version { get; private set; }
        public string Ports{ get; private set; }

        public bool IsRunning
        {
            get { return Replicas == "1/1"; }
        }

        public delegate void ReplicasChangedEventHandler(object sender, EventArgs e);

        public event ReplicasChangedEventHandler ReplicasChanged;

        public string ShortName
        {
            get
            {
                return Name.Split('_').Last();
            }
        }

        public DtopService()
        {
        }
        public DtopService(string name)
        {
            Name = name;
        }

        public static DtopService Parse(string line)
        {
            var elts = line.Split(';');
            string version = elts[4].Split(':')[1];

            DtopService service = new DtopService()
            {
                ID = elts[0],
                Name = elts[1],
                Mode = elts[2],
                Replicas = elts[3],
                Image = elts[4],
                Version = version
            };
            if (elts.Length > 5) service.Ports = elts[5];
            
            return service;
        }

        internal void Update(DtopService ds)
        {
            ID = ds.ID;
            Mode = ds.Mode;
            if (Replicas != ds.Replicas)
            {
                Replicas = ds.Replicas;
                if (ReplicasChanged != null)
                {
                    ReplicasChanged(this, new EventArgs());
                }
            }
            Image = ds.Image;
            Version = ds.Version;
            Ports = ds.Ports;
        }

        internal void Reset()
        {
            Replicas = "";
            if (ReplicasChanged != null)
            {
                ReplicasChanged(this, new EventArgs());
            }
        }

        internal string Write()
        {
            return String.Format("{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}",
                ID, Name, Mode, Replicas, Image, Version, Ports);
        }
        internal static string WriteTitle()
        {
            return String.Format("{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}",
                "ID", "Name", "Mode", "Replicas", "Image", "Version", "Ports");
        }
    }
}
