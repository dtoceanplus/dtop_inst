﻿// This is DTOceanPlus Monitor Tool.
// This tool monitors the different components of the DTOceanPlus modules on Windows.
// Copyright (C) 2021 OPEN CASCADE
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
// License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace dtop_mon
{
    public partial class FrmBackup : Form
    {
        private BackupManager m_manager = new BackupManager();
        private List<DtopModule> m_modules = new List<DtopModule>();
        private BackupMode m_mode = BackupMode.Backup;

        public enum BackupMode
        {
            Backup,
            Restore,
            Init
        }

        public string BackupName
        {
            get { return tbxBackupName.Text; }
            set { tbxBackupName.Text = value; }
        }
        public FrmBackup(IEnumerable<DtopModule> modules, BackupMode mode)
        {
            InitializeComponent();
            m_mode = mode;
            m_modules.AddRange(modules);

            if (mode == BackupMode.Init)
            {
                this.Text = "Reinitialize project";
                btnBackup.Text = "Reset";
                tbxBackupName.Text = "<Init>";
                tbxBackupName.ReadOnly = true;
            }
            else if (mode == BackupMode.Restore)
            {
                this.Text = "Restore project";
                btnBackup.Text = "Restore";
                tbxBackupName.ReadOnly = true;
            }

            m_manager.ModuleBackedUp += this.ModuleBackedUp;
        }

        private void ModuleBackedUp(object sender, BackupedModuleEventArgs e)
        {
            ListViewItem item = new ListViewItem(e.Module);
            item.ImageIndex = e.Succeed ? 0 : 1;
            listView1.Items.Add(item);
            listView1.Refresh();
            item.ToolTipText = e.Error;
        }

        private void btnBackup_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(tbxBackupName.Text)) return;
            listView1.Items.Clear();

            string name = tbxBackupName.Text;
            if (btnBackup.Text == "Backup")
            {
                string txt = tbxBackupName.Text;
                name = name.Replace(" ", "_");
                foreach (char c in Path.GetInvalidFileNameChars()) name = name.Replace(c, '_');
                if (name != txt)
                {
                    MessageBox.Show(String.Format("Invalid characters have been replaced by '_'!", name),
                        this.Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    tbxBackupName.Text = name;
                }
            }

            int res = 0;
            string errors = "";
            if (m_mode == BackupMode.Backup)
                res = RunBackup(name);
            else if (m_mode == BackupMode.Restore)
                res = RunRestore(name, out errors);
            else if (m_mode == BackupMode.Init)
                res = RunReset(name, out errors);

            if (res < 0) return;

            if (res != m_modules.Count)
            {
                int bad = m_modules.Count - res;
                string message = String.Format("Something wrong happened {0} modules were not processed!", bad);
                message += Environment.NewLine + errors;
                MessageBox.Show(message, this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private int RunBackup(string name)
        {
            int res = 0;
            string backup = Path.Combine(Properties.Settings.Default.DtopPath, "_backup", name);
            if (Directory.Exists(backup))
            {
                MessageBox.Show(String.Format("Backup '{0}' already exists!", name), this.Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return -1;
            }

            this.Cursor = Cursors.WaitCursor;
            btnQuit.Enabled = false;
            try
            {
                string data = Path.Combine(Properties.Settings.Default.DtopPath, "_volume", "data");
                res = m_manager.Backup(m_modules, backup);
            }
            finally
            {
                this.Cursor = Cursors.Default;
                btnQuit.Enabled = true;
            }
            return res;
        }
        private int RunRestore(string name, out string errors)
        {
            int res = 0;

            this.Cursor = Cursors.WaitCursor;
            btnQuit.Enabled = false;
            try
            {
                string backup = Path.Combine(Properties.Settings.Default.DtopPath, "_backup", name);
                res = m_manager.Restore(m_modules, backup, out errors);
            }
            finally
            {
                this.Cursor = Cursors.Default;
                btnQuit.Enabled = true;
            }
            return res;
        }
        private int RunReset(string name, out string errors)
        {
            int res = 0;

            this.Cursor = Cursors.WaitCursor;
            btnQuit.Enabled = false;
            try
            {
                string data = Path.Combine(Properties.Settings.Default.DtopPath, "_volume", "data");
                res = m_manager.Reset(m_modules, data, out errors);
            }
            finally
            {
                this.Cursor = Cursors.Default;
                btnQuit.Enabled = true;
            }
            return res;
        }

        private void btnQuit_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
