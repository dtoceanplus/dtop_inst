// This is DTOceanPlus Monitor Tool.
// This tool monitors the different components of the DTOceanPlus modules on Windows.
// Copyright (C) 2021 OPEN CASCADE
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
// License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.
﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dtop_mon
{
    public static class Utils
    {
        public static string ParseInfoLine(string line)
        {
            return line.Split(':')[1].Trim();
        }

        public static float ParseSize(string size)
        {
            float res = 0;
            if (size.EndsWith("MiB"))
            {
                res = float.Parse(size.Substring(0, size.Length - 3), CultureInfo.InvariantCulture);
            }
            else if (size.EndsWith("GiB"))
            {
                res = float.Parse(size.Substring(0, size.Length - 3), CultureInfo.InvariantCulture) * 1024;
            }
            else if (size.EndsWith("MB"))
            {
                res = float.Parse(size.Substring(0, size.Length - 2), CultureInfo.InvariantCulture);
            }
            else if (size.EndsWith("GB"))
            {
                res = float.Parse(size.Substring(0, size.Length - 2), CultureInfo.InvariantCulture) * 1000;
            }

            return res;
        }

        public static void OpenUrl(string url)
        {
            if (File.Exists(Properties.Settings.Default.BrowserExe))
            {
                Process.Start(Properties.Settings.Default.BrowserExe, url);
            }
            else
            {
                Process.Start(url);
            }
        }

        internal static long GetBackupDirectorySize(DirectoryInfo dir)
        {
            long res = 0;
            foreach (var file in dir.GetFiles())
            {
                res += file.Length;
            }

            return res;
        }
        public static string GetStrSize(long size)
        {
            string[] units = new string[] { "B", "KiB", "MiB", "GiB", "TiB" };

            double sz = size;
            int nb = 0;
            while (sz > 1024 && nb < units.Length)
            {
                sz = sz / 1024;
                nb++;
            }

            return String.Format("{0:F2} {1}", sz, units[nb]);
        }

    }
}
