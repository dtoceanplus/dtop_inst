// This is DTOceanPlus Monitor Tool.
// This tool monitors the different components of the DTOceanPlus modules on Windows.
// Copyright (C) 2021 OPEN CASCADE
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
// License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.
﻿
namespace dtop_mon
{
    partial class FrmOptions
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmOptions));
            this.lblBrowser = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tbxBrowser = new System.Windows.Forms.TextBox();
            this.tbxUrlPattern = new System.Windows.Forms.TextBox();
            this.ckxTopMost = new System.Windows.Forms.CheckBox();
            this.btnOK = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnSelectBrowser = new System.Windows.Forms.Button();
            this.ofdSelectBrowser = new System.Windows.Forms.OpenFileDialog();
            this.btnSelectDtopInstall = new System.Windows.Forms.Button();
            this.tbxDtopInstall = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.fbdSelectInstallDir = new System.Windows.Forms.FolderBrowserDialog();
            this.gbxInstall = new System.Windows.Forms.GroupBox();
            this.ckxKeepBackground = new System.Windows.Forms.CheckBox();
            this.ckxDeactivateWarning = new System.Windows.Forms.CheckBox();
            this.gbxInstall.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblBrowser
            // 
            this.lblBrowser.AutoSize = true;
            this.lblBrowser.Location = new System.Drawing.Point(32, 121);
            this.lblBrowser.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblBrowser.Name = "lblBrowser";
            this.lblBrowser.Size = new System.Drawing.Size(45, 13);
            this.lblBrowser.TabIndex = 2;
            this.lblBrowser.Text = "Browser";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(20, 54);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(43, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Domain";
            // 
            // tbxBrowser
            // 
            this.tbxBrowser.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbxBrowser.Location = new System.Drawing.Point(87, 118);
            this.tbxBrowser.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.tbxBrowser.Name = "tbxBrowser";
            this.tbxBrowser.ReadOnly = true;
            this.tbxBrowser.Size = new System.Drawing.Size(311, 20);
            this.tbxBrowser.TabIndex = 3;
            // 
            // tbxUrlPattern
            // 
            this.tbxUrlPattern.Location = new System.Drawing.Point(75, 51);
            this.tbxUrlPattern.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.tbxUrlPattern.Name = "tbxUrlPattern";
            this.tbxUrlPattern.ReadOnly = true;
            this.tbxUrlPattern.Size = new System.Drawing.Size(311, 20);
            this.tbxUrlPattern.TabIndex = 3;
            // 
            // ckxTopMost
            // 
            this.ckxTopMost.AutoSize = true;
            this.ckxTopMost.Location = new System.Drawing.Point(87, 147);
            this.ckxTopMost.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.ckxTopMost.Name = "ckxTopMost";
            this.ckxTopMost.Size = new System.Drawing.Size(88, 17);
            this.ckxTopMost.TabIndex = 5;
            this.ckxTopMost.Text = "Keep on &Top";
            this.ckxTopMost.UseVisualStyleBackColor = true;
            // 
            // btnOK
            // 
            this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOK.Location = new System.Drawing.Point(293, 182);
            this.btnOK.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(69, 25);
            this.btnOK.TabIndex = 7;
            this.btnOK.Text = "&OK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(365, 182);
            this.btnCancel.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(69, 25);
            this.btnCancel.TabIndex = 0;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // btnSelectBrowser
            // 
            this.btnSelectBrowser.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSelectBrowser.Image = global::dtop_mon.Properties.Resources.folder;
            this.btnSelectBrowser.Location = new System.Drawing.Point(401, 116);
            this.btnSelectBrowser.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.btnSelectBrowser.Name = "btnSelectBrowser";
            this.btnSelectBrowser.Size = new System.Drawing.Size(28, 25);
            this.btnSelectBrowser.TabIndex = 4;
            this.btnSelectBrowser.UseVisualStyleBackColor = true;
            this.btnSelectBrowser.Click += new System.EventHandler(this.btnSelectBrowser_Click);
            // 
            // ofdSelectBrowser
            // 
            this.ofdSelectBrowser.AddExtension = false;
            this.ofdSelectBrowser.Filter = "Executables|*.exe|All Files|*.*";
            this.ofdSelectBrowser.Title = "Select Browser";
            // 
            // btnSelectDtopInstall
            // 
            this.btnSelectDtopInstall.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSelectDtopInstall.Image = global::dtop_mon.Properties.Resources.folder;
            this.btnSelectDtopInstall.Location = new System.Drawing.Point(389, 20);
            this.btnSelectDtopInstall.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.btnSelectDtopInstall.Name = "btnSelectDtopInstall";
            this.btnSelectDtopInstall.Size = new System.Drawing.Size(28, 25);
            this.btnSelectDtopInstall.TabIndex = 2;
            this.btnSelectDtopInstall.UseVisualStyleBackColor = true;
            this.btnSelectDtopInstall.Click += new System.EventHandler(this.btnSelectDtopInstall_Click);
            // 
            // tbxDtopInstall
            // 
            this.tbxDtopInstall.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbxDtopInstall.Location = new System.Drawing.Point(75, 22);
            this.tbxDtopInstall.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.tbxDtopInstall.Name = "tbxDtopInstall";
            this.tbxDtopInstall.ReadOnly = true;
            this.tbxDtopInstall.Size = new System.Drawing.Size(311, 20);
            this.tbxDtopInstall.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 25);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(50, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Install Dir";
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.AddExtension = false;
            this.openFileDialog1.Filter = "Executables|*.exe|All Files|*.*";
            this.openFileDialog1.Title = "Select Browser";
            // 
            // gbxInstall
            // 
            this.gbxInstall.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbxInstall.Controls.Add(this.tbxDtopInstall);
            this.gbxInstall.Controls.Add(this.btnSelectDtopInstall);
            this.gbxInstall.Controls.Add(this.label1);
            this.gbxInstall.Controls.Add(this.label2);
            this.gbxInstall.Controls.Add(this.tbxUrlPattern);
            this.gbxInstall.Location = new System.Drawing.Point(12, 12);
            this.gbxInstall.Name = "gbxInstall";
            this.gbxInstall.Size = new System.Drawing.Size(422, 86);
            this.gbxInstall.TabIndex = 1;
            this.gbxInstall.TabStop = false;
            this.gbxInstall.Text = "Installation";
            // 
            // ckxKeepBackground
            // 
            this.ckxKeepBackground.AutoSize = true;
            this.ckxKeepBackground.Location = new System.Drawing.Point(87, 170);
            this.ckxKeepBackground.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.ckxKeepBackground.Name = "ckxKeepBackground";
            this.ckxKeepBackground.Size = new System.Drawing.Size(123, 17);
            this.ckxKeepBackground.TabIndex = 6;
            this.ckxKeepBackground.Text = "Keep in &Background";
            this.ckxKeepBackground.UseVisualStyleBackColor = true;
            // 
            // ckxDeactivateWarning
            // 
            this.ckxDeactivateWarning.AutoSize = true;
            this.ckxDeactivateWarning.Location = new System.Drawing.Point(87, 193);
            this.ckxDeactivateWarning.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.ckxDeactivateWarning.Name = "ckxDeactivateWarning";
            this.ckxDeactivateWarning.Size = new System.Drawing.Size(152, 17);
            this.ckxDeactivateWarning.TabIndex = 8;
            this.ckxDeactivateWarning.Text = "Deactive Backup Warning";
            this.ckxDeactivateWarning.UseVisualStyleBackColor = true;
            // 
            // FrmOptions
            // 
            this.AcceptButton = this.btnOK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(446, 219);
            this.Controls.Add(this.ckxDeactivateWarning);
            this.Controls.Add(this.ckxKeepBackground);
            this.Controls.Add(this.gbxInstall);
            this.Controls.Add(this.btnSelectBrowser);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.ckxTopMost);
            this.Controls.Add(this.tbxBrowser);
            this.Controls.Add(this.lblBrowser);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmOptions";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Options";
            this.Load += new System.EventHandler(this.FrmOptions_Load);
            this.gbxInstall.ResumeLayout(false);
            this.gbxInstall.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblBrowser;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbxBrowser;
        private System.Windows.Forms.TextBox tbxUrlPattern;
        private System.Windows.Forms.CheckBox ckxTopMost;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnSelectBrowser;
        private System.Windows.Forms.OpenFileDialog ofdSelectBrowser;
        private System.Windows.Forms.Button btnSelectDtopInstall;
        private System.Windows.Forms.TextBox tbxDtopInstall;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.FolderBrowserDialog fbdSelectInstallDir;
        private System.Windows.Forms.GroupBox gbxInstall;
        private System.Windows.Forms.CheckBox ckxKeepBackground;
        private System.Windows.Forms.CheckBox ckxDeactivateWarning;
    }
}