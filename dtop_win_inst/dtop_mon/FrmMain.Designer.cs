// This is DTOceanPlus Monitor Tool.
// This tool monitors the different components of the DTOceanPlus modules on Windows.
// Copyright (C) 2021 OPEN CASCADE
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
// License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.
﻿
namespace dtop_mon
{
    partial class FrmMain
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmMain));
            this.notifyIcon1 = new System.Windows.Forms.NotifyIcon(this.components);
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.optionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmCheckUpdates = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.tsmQuit = new System.Windows.Forms.ToolStripMenuItem();
            this.lvwServices = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tpgStatus = new System.Windows.Forms.TabPage();
            this.pnlServices = new System.Windows.Forms.Panel();
            this.pnlTopServices = new System.Windows.Forms.Panel();
            this.btnOpenPortainer = new System.Windows.Forms.Button();
            this.btnRefreshStatus = new System.Windows.Forms.Button();
            this.btnLaunchDTO = new System.Windows.Forms.Button();
            this.tpgImages = new System.Windows.Forms.TabPage();
            this.lvwImages = new System.Windows.Forms.ListView();
            this.colImageID = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colImageRepository = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colImageTag = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colImageSize = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colImageDate = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colImageDigest = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.tpgServices = new System.Windows.Forms.TabPage();
            this.tpgStats = new System.Windows.Forms.TabPage();
            this.lvwStats = new System.Windows.Forms.ListView();
            this.colStatName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colStatCpu = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colStatMemUsage = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colStatMemTotal = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.pnlStats = new System.Windows.Forms.Panel();
            this.ckxAutoRefresh = new System.Windows.Forms.CheckBox();
            this.btnRefresh = new System.Windows.Forms.Button();
            this.tpgInfo = new System.Windows.Forms.TabPage();
            this.lvwDockerInfo = new System.Windows.Forms.ListView();
            this.colInfoName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colInfoValue = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lvwDisk = new System.Windows.Forms.ListView();
            this.colDiskType = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colDiskTotal = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colDiskActive = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colDiskSize = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colDiskReclaimable = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.pnlLog = new System.Windows.Forms.Panel();
            this.btnCleanUp = new System.Windows.Forms.Button();
            this.btnDebug = new System.Windows.Forms.Button();
            this.tpgBackup = new System.Windows.Forms.TabPage();
            this.lvwBackups = new System.Windows.Forms.ListView();
            this.colBackupName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colBAckupSize = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colBackupDate = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.pnlBackup = new System.Windows.Forms.Panel();
            this.btnLoadArchive = new System.Windows.Forms.Button();
            this.btnArchive = new System.Windows.Forms.Button();
            this.btnBackup = new System.Windows.Forms.Button();
            this.btnBackupDelete = new System.Windows.Forms.Button();
            this.btnRestore = new System.Windows.Forms.Button();
            this.btnBackupRefresh = new System.Windows.Forms.Button();
            this.btnReset = new System.Windows.Forms.Button();
            this.lblBackupWarning = new System.Windows.Forms.Label();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.sfdSaveLog = new System.Windows.Forms.SaveFileDialog();
            this.timerStat = new System.Windows.Forms.Timer(this.components);
            this.bgwStats = new System.ComponentModel.BackgroundWorker();
            this.lblTotalMem = new System.Windows.Forms.Label();
            this.contextMenuStrip1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tpgStatus.SuspendLayout();
            this.pnlTopServices.SuspendLayout();
            this.tpgImages.SuspendLayout();
            this.tpgServices.SuspendLayout();
            this.tpgStats.SuspendLayout();
            this.pnlStats.SuspendLayout();
            this.tpgInfo.SuspendLayout();
            this.pnlLog.SuspendLayout();
            this.tpgBackup.SuspendLayout();
            this.pnlBackup.SuspendLayout();
            this.SuspendLayout();
            // 
            // notifyIcon1
            // 
            this.notifyIcon1.ContextMenuStrip = this.contextMenuStrip1;
            this.notifyIcon1.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon1.Icon")));
            this.notifyIcon1.Text = "DTOPmon";
            this.notifyIcon1.Visible = true;
            this.notifyIcon1.DoubleClick += new System.EventHandler(this.notifyIcon1_DoubleClick);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.optionsToolStripMenuItem,
            this.tsmCheckUpdates,
            this.toolStripSeparator1,
            this.tsmQuit});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.ShowImageMargin = false;
            this.contextMenuStrip1.Size = new System.Drawing.Size(147, 76);
            // 
            // optionsToolStripMenuItem
            // 
            this.optionsToolStripMenuItem.Name = "optionsToolStripMenuItem";
            this.optionsToolStripMenuItem.ShowShortcutKeys = false;
            this.optionsToolStripMenuItem.Size = new System.Drawing.Size(146, 22);
            this.optionsToolStripMenuItem.Text = "Options";
            this.optionsToolStripMenuItem.Click += new System.EventHandler(this.optionsToolStripMenuItem_Click);
            // 
            // tsmCheckUpdates
            // 
            this.tsmCheckUpdates.Name = "tsmCheckUpdates";
            this.tsmCheckUpdates.Size = new System.Drawing.Size(146, 22);
            this.tsmCheckUpdates.Text = "Check for Updates";
            this.tsmCheckUpdates.Click += new System.EventHandler(this.tsmCheckUpdates_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(143, 6);
            // 
            // tsmQuit
            // 
            this.tsmQuit.Name = "tsmQuit";
            this.tsmQuit.Size = new System.Drawing.Size(146, 22);
            this.tsmQuit.Text = "Quit";
            this.tsmQuit.Click += new System.EventHandler(this.tsmQuit_Click);
            // 
            // lvwServices
            // 
            this.lvwServices.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3,
            this.columnHeader4,
            this.columnHeader5,
            this.columnHeader6});
            this.lvwServices.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lvwServices.FullRowSelect = true;
            this.lvwServices.GridLines = true;
            this.lvwServices.HideSelection = false;
            this.lvwServices.Location = new System.Drawing.Point(2, 3);
            this.lvwServices.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.lvwServices.MultiSelect = false;
            this.lvwServices.Name = "lvwServices";
            this.lvwServices.Size = new System.Drawing.Size(407, 503);
            this.lvwServices.TabIndex = 4;
            this.lvwServices.UseCompatibleStateImageBehavior = false;
            this.lvwServices.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "ID";
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Name";
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Mode";
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Replicas";
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "Image";
            // 
            // columnHeader6
            // 
            this.columnHeader6.Text = "Ports";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tpgStatus);
            this.tabControl1.Controls.Add(this.tpgImages);
            this.tabControl1.Controls.Add(this.tpgServices);
            this.tabControl1.Controls.Add(this.tpgStats);
            this.tabControl1.Controls.Add(this.tpgInfo);
            this.tabControl1.Controls.Add(this.tpgBackup);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.ImageList = this.imageList1;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(419, 536);
            this.tabControl1.TabIndex = 0;
            this.tabControl1.SelectedIndexChanged += new System.EventHandler(this.tabControl1_SelectedIndexChanged);
            // 
            // tpgStatus
            // 
            this.tpgStatus.Controls.Add(this.pnlServices);
            this.tpgStatus.Controls.Add(this.pnlTopServices);
            this.tpgStatus.ImageIndex = 0;
            this.tpgStatus.Location = new System.Drawing.Point(4, 23);
            this.tpgStatus.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.tpgStatus.Name = "tpgStatus";
            this.tpgStatus.Padding = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.tpgStatus.Size = new System.Drawing.Size(411, 509);
            this.tpgStatus.TabIndex = 0;
            this.tpgStatus.Text = "Status";
            // 
            // pnlServices
            // 
            this.pnlServices.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlServices.Location = new System.Drawing.Point(2, 43);
            this.pnlServices.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.pnlServices.Name = "pnlServices";
            this.pnlServices.Size = new System.Drawing.Size(407, 463);
            this.pnlServices.TabIndex = 1;
            // 
            // pnlTopServices
            // 
            this.pnlTopServices.Controls.Add(this.btnOpenPortainer);
            this.pnlTopServices.Controls.Add(this.btnRefreshStatus);
            this.pnlTopServices.Controls.Add(this.btnLaunchDTO);
            this.pnlTopServices.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlTopServices.Location = new System.Drawing.Point(2, 3);
            this.pnlTopServices.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.pnlTopServices.Name = "pnlTopServices";
            this.pnlTopServices.Size = new System.Drawing.Size(407, 40);
            this.pnlTopServices.TabIndex = 0;
            // 
            // btnOpenPortainer
            // 
            this.btnOpenPortainer.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOpenPortainer.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnOpenPortainer.Location = new System.Drawing.Point(293, 6);
            this.btnOpenPortainer.Name = "btnOpenPortainer";
            this.btnOpenPortainer.Size = new System.Drawing.Size(110, 30);
            this.btnOpenPortainer.TabIndex = 3;
            this.btnOpenPortainer.Text = "&Portainer";
            this.btnOpenPortainer.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnOpenPortainer.UseVisualStyleBackColor = true;
            this.btnOpenPortainer.Click += new System.EventHandler(this.btnOpenPortainer_Click);
            // 
            // btnRefreshStatus
            // 
            this.btnRefreshStatus.Image = global::dtop_mon.Properties.Resources.arrow_refresh;
            this.btnRefreshStatus.Location = new System.Drawing.Point(5, 6);
            this.btnRefreshStatus.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.btnRefreshStatus.Name = "btnRefreshStatus";
            this.btnRefreshStatus.Size = new System.Drawing.Size(77, 30);
            this.btnRefreshStatus.TabIndex = 0;
            this.btnRefreshStatus.Text = "Refresh";
            this.btnRefreshStatus.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnRefreshStatus.UseVisualStyleBackColor = true;
            this.btnRefreshStatus.Click += new System.EventHandler(this.btnRefreshStatus_Click);
            // 
            // btnLaunchDTO
            // 
            this.btnLaunchDTO.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnLaunchDTO.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnLaunchDTO.Location = new System.Drawing.Point(180, 6);
            this.btnLaunchDTO.Name = "btnLaunchDTO";
            this.btnLaunchDTO.Size = new System.Drawing.Size(108, 30);
            this.btnLaunchDTO.TabIndex = 2;
            this.btnLaunchDTO.Text = "Launch &DTO+";
            this.btnLaunchDTO.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnLaunchDTO.UseVisualStyleBackColor = true;
            this.btnLaunchDTO.Click += new System.EventHandler(this.btnLaunchDTO_Click);
            // 
            // tpgImages
            // 
            this.tpgImages.Controls.Add(this.lvwImages);
            this.tpgImages.ImageIndex = 4;
            this.tpgImages.Location = new System.Drawing.Point(4, 23);
            this.tpgImages.Margin = new System.Windows.Forms.Padding(2);
            this.tpgImages.Name = "tpgImages";
            this.tpgImages.Padding = new System.Windows.Forms.Padding(2);
            this.tpgImages.Size = new System.Drawing.Size(411, 509);
            this.tpgImages.TabIndex = 4;
            this.tpgImages.Text = "Images";
            this.tpgImages.UseVisualStyleBackColor = true;
            // 
            // lvwImages
            // 
            this.lvwImages.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colImageID,
            this.colImageRepository,
            this.colImageTag,
            this.colImageSize,
            this.colImageDate,
            this.colImageDigest});
            this.lvwImages.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lvwImages.FullRowSelect = true;
            this.lvwImages.GridLines = true;
            this.lvwImages.HideSelection = false;
            this.lvwImages.Location = new System.Drawing.Point(2, 2);
            this.lvwImages.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.lvwImages.MultiSelect = false;
            this.lvwImages.Name = "lvwImages";
            this.lvwImages.Size = new System.Drawing.Size(407, 505);
            this.lvwImages.TabIndex = 5;
            this.lvwImages.UseCompatibleStateImageBehavior = false;
            this.lvwImages.View = System.Windows.Forms.View.Details;
            this.lvwImages.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this.lvwImages_ColumnClick);
            // 
            // colImageID
            // 
            this.colImageID.Text = "ID";
            // 
            // colImageRepository
            // 
            this.colImageRepository.Text = "Repository";
            // 
            // colImageTag
            // 
            this.colImageTag.Text = "Mode";
            // 
            // colImageSize
            // 
            this.colImageSize.Text = "Size";
            this.colImageSize.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // colImageDate
            // 
            this.colImageDate.Text = "Date";
            // 
            // colImageDigest
            // 
            this.colImageDigest.Text = "Digest";
            // 
            // tpgServices
            // 
            this.tpgServices.Controls.Add(this.lvwServices);
            this.tpgServices.ImageIndex = 1;
            this.tpgServices.Location = new System.Drawing.Point(4, 23);
            this.tpgServices.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.tpgServices.Name = "tpgServices";
            this.tpgServices.Padding = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.tpgServices.Size = new System.Drawing.Size(411, 509);
            this.tpgServices.TabIndex = 1;
            this.tpgServices.Text = "Services";
            // 
            // tpgStats
            // 
            this.tpgStats.Controls.Add(this.lvwStats);
            this.tpgStats.Controls.Add(this.pnlStats);
            this.tpgStats.ImageIndex = 2;
            this.tpgStats.Location = new System.Drawing.Point(4, 23);
            this.tpgStats.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.tpgStats.Name = "tpgStats";
            this.tpgStats.Size = new System.Drawing.Size(411, 509);
            this.tpgStats.TabIndex = 2;
            this.tpgStats.Text = "Stats";
            // 
            // lvwStats
            // 
            this.lvwStats.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colStatName,
            this.colStatCpu,
            this.colStatMemUsage,
            this.colStatMemTotal});
            this.lvwStats.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lvwStats.FullRowSelect = true;
            this.lvwStats.HideSelection = false;
            this.lvwStats.Location = new System.Drawing.Point(0, 40);
            this.lvwStats.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.lvwStats.MultiSelect = false;
            this.lvwStats.Name = "lvwStats";
            this.lvwStats.Size = new System.Drawing.Size(411, 469);
            this.lvwStats.TabIndex = 1;
            this.lvwStats.UseCompatibleStateImageBehavior = false;
            this.lvwStats.View = System.Windows.Forms.View.Details;
            this.lvwStats.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this.lvwStats_ColumnClick);
            // 
            // colStatName
            // 
            this.colStatName.Text = "Name";
            this.colStatName.Width = 200;
            // 
            // colStatCpu
            // 
            this.colStatCpu.Text = "CPU %";
            this.colStatCpu.Width = 100;
            // 
            // colStatMemUsage
            // 
            this.colStatMemUsage.Text = "Mem Usage";
            this.colStatMemUsage.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.colStatMemUsage.Width = 100;
            // 
            // colStatMemTotal
            // 
            this.colStatMemTotal.Text = "Limit";
            this.colStatMemTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // pnlStats
            // 
            this.pnlStats.Controls.Add(this.lblTotalMem);
            this.pnlStats.Controls.Add(this.ckxAutoRefresh);
            this.pnlStats.Controls.Add(this.btnRefresh);
            this.pnlStats.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlStats.Location = new System.Drawing.Point(0, 0);
            this.pnlStats.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.pnlStats.Name = "pnlStats";
            this.pnlStats.Size = new System.Drawing.Size(411, 40);
            this.pnlStats.TabIndex = 0;
            // 
            // ckxAutoRefresh
            // 
            this.ckxAutoRefresh.AutoSize = true;
            this.ckxAutoRefresh.Location = new System.Drawing.Point(99, 14);
            this.ckxAutoRefresh.Name = "ckxAutoRefresh";
            this.ckxAutoRefresh.Size = new System.Drawing.Size(88, 17);
            this.ckxAutoRefresh.TabIndex = 1;
            this.ckxAutoRefresh.Text = "Auto Refresh";
            this.ckxAutoRefresh.UseVisualStyleBackColor = true;
            this.ckxAutoRefresh.CheckedChanged += new System.EventHandler(this.ckxAutoRefresh_CheckedChanged);
            // 
            // btnRefresh
            // 
            this.btnRefresh.Image = global::dtop_mon.Properties.Resources.arrow_refresh;
            this.btnRefresh.Location = new System.Drawing.Point(2, 6);
            this.btnRefresh.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(77, 30);
            this.btnRefresh.TabIndex = 0;
            this.btnRefresh.Text = "Refresh";
            this.btnRefresh.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnRefresh.UseVisualStyleBackColor = true;
            this.btnRefresh.Click += new System.EventHandler(this.btnStatrefresh_Click);
            // 
            // tpgInfo
            // 
            this.tpgInfo.Controls.Add(this.lvwDockerInfo);
            this.tpgInfo.Controls.Add(this.lvwDisk);
            this.tpgInfo.Controls.Add(this.pnlLog);
            this.tpgInfo.ImageIndex = 3;
            this.tpgInfo.Location = new System.Drawing.Point(4, 23);
            this.tpgInfo.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.tpgInfo.Name = "tpgInfo";
            this.tpgInfo.Size = new System.Drawing.Size(411, 509);
            this.tpgInfo.TabIndex = 3;
            this.tpgInfo.Text = "Info";
            // 
            // lvwDockerInfo
            // 
            this.lvwDockerInfo.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colInfoName,
            this.colInfoValue});
            this.lvwDockerInfo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lvwDockerInfo.FullRowSelect = true;
            this.lvwDockerInfo.GridLines = true;
            this.lvwDockerInfo.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.lvwDockerInfo.HideSelection = false;
            this.lvwDockerInfo.Location = new System.Drawing.Point(0, 40);
            this.lvwDockerInfo.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.lvwDockerInfo.MultiSelect = false;
            this.lvwDockerInfo.Name = "lvwDockerInfo";
            this.lvwDockerInfo.Size = new System.Drawing.Size(411, 283);
            this.lvwDockerInfo.TabIndex = 6;
            this.lvwDockerInfo.UseCompatibleStateImageBehavior = false;
            this.lvwDockerInfo.View = System.Windows.Forms.View.Details;
            // 
            // colInfoName
            // 
            this.colInfoName.Text = "Parameter";
            this.colInfoName.Width = 111;
            // 
            // colInfoValue
            // 
            this.colInfoValue.Text = "Value";
            this.colInfoValue.Width = 275;
            // 
            // lvwDisk
            // 
            this.lvwDisk.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colDiskType,
            this.colDiskTotal,
            this.colDiskActive,
            this.colDiskSize,
            this.colDiskReclaimable});
            this.lvwDisk.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lvwDisk.FullRowSelect = true;
            this.lvwDisk.GridLines = true;
            this.lvwDisk.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.lvwDisk.HideSelection = false;
            this.lvwDisk.Location = new System.Drawing.Point(0, 323);
            this.lvwDisk.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.lvwDisk.MultiSelect = false;
            this.lvwDisk.Name = "lvwDisk";
            this.lvwDisk.Size = new System.Drawing.Size(411, 186);
            this.lvwDisk.TabIndex = 5;
            this.lvwDisk.UseCompatibleStateImageBehavior = false;
            this.lvwDisk.View = System.Windows.Forms.View.Details;
            // 
            // colDiskType
            // 
            this.colDiskType.Text = "Type";
            this.colDiskType.Width = 118;
            // 
            // colDiskTotal
            // 
            this.colDiskTotal.Text = "Total";
            this.colDiskTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.colDiskTotal.Width = 53;
            // 
            // colDiskActive
            // 
            this.colDiskActive.Text = "Active";
            this.colDiskActive.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.colDiskActive.Width = 55;
            // 
            // colDiskSize
            // 
            this.colDiskSize.Text = "Size";
            this.colDiskSize.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // colDiskReclaimable
            // 
            this.colDiskReclaimable.Text = "Reclaimable";
            this.colDiskReclaimable.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.colDiskReclaimable.Width = 94;
            // 
            // pnlLog
            // 
            this.pnlLog.Controls.Add(this.btnCleanUp);
            this.pnlLog.Controls.Add(this.btnDebug);
            this.pnlLog.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlLog.Location = new System.Drawing.Point(0, 0);
            this.pnlLog.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.pnlLog.Name = "pnlLog";
            this.pnlLog.Size = new System.Drawing.Size(411, 40);
            this.pnlLog.TabIndex = 0;
            // 
            // btnCleanUp
            // 
            this.btnCleanUp.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCleanUp.Image = global::dtop_mon.Properties.Resources.broom;
            this.btnCleanUp.Location = new System.Drawing.Point(322, 6);
            this.btnCleanUp.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.btnCleanUp.Name = "btnCleanUp";
            this.btnCleanUp.Size = new System.Drawing.Size(82, 30);
            this.btnCleanUp.TabIndex = 3;
            this.btnCleanUp.Text = "Clean up";
            this.btnCleanUp.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnCleanUp.UseVisualStyleBackColor = true;
            this.btnCleanUp.Click += new System.EventHandler(this.btnCleanUp_Click);
            // 
            // btnDebug
            // 
            this.btnDebug.Image = global::dtop_mon.Properties.Resources.hammer;
            this.btnDebug.Location = new System.Drawing.Point(7, 6);
            this.btnDebug.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.btnDebug.Name = "btnDebug";
            this.btnDebug.Size = new System.Drawing.Size(91, 30);
            this.btnDebug.TabIndex = 0;
            this.btnDebug.Text = "Debug Info";
            this.btnDebug.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnDebug.UseVisualStyleBackColor = true;
            this.btnDebug.Click += new System.EventHandler(this.btnDebug_Click);
            // 
            // tpgBackup
            // 
            this.tpgBackup.Controls.Add(this.lvwBackups);
            this.tpgBackup.Controls.Add(this.pnlBackup);
            this.tpgBackup.Controls.Add(this.lblBackupWarning);
            this.tpgBackup.ImageIndex = 5;
            this.tpgBackup.Location = new System.Drawing.Point(4, 23);
            this.tpgBackup.Margin = new System.Windows.Forms.Padding(2);
            this.tpgBackup.Name = "tpgBackup";
            this.tpgBackup.Padding = new System.Windows.Forms.Padding(2);
            this.tpgBackup.Size = new System.Drawing.Size(411, 509);
            this.tpgBackup.TabIndex = 5;
            this.tpgBackup.Text = "Backup";
            // 
            // lvwBackups
            // 
            this.lvwBackups.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colBackupName,
            this.colBAckupSize,
            this.colBackupDate});
            this.lvwBackups.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lvwBackups.FullRowSelect = true;
            this.lvwBackups.HideSelection = false;
            this.lvwBackups.Location = new System.Drawing.Point(2, 98);
            this.lvwBackups.MultiSelect = false;
            this.lvwBackups.Name = "lvwBackups";
            this.lvwBackups.Size = new System.Drawing.Size(407, 409);
            this.lvwBackups.TabIndex = 5;
            this.lvwBackups.UseCompatibleStateImageBehavior = false;
            this.lvwBackups.View = System.Windows.Forms.View.Details;
            this.lvwBackups.SelectedIndexChanged += new System.EventHandler(this.lvwBackups_SelectedIndexChanged);
            // 
            // colBackupName
            // 
            this.colBackupName.Text = "Backup";
            this.colBackupName.Width = 186;
            // 
            // colBAckupSize
            // 
            this.colBAckupSize.Text = "Size";
            // 
            // colBackupDate
            // 
            this.colBackupDate.Text = "Creation";
            this.colBackupDate.Width = 163;
            // 
            // pnlBackup
            // 
            this.pnlBackup.Controls.Add(this.btnLoadArchive);
            this.pnlBackup.Controls.Add(this.btnArchive);
            this.pnlBackup.Controls.Add(this.btnBackup);
            this.pnlBackup.Controls.Add(this.btnBackupDelete);
            this.pnlBackup.Controls.Add(this.btnRestore);
            this.pnlBackup.Controls.Add(this.btnBackupRefresh);
            this.pnlBackup.Controls.Add(this.btnReset);
            this.pnlBackup.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlBackup.Location = new System.Drawing.Point(2, 22);
            this.pnlBackup.Name = "pnlBackup";
            this.pnlBackup.Size = new System.Drawing.Size(407, 76);
            this.pnlBackup.TabIndex = 6;
            // 
            // btnLoadArchive
            // 
            this.btnLoadArchive.Location = new System.Drawing.Point(80, 39);
            this.btnLoadArchive.Margin = new System.Windows.Forms.Padding(2);
            this.btnLoadArchive.Name = "btnLoadArchive";
            this.btnLoadArchive.Size = new System.Drawing.Size(87, 29);
            this.btnLoadArchive.TabIndex = 6;
            this.btnLoadArchive.Text = "Import";
            this.btnLoadArchive.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnLoadArchive.UseVisualStyleBackColor = true;
            this.btnLoadArchive.Click += new System.EventHandler(this.btnLoadArchive_Click);
            // 
            // btnArchive
            // 
            this.btnArchive.Location = new System.Drawing.Point(4, 39);
            this.btnArchive.Margin = new System.Windows.Forms.Padding(2);
            this.btnArchive.Name = "btnArchive";
            this.btnArchive.Size = new System.Drawing.Size(72, 29);
            this.btnArchive.TabIndex = 5;
            this.btnArchive.Text = "Archive";
            this.btnArchive.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnArchive.UseVisualStyleBackColor = true;
            this.btnArchive.Click += new System.EventHandler(this.btnArchive_Click);
            // 
            // btnBackup
            // 
            this.btnBackup.Image = global::dtop_mon.Properties.Resources.arrow_down;
            this.btnBackup.Location = new System.Drawing.Point(3, 6);
            this.btnBackup.Margin = new System.Windows.Forms.Padding(2);
            this.btnBackup.Name = "btnBackup";
            this.btnBackup.Size = new System.Drawing.Size(72, 29);
            this.btnBackup.TabIndex = 0;
            this.btnBackup.Text = "Backup";
            this.btnBackup.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnBackup.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnBackup.UseVisualStyleBackColor = true;
            this.btnBackup.Click += new System.EventHandler(this.btnBackup_Click);
            // 
            // btnBackupDelete
            // 
            this.btnBackupDelete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnBackupDelete.Image = global::dtop_mon.Properties.Resources.cross;
            this.btnBackupDelete.Location = new System.Drawing.Point(253, 6);
            this.btnBackupDelete.Margin = new System.Windows.Forms.Padding(2);
            this.btnBackupDelete.Name = "btnBackupDelete";
            this.btnBackupDelete.Size = new System.Drawing.Size(72, 29);
            this.btnBackupDelete.TabIndex = 3;
            this.btnBackupDelete.Text = "Delete";
            this.btnBackupDelete.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnBackupDelete.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnBackupDelete.UseVisualStyleBackColor = true;
            this.btnBackupDelete.Click += new System.EventHandler(this.btnBackupDelete_Click);
            // 
            // btnRestore
            // 
            this.btnRestore.Image = global::dtop_mon.Properties.Resources.arrow_up;
            this.btnRestore.Location = new System.Drawing.Point(79, 6);
            this.btnRestore.Margin = new System.Windows.Forms.Padding(2);
            this.btnRestore.Name = "btnRestore";
            this.btnRestore.Size = new System.Drawing.Size(72, 29);
            this.btnRestore.TabIndex = 1;
            this.btnRestore.Text = "Restore";
            this.btnRestore.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnRestore.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnRestore.UseVisualStyleBackColor = true;
            this.btnRestore.Click += new System.EventHandler(this.btnRestore_Click);
            // 
            // btnBackupRefresh
            // 
            this.btnBackupRefresh.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnBackupRefresh.Image = global::dtop_mon.Properties.Resources.arrow_refresh;
            this.btnBackupRefresh.Location = new System.Drawing.Point(329, 6);
            this.btnBackupRefresh.Margin = new System.Windows.Forms.Padding(2);
            this.btnBackupRefresh.Name = "btnBackupRefresh";
            this.btnBackupRefresh.Size = new System.Drawing.Size(72, 29);
            this.btnBackupRefresh.TabIndex = 4;
            this.btnBackupRefresh.Text = "Refresh";
            this.btnBackupRefresh.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnBackupRefresh.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnBackupRefresh.UseVisualStyleBackColor = true;
            this.btnBackupRefresh.Click += new System.EventHandler(this.btnBackupRefresh_Click);
            // 
            // btnReset
            // 
            this.btnReset.Image = global::dtop_mon.Properties.Resources.folder_page_white;
            this.btnReset.Location = new System.Drawing.Point(155, 6);
            this.btnReset.Margin = new System.Windows.Forms.Padding(2);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(72, 29);
            this.btnReset.TabIndex = 2;
            this.btnReset.Text = "Reset";
            this.btnReset.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnReset.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnReset.UseVisualStyleBackColor = true;
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            // 
            // lblBackupWarning
            // 
            this.lblBackupWarning.BackColor = System.Drawing.SystemColors.Info;
            this.lblBackupWarning.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblBackupWarning.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBackupWarning.ForeColor = System.Drawing.Color.Red;
            this.lblBackupWarning.Location = new System.Drawing.Point(2, 2);
            this.lblBackupWarning.Name = "lblBackupWarning";
            this.lblBackupWarning.Size = new System.Drawing.Size(407, 20);
            this.lblBackupWarning.TabIndex = 7;
            this.lblBackupWarning.Text = "WARNING: Backup\\Restore must be used with caution";
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "magnifier.png");
            this.imageList1.Images.SetKeyName(1, "application_cascade.png");
            this.imageList1.Images.SetKeyName(2, "chart_curve.png");
            this.imageList1.Images.SetKeyName(3, "information.png");
            this.imageList1.Images.SetKeyName(4, "database.png");
            this.imageList1.Images.SetKeyName(5, "folder.png");
            // 
            // sfdSaveLog
            // 
            this.sfdSaveLog.Filter = "Text Files|*.txt|All Files|*.*";
            this.sfdSaveLog.Title = "Generate Log Info";
            // 
            // timerStat
            // 
            this.timerStat.Interval = 1000;
            this.timerStat.Tick += new System.EventHandler(this.timerStat_Tick);
            // 
            // bgwStats
            // 
            this.bgwStats.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bgwStats_DoWork);
            this.bgwStats.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bgwStats_RunWorkerCompleted);
            // 
            // lblTotalMem
            // 
            this.lblTotalMem.AutoSize = true;
            this.lblTotalMem.Location = new System.Drawing.Point(193, 15);
            this.lblTotalMem.Name = "lblTotalMem";
            this.lblTotalMem.Size = new System.Drawing.Size(83, 13);
            this.lblTotalMem.TabIndex = 2;
            this.lblTotalMem.Text = "Total Memory = ";
            // 
            // FrmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.ClientSize = new System.Drawing.Size(419, 536);
            this.Controls.Add(this.tabControl1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(2);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(366, 575);
            this.Name = "FrmMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "DTOceanPlus Monitor";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmMain_FormClosing);
            this.Load += new System.EventHandler(this.FrmMain_Load);
            this.Shown += new System.EventHandler(this.FrmMain_Shown);
            this.contextMenuStrip1.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tpgStatus.ResumeLayout(false);
            this.pnlTopServices.ResumeLayout(false);
            this.tpgImages.ResumeLayout(false);
            this.tpgServices.ResumeLayout(false);
            this.tpgStats.ResumeLayout(false);
            this.pnlStats.ResumeLayout(false);
            this.pnlStats.PerformLayout();
            this.tpgInfo.ResumeLayout(false);
            this.pnlLog.ResumeLayout(false);
            this.tpgBackup.ResumeLayout(false);
            this.pnlBackup.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.NotifyIcon notifyIcon1;
        private System.Windows.Forms.ListView lvwServices;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.ColumnHeader columnHeader6;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tpgStatus;
        private System.Windows.Forms.TabPage tpgServices;
        private System.Windows.Forms.TabPage tpgStats;
        private System.Windows.Forms.TabPage tpgInfo;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem tsmQuit;
        private System.Windows.Forms.Panel pnlLog;
        private System.Windows.Forms.Button btnDebug;
        private System.Windows.Forms.SaveFileDialog sfdSaveLog;
        private System.Windows.Forms.ListView lvwStats;
        private System.Windows.Forms.ColumnHeader colStatName;
        private System.Windows.Forms.ColumnHeader colStatCpu;
        private System.Windows.Forms.ColumnHeader colStatMemUsage;
        private System.Windows.Forms.Panel pnlStats;
        private System.Windows.Forms.Button btnRefresh;
        private System.Windows.Forms.Panel pnlServices;
        private System.Windows.Forms.Panel pnlTopServices;
        private System.Windows.Forms.Button btnRefreshStatus;
        private System.Windows.Forms.ListView lvwDisk;
        private System.Windows.Forms.ColumnHeader colDiskType;
        private System.Windows.Forms.ColumnHeader colDiskTotal;
        private System.Windows.Forms.ColumnHeader colDiskActive;
        private System.Windows.Forms.ColumnHeader colDiskSize;
        private System.Windows.Forms.ColumnHeader colDiskReclaimable;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.Button btnCleanUp;
        private System.Windows.Forms.ToolStripMenuItem optionsToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem tsmCheckUpdates;
        private System.Windows.Forms.CheckBox ckxAutoRefresh;
        private System.Windows.Forms.Timer timerStat;
        private System.Windows.Forms.ColumnHeader colStatMemTotal;
        private System.ComponentModel.BackgroundWorker bgwStats;
        private System.Windows.Forms.Button btnOpenPortainer;
        private System.Windows.Forms.Button btnLaunchDTO;
        private System.Windows.Forms.TabPage tpgImages;
        private System.Windows.Forms.ListView lvwImages;
        private System.Windows.Forms.ColumnHeader colImageID;
        private System.Windows.Forms.ColumnHeader colImageRepository;
        private System.Windows.Forms.ColumnHeader colImageTag;
        private System.Windows.Forms.ColumnHeader colImageSize;
        private System.Windows.Forms.ColumnHeader colImageDate;
        private System.Windows.Forms.ColumnHeader colImageDigest;
        private System.Windows.Forms.TabPage tpgBackup;
        private System.Windows.Forms.Button btnBackup;
        private System.Windows.Forms.Button btnReset;
        private System.Windows.Forms.Button btnRestore;
        private System.Windows.Forms.ListView lvwBackups;
        private System.Windows.Forms.ColumnHeader colBackupName;
        private System.Windows.Forms.ColumnHeader colBackupDate;
        private System.Windows.Forms.Button btnBackupRefresh;
        private System.Windows.Forms.Button btnBackupDelete;
        private System.Windows.Forms.ColumnHeader colBAckupSize;
        private System.Windows.Forms.ListView lvwDockerInfo;
        private System.Windows.Forms.ColumnHeader colInfoName;
        private System.Windows.Forms.ColumnHeader colInfoValue;
        private System.Windows.Forms.Panel pnlBackup;
        private System.Windows.Forms.Label lblBackupWarning;
        private System.Windows.Forms.Button btnLoadArchive;
        private System.Windows.Forms.Button btnArchive;
        private System.Windows.Forms.Label lblTotalMem;
    }
}

