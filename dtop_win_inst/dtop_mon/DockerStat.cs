// This is DTOceanPlus Monitor Tool.
// This tool monitors the different components of the DTOceanPlus modules on Windows.
// Copyright (C) 2021 OPEN CASCADE
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
// License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dtop_mon
{
    public class DockerStat
    {
        public string Name { get; set; }
        public string CpuPercent { get; set; }
        public float MemUsage { get; set; }
        public float MemLimit { get; set; }

        private DockerStat()
        {
        }

        public static DockerStat FromLine(string line)
        {
            var elts = line.Split(';');
            // trunc name
            elts[0] = elts[0].Substring(0, elts[0].IndexOf("."));
            // 
            var mem = elts[2].Split('/');
            float mu = Utils.ParseSize(mem[0].Trim());
            float tot = Utils.ParseSize(mem[1].Trim());

            return new DockerStat() { Name = elts[0], CpuPercent = elts[1], MemUsage = mu, MemLimit = tot };
        }

        public string[] AsArray()
        {
            return new string[] { Name, CpuPercent, MemUsage.ToString(), MemLimit.ToString() };
        }
    }
}
