// This is DTOceanPlus Monitor Tool.
// This tool monitors the different components of the DTOceanPlus modules on Windows.
// Copyright (C) 2021 OPEN CASCADE
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
// License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace dtop_mon
{
    static class Program
    {
        static Mutex s_mutex = new Mutex(true, "DTOCEANPLUS_MONITOR_INSTANCE");

        /// <summary>
        ///  The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            if (!s_mutex.WaitOne(TimeSpan.Zero, true))
            {
                MessageBox.Show("DTOceanPlus Monitor is already running!", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            else
            {
                //Application.SetHighDpiMode(HighDpiMode.SystemAware);
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);

                Application.ThreadException += Application_ThreadException;

                if (Properties.Settings.Default.UpgradeSettings)
                {
                    Properties.Settings.Default.Upgrade();
                    Properties.Settings.Default.UpgradeSettings = false;
                }

                Application.Run(new FrmMain());
                s_mutex.ReleaseMutex();
            }
        }

        private static void Application_ThreadException(object sender, System.Threading.ThreadExceptionEventArgs e)
        {
            string message = e.Exception.Message;
#if DEBUG
            Exception exc = e.Exception.InnerException;
            while (exc != null)
            {
                message += Environment.NewLine + exc.Message;
                exc = exc.InnerException;
            }

            message += Environment.NewLine + e.Exception.StackTrace;
#endif

            MessageBox.Show(message, "DTOceanPlus Monitor", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
    }
}
