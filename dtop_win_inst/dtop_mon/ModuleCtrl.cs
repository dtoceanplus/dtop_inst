// This is DTOceanPlus Monitor Tool.
// This tool monitors the different components of the DTOceanPlus modules on Windows.
// Copyright (C) 2021 OPEN CASCADE
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
// License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.
﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace dtop_mon
{
    public partial class ModuleCtrl : UserControl
    {
        private DtopModule m_module = null;
        private Dictionary<string, Label> m_labels = new Dictionary<string, Label>();

        public ModuleCtrl()
        {
            InitializeComponent();
            btnPause.Visible = false;
        }

        public void SetModule(DtopModule module)
        {
            m_module = module;
            m_module.StackStatusChanged += this.Module_StackStatusChanged;
            linkLabel1.Text = module.ShortName.ToUpper();
            toolTip1.SetToolTip(linkLabel1, module.Name);

            int pos = 77;
            int w = 72;
            foreach (var service in m_module.Services)
            {
                Label lbl = new Label();
                lbl.Text = "      " + service.ShortName;
                lbl.AutoSize = false;
                lbl.Size = new Size(w, 24);
                lbl.Location = new Point(pos, 0);
                pos += w + 2;
                lbl.TextAlign = ContentAlignment.MiddleLeft;
                lbl.ImageAlign = ContentAlignment.MiddleLeft;
                lbl.ImageList = imageList1;
                lbl.ImageIndex = SetStatus(service.Replicas);

                m_labels[service.Name] = lbl;
                service.ReplicasChanged += this.Service_ReplicasChanged;

                this.Controls.Add(lbl);
            }
        }

        private void Module_StackStatusChanged(object sender, StackStatusEventArgs e)
        {
            btnPause.Enabled = e.Status;
        }

        private void Service_ReplicasChanged(object sender, EventArgs e)
        {
            DtopService s = sender as DtopService;
            if (s != null && m_labels.ContainsKey(s.Name))
            {
                m_labels[s.Name].ImageIndex = SetStatus(s.Replicas);
            }
        }

        public void ResetStatus()
        {
            foreach (var ctrl in this.Controls)
            {
                if (ctrl is Label)
                {
                    Label lbl = ctrl as Label;
                    lbl.ImageIndex = 0;
                }
            }
        }

        private int SetStatus(string replicas)
        {
            if (replicas == "0/1") return 2;
            if (replicas == "1/1") return 1;
            return 0;
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if (m_module != null)
            {
                m_module.OpenModule();
            }
        }

        private void btnPause_Click(object sender, EventArgs e)
        {
            // stack rm
            this.UseWaitCursor = true;
            DockerCommand.RemoveStack(m_module.ShortName);
            btnPause.Enabled = false;
            this.UseWaitCursor = false;
        }
    }
}
