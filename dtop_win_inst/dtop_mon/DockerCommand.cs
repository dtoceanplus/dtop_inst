// This is DTOceanPlus Monitor Tool.
// This tool monitors the different components of the DTOceanPlus modules on Windows.
// Copyright (C) 2021 OPEN CASCADE
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
// License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.
﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dtop_mon
{
    /// <summary>
    /// Manage Docker Commands.
    /// </summary>
    public static class DockerCommand
    {
        /// <summary>
        /// Runs a Docker command.
        /// </summary>
        /// <param name="command">The command with all the parameters.</param>
        /// <returns>Array of lines from the output.</returns>
        private static string[] RunCommand(string command, bool safe = false)
        {
            List<string> res = new List<string>();
            Process proc = new Process()
            {
                StartInfo = new ProcessStartInfo()
                {
                    FileName = "docker", // TODO parameter
                    Arguments = command,
                    UseShellExecute = false,
                    CreateNoWindow = true,
                    RedirectStandardError = true,
                    RedirectStandardOutput = true,
                }
            };

            proc.Start();
            while (!proc.StandardOutput.EndOfStream)
            {
                res.Add(proc.StandardOutput.ReadLine());
            }

            proc.WaitForExit();
            if (proc.ExitCode != 0)
            {
                string err = String.Format("Error when execution command: {0}\n\n{1}", command, proc.StandardError.ReadToEnd());
                if (!safe) throw new InvalidOperationException(err);
            }

            return res.ToArray();
        }

        public static bool IsDockerRunning()
        {
            bool res = false;
            try
            {
                RunCommand("info");
                res = true;
            }
            catch (InvalidOperationException)
            {
                res = false;
            }

            return res;
        }

        /// <summary>
        /// Get Docker Info.
        /// </summary>
        /// <returns>Instance of DockerInfo.</returns>
        public static DockerInfo Info()
        {
            DockerInfo di = new DockerInfo();
            di.Version = DockerCommand.Version();

            string[] info = RunCommand("info", true);
            foreach (string line in info)
            {
                string tline = line.Trim();
                if (tline.StartsWith("Total Memory:")) di.Memory = Utils.ParseInfoLine(tline);
                else if (tline.StartsWith("CPUs:")) di.CPUs = Utils.ParseInfoLine(tline);
                else if (tline.StartsWith("Swarm:")) di.Swarm = Utils.ParseInfoLine(tline);
            }

            return di;
        }
        /// <summary>
        /// Get Raw information.
        /// </summary>
        /// <returns></returns>
        public static string[] RawInfo()
        {
            return RunCommand("info");
        }

        public static List<DtopService> ListServices()
        {
            List<DtopService> services = new List<DtopService>();
            string[] res = RunCommand("service ls --format {{.ID}};{{.Name}};{{.Mode}};{{.Replicas}};{{.Image}};{{.Ports}}", true);

            foreach (var r in res)
            {
                DtopService service = DtopService.Parse(r);
                services.Add(service);
            }

            return services;
        }
        public static List<DtopImage> ListImages()
        {
            List<DtopImage> images = new List<DtopImage>();
            string[] res = RunCommand("image ls --format {{.ID}};{{.Repository}};{{.Tag}};{{.Size}};{{.CreatedAt}};{{.Digest}}", true);

            foreach (var r in res)
            {
                images.Add(DtopImage.Parse(r));
            }

            return images;
        }
        public static DockerStat[] Stats()
        {
            var stats = RunCommand("stats --no-stream --format {{.Name}};{{.CPUPerc}};{{.MemUsage}}", true);
            List<DockerStat> res = new List<DockerStat>();
            foreach (var stat in stats)
            {
                res.Add(DockerStat.FromLine(stat));
            }
            return res.ToArray();
        }

        public static string Version()
        {
            return RunCommand("-v")[0];
        }

        public static List<string[]> DiskUsage()
        {
            List<string[]> res = new List<string[]>();
            var df = RunCommand("system df --format {{.Type}};{{.TotalCount}};{{.Active}};{{.Size}};{{.Reclaimable}}", true);

            foreach (var d in df)
            {
                res.Add(d.Split(';'));
            }

            return res;
        }

        public static int CleanUnusedContainers()
        {
            var mm = RunCommand("container ls --filter status=exited -aq");
            int nb = mm.Length;
            if (nb > 0)
            {
                string containers = String.Join(" ", mm);
                RunCommand(String.Format("container rm {0}", containers));
            }

            return nb;
        }

        private static string GetContainerId(string container)
        {
            var res = RunCommand(String.Format("ps -q -f name={0}", container));
            if (res.Length == 0) return null;
            return res[0].Trim();
        }

        public static bool BackupFile(string containerName, string source, string dest)
        {
            string id = GetContainerId(containerName);
            if (String.IsNullOrEmpty(id)) return false;

            RunCommand(String.Format("cp {0}:{1} \"{2}\"", id, source, dest));
            return true;
        }
        public static bool BackupSqlDump(string containerName, string dumpCommand, string sqlFile)
        {
            string id = GetContainerId(containerName);
            if (String.IsNullOrEmpty(id)) return false;

            var res = RunCommand(String.Format("exec {0} bash -c \"{1} > /tmp/dump.sql\"", id, dumpCommand));
            RunCommand(String.Format("cp {0}:/tmp/dump.sql \"{1}\"", id, sqlFile));
            RunCommand(String.Format("exec {0} rm /tmp/dump.sql", id));
            return true;
        }

        public static bool BackupArchive(string containerName, string wkdir, string source, string dest)
        {
            string id = GetContainerId(containerName);
            if (String.IsNullOrEmpty(id)) return false;

            RunCommand(String.Format("exec {0} tar -czf {0}.tar.gz {1}", id, source));
            RunCommand(String.Format("cp {0}:/app/{0}.tar.gz \"{1}\"", id, dest));
            RunCommand(String.Format("exec {0} rm {0}.tar.gz", id));

            return true;
        }

        public static bool RestoreFile(string containerName, string source, string dest)
        {
            string id = GetContainerId(containerName);
            if (String.IsNullOrEmpty(id)) return false;

            RunCommand(String.Format("exec {0} rm {1}", id, dest));
            RunCommand(String.Format("cp \"{1}\" {0}:{2}", id, source, dest));
            return true;
        }
        public static bool RestoreSqlDump(string containerName, string sqlFile, string restoreCommand)
        {
            string id = GetContainerId(containerName);
            if (String.IsNullOrEmpty(id)) return false;

            string localSqlFile = "/tmp/db.sql";
            string dbFlag = (restoreCommand.StartsWith("mysql")) ? " < " : " -f ";
            
            RunCommand(String.Format("cp \"{1}\" {0}:{2}", id, sqlFile, localSqlFile));
            RunCommand(String.Format("exec {0} bash -c \"{1} {2} {3}\"", id, restoreCommand, dbFlag, localSqlFile));
            RunCommand(String.Format("exec {0} rm {1}", id, localSqlFile));

            return true;
        }
        public static bool RestoreArchive(string containerName, string archive, string wkdir, string folder)
        {
            string id = GetContainerId(containerName);
            if (String.IsNullOrEmpty(id)) return false;

            RunCommand(String.Format("exec {0} bash -c \"cd {1}; rm -rf *\"", id, folder));
            RunCommand(String.Format("cp \"{1}\" {0}:{2}/{0}.tar.gz", id, archive, wkdir));
            RunCommand(String.Format("exec {0} tar -xzf {0}.tar.gz", id));
            RunCommand(String.Format("exec {0} rm {0}.tar.gz", id));

            return true;
        }
        public static bool ResetArchive(string containerName, string init, string workingDir, string folder)
        {
            string id = GetContainerId(containerName);
            if (String.IsNullOrEmpty(id)) return false;

            RunCommand(String.Format("exec {0} bash -c \"cd {1}; rm -rf *\"", id, folder));

            return true;
        }
        public static bool ResetFile(string containerName, string init, string file)
        {
            string id = GetContainerId(containerName);
            if (String.IsNullOrEmpty(id)) return false;

            RunCommand(String.Format("exec {0} rm {1}", id, file));
            RunCommand(String.Format("cp \"{1}\" {0}:{2}", id, init, file));

            return true;
        }
        public static bool ResetSql(string containerName, string init, string restoreCommand)
        {
            return RestoreSqlDump(containerName, init, restoreCommand);
        }

        public static string[] ListStacks()
        {
            return RunCommand("stack ls --format {{.Name}}");
        }
        internal static void RemoveStack(string stackName)
        {
            RunCommand(String.Format("stack rm {0}", stackName));
        }
    }
}
