// This is DTOceanPlus Monitor Tool.
// This tool monitors the different components of the DTOceanPlus modules on Windows.
// Copyright (C) 2021 OPEN CASCADE
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
// License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.
using ICSharpCode.SharpZipLib.Core;
using ICSharpCode.SharpZipLib.GZip;
using ICSharpCode.SharpZipLib.Zip;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace dtop_mon
{
    public partial class FrmMain : Form
    {
        private List<DtopModule> m_modules = new List<DtopModule>();
        private DockerInfo m_info = null;
        /// <summary>
        /// Flag to explicilty close the application.
        /// </summary>
        private bool m_doClose = false;

        public FrmMain()
        {
            InitializeComponent();
            timerStat.Interval = Properties.Settings.Default.StatRefresh;
            lvwStats.ListViewItemSorter = new ListViewColumnSorter();
            lvwImages.ListViewItemSorter = new ListViewColumnSorter();
            SetOptions();
            ConfigureModules();

            if (String.IsNullOrEmpty(Properties.Settings.Default.DtopPath))
            {
                // if dtop install path not set, use current exe directory
                Properties.Settings.Default.DtopPath = System.AppContext.BaseDirectory;
            }
        }

        private void FrmMain_Load(object sender, EventArgs e)
        {
            if (!DockerCommand.IsDockerRunning())
            {
                MessageBox.Show("Docker is not Running!\nPlease Start Docker before launching this application.",
                    this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);

                pnlBackup.Enabled = false;
            }
        }

        private void FrmMain_Shown(object sender, EventArgs e)
        {
            if (!this.DesignMode)
            {
                BuildModulesPanel();
                UpdateStatus();
                this.ShowInTaskbar = true;
            }
        }

        private void ConfigureModules()
        {
            m_modules.Add(new DtopModule("Main Module", "mm",
                new SqlBackup("mm_database",
                              "mysqldump -uroot -pmysql main_db",
                              "mysql -umm_user -pmm_pass --database=main_db",
                              Path.Combine(GetDataDir("mm"), "scripts", "mm_mysql_6_db_reinitialize_sql")),
                "mm_database", "mm_digitalrep"));
            m_modules.Add(new DtopModule("Energy Capture", "ec",
                new CopyFileBackup("ec_backend", "/app/src/instance/energycapt.db", "energycapt.db_initial"),
                "ec_redis", "ec_worker"));
            m_modules.Add(new DtopModule("Energy Delivery", "ed",
                new CopyFileBackup("ed_backend", "/app/instance/energydeliv.db", "energydeliv.db_initial")));
            m_modules.Add(new DtopModule("Energy Transformation", "et",
                new CopyFileBackup("et_backend", "/app/src/dtop_energytransf/Databases/ET_DB_test.db", "et.db_initial")));
            m_modules.Add(new DtopModule("Logistics and Marine Operations", "lmo",
                new CopyFileBackup("lmo_backend", "/app/src/dtop_lmo/instance/lmo.db", "lmo.db_initial")));
            m_modules.Add(new DtopModule("Machine Characterization", "mc",
                new CopyFileBackup("mc_backend", "/app/src/instance/machine.db", "machine.db_initial"),
                "mc_redis", "mc_worker"));
            m_modules.Add(new DtopModule("Site Characterisation", "sc",
                new ArchiveBackup("sc_backend", "/app", "storage")));
            m_modules.Add(new DtopModule("Stage Gate", "sg",
                new CopyFileBackup("sg_backend", "/app/src/instance/stagegate.db", "stagegate.db_initial")));
            m_modules.Add(new DtopModule("Station Keeping", "sk",
                new ArchiveBackup("sk_backend", "/app/", "storage")));
            m_modules.Add(new DtopModule("Structured Innovation", "si",
                new CopyFileBackup("si_backend", "/data/structinn.sqlite", "structinn.sqlite_initial")));
            m_modules.Add(new DtopModule("System Performance and Energy Yield", "spey",
                new CopyFileBackup("spey_backend", "/app/src/dtop_spey/databases/spey.db", "spey.db_initial")));
            m_modules.Add(new DtopModule("System RAMS", "rams",
                new CopyFileBackup("rams_backend", "/app/src/rams_db/rams.db", "rams.db_initial"),
                "redis", "worker"));
            m_modules.Add(new DtopModule("System Environmental and Social Acceptance", "esa",
                new ArchiveBackup("esa_backend", "/app", "storage")));
            m_modules.Add(new DtopModule("System Lifetime Costs", "slc",
                new CopyFileBackup("slc_backend", "/app/data/slc.db", "slc.db_initial")));
            m_modules.Add(new DtopModule("Catalogues", "cm",
                new SqlBackup("cm_backend",
                    "PGPASSWORD=postgres pg_dump --username=postgres --host=localhost --clean dtop-catalog",
                    "PGPASSWORD=postgres psql -U postgres -h localhost -d dtop-catalog -q",
                    Path.Combine(GetDataDir("cm"), "scripts", "cm_postgres_1_db_reinitialize_sql"))
                ));
        }
        private string GetDataDir(string module)
        {
            if (String.IsNullOrEmpty(module))
                return Path.Combine(Properties.Settings.Default.DtopPath, "_volume", "data");

            return Path.Combine(Properties.Settings.Default.DtopPath, "_volume", "data", module);
        }

        private void tsmQuit_Click(object sender, EventArgs e)
        {
            m_doClose = true;
            this.Close();
        }

        /// <summary>
        /// Dynamically creates modules' controls on the first page.
        /// </summary>
        private void BuildModulesPanel()
        {        
            int x = 0, y = 0;
            foreach (DtopModule module in m_modules)
            {
                ModuleCtrl mc = new ModuleCtrl();
                mc.SetModule(module);
                mc.Location = new Point(x, y);
                mc.Width = pnlServices.Width - 5;
                mc.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
                pnlServices.Controls.Add(mc);
                y += 26;
            }
        }
        /*
        private void ResetModules()
        {
            foreach (var ctrl in pnlServices.Controls)
            {
                if (ctrl is ModuleCtrl)
                {
                    var mc = ctrl as ModuleCtrl;
                    mc.ResetStatus();
                }
            }
        }*/

        private void FrmMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!m_doClose && Properties.Settings.Default.KeepInBackground)
            {
                // cancel close
                e.Cancel = true;
                // hide
                this.TopMost = false;
                this.Hide();
                this.ShowInTaskbar = false;
            }
        }

        private void notifyIcon1_DoubleClick(object sender, EventArgs e)
        {
            this.TopMost = Properties.Settings.Default.TopMost;
            this.Show();
        }

        private void UpdateStatus()
        {
            if (this.DesignMode) return;

            //
            UpdateDockerInfo(false);
            if (!m_info.SwarmActive)
            {
                UpdateIcon(-1);
                return;
            }

            var services = DockerCommand.ListServices();
            var stacks = DockerCommand.ListStacks();
            int nbErrors = services.Count(s => !s.IsRunning);
            UpdateIcon(nbErrors);

            try
            {
                tpgStatus.SuspendLayout();
                foreach (DtopModule module in m_modules)
                {
                    module.SetStackStatus(stacks.Contains(module.ShortName));
                    foreach (var service in module.Services)
                    {
                        var ss = services.FirstOrDefault(s => s.Name == service.Name);
                        if (ss != null)
                        {
                            service.Update(ss);
                        }
                        else
                        {
                            service.Reset();
                        }
                    }
                }
            }
            finally
            {
                tpgStatus.ResumeLayout();
            }
        }

        private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (tabControl1.SelectedTab != tpgStats) timerStat.Stop();
            else if (ckxAutoRefresh.Checked) timerStat.Start();

            if (tabControl1.SelectedTab == tpgStatus)
            {
                UpdateStatus();
            }
            else if (tabControl1.SelectedTab == tpgServices)
            {
                UpdateServices();
            }
            else if (tabControl1.SelectedTab == tpgImages)
            {
                UpdateImages();
            }
            else if (tabControl1.SelectedTab == tpgInfo)
            {
                UpdateDockerInfo(true);
            }
            else if (tabControl1.SelectedTab == tpgBackup)
            {
                RefreshBackups();
            }
        }

        private void UpdateIcon(int nbErrors)
        {
            if (nbErrors < 0) notifyIcon1.Icon = Properties.Resources.dtop_warning;
            else if (nbErrors > 0) notifyIcon1.Icon = Properties.Resources.dtop_error;
            else notifyIcon1.Icon = Properties.Resources.dtop;
        }

        private void UpdateServices()
        {
            if (this.DesignMode) return;
            try
            {
                lvwServices.SuspendLayout();
                lvwServices.Items.Clear();
                
                lvwServices.Groups.Clear();
                foreach (var module in m_modules)
                {
                    lvwServices.Groups.Add(module.ShortName, module.Name);
                }
                lvwServices.Groups.Add("Other", "Other");

                var services = DockerCommand.ListServices();
                int nbErrors = services.Count(s => !s.IsRunning);
                UpdateIcon(nbErrors);
                foreach (var service in services)
                {
                    ListViewItem item = new ListViewItem(new string[] { service.ID, service.Name, service.Mode, service.Replicas, service.Version, service.Ports });
                    if (!service.IsRunning)
                    {
                        item.ForeColor = Color.Red;
                    }
                    // find modules
                    var module = m_modules.FirstOrDefault(m => m.HasService(service.Name));
                    if (module == null) item.Group = lvwServices.Groups["Other"];
                    else item.Group = lvwServices.Groups[module.ShortName];

                    lvwServices.Items.Add(item);
                }
            }
            finally
            {
                lvwServices.AutoResizeColumns(ColumnHeaderAutoResizeStyle.ColumnContent);
                lvwServices.ResumeLayout();
            }
        }
        private void UpdateImages()
        {
            if (this.DesignMode) return;
            try
            {
                lvwImages.SuspendLayout();
                lvwImages.Items.Clear();

                var images = DockerCommand.ListImages();
                foreach (var image in images)
                {
                    ListViewItem item = new ListViewItem(image.AsStringArray());
                    lvwImages.Items.Add(item);
                }
            }
            finally
            {
                lvwImages.AutoResizeColumns(ColumnHeaderAutoResizeStyle.ColumnContent);
                lvwImages.ResumeLayout();
            }
        }
        private void UpdateDockerInfo(bool diskUsage)
        {
            if (this.DesignMode) return;
            if (m_info == null)
            {
                m_info = DockerCommand.Info();
                lvwDockerInfo.Items.Clear();
                AddDockerInfoItem("CPUs", m_info.CPUs);
                AddDockerInfoItem("Memory", m_info.Memory);
                AddDockerInfoItem("Swarm", m_info.Swarm);
                AddDockerInfoItem("Version", m_info.Version);
            }

            if (diskUsage)
            {
                var dd = DockerCommand.DiskUsage();
                lvwDisk.Items.Clear();
                foreach (var d in dd)
                {
                    lvwDisk.Items.Add(new ListViewItem(d));
                }
                //lvwDisk.AutoResizeColumns(ColumnHeaderAutoResizeStyle.ColumnContent);
            }
        }

        private void AddDockerInfoItem(string name, string value)
        {
            ListViewItem item = new ListViewItem(new string[] { name, value });
            lvwDockerInfo.Items.Add(item);
        }

        private void btnDebug_Click(object sender, EventArgs e)
        {
            sfdSaveLog.InitialDirectory = Properties.Settings.Default.DtopPath;
            sfdSaveLog.FileName = String.Format("dtop_debug_{0}.txt", DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss"));
            if (sfdSaveLog.ShowDialog() == DialogResult.OK)
            {
                string log = sfdSaveLog.FileName;
                using (StreamWriter sw = new StreamWriter(log))
                {
                    if (!DockerCommand.IsDockerRunning())
                    {
                        sw.WriteLine("Docker is not running!");
                    }
                    else
                    {
                        var info = DockerCommand.RawInfo();
                        sw.WriteLine("=== Info ===");
                        foreach (string ii in info) sw.WriteLine(ii);
                        sw.WriteLine("");

                        var services = DockerCommand.ListServices();
                        sw.WriteLine("=== Services ===");
                        sw.WriteLine(DtopService.WriteTitle());
                        foreach (var ss in services) sw.WriteLine(ss.Write());
                        sw.WriteLine("");

                        var images = DockerCommand.ListImages();
                        sw.WriteLine("=== Images ===");
                        string format = "{0,-12}\t{1,-90}\t{2,-6}\t{3,-6}\t{4,-30}\t{5}";
                        sw.WriteLine(format, "ID", "Repository", "Tag", "Size", "CreatedAt", "Digest");
                        foreach (var img in images) sw.WriteLine(format, img);
                        sw.WriteLine("");
                    }
                }
            }
        }

        private void btnStatrefresh_Click(object sender, EventArgs e)
        {
            if (bgwStats.IsBusy) return;
            this.Cursor = Cursors.WaitCursor;
            try
            {
                UpdateStats();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void UpdateStats()
        {
            if (ckxAutoRefresh.Checked) timerStat.Stop();
            bgwStats.RunWorkerAsync();
        }

        private void btnRefreshStatus_Click(object sender, EventArgs e)
        {
            UpdateStatus();
        }

        private void btnCleanUp_Click(object sender, EventArgs e)
        {
            int nb = DockerCommand.CleanUnusedContainers();
            if (nb > 0)
            {
                MessageBox.Show(String.Format("{0} containers removed", nb), this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void optionsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmOptions frm = new FrmOptions();
            frm.TopMost = Properties.Settings.Default.TopMost;
            frm.ShowDialog();
            SetOptions(); // always because of TopMost
        }

        private void SetOptions()
        {
            this.TopMost = Properties.Settings.Default.TopMost;
            lblBackupWarning.Visible = !Properties.Settings.Default.DeActiveWarning;
        }

        private void tsmCheckUpdates_Click(object sender, EventArgs e)
        {
            var nv = UpdateChecker.CheckUpdates();
            if (nv == null)
            {
                notifyIcon1.ShowBalloonTip(2000, "Error", "Unable to check for updates", ToolTipIcon.Error);
            }
            else
            {
                List<DtopModule> toUpdate = new List<DtopModule>();
                foreach (var module in m_modules)
                {
                    if (String.IsNullOrEmpty(module.Version)) continue;
                    if (nv.ContainsKey(module.ShortName) && nv[module.ShortName] != module.Version)
                    {
                        toUpdate.Add(module);
                    }
                }

                if (toUpdate.Count == 0)
                {
                    this.TopMost = false;
                    MessageBox.Show("No updates available", this.Text, MessageBoxButtons.OK);
                    this.TopMost = Properties.Settings.Default.TopMost;
                }
                else
                {
                    StringBuilder bld = new StringBuilder();
                    bld.AppendLine("Available updates for:");
                    foreach (var module in toUpdate)
                    {
                        bld.AppendLine(String.Format("  {0}: {1} => {2}", module.ShortName.ToUpper(),
                            module.Version, nv[module.ShortName]));
                    }

                    //notifyIcon1.ShowBalloonTip(2000, "Updates", bld.ToString(), ToolTipIcon.Info);
                    notifyIcon1.Icon = Properties.Resources.dtop_update;

                    if (Directory.Exists(Properties.Settings.Default.DtopPath))
                    {
                        string currentEnv = Path.Combine(Properties.Settings.Default.DtopPath, "dtop_inst.env");
                        string newEnv = Path.Combine(Properties.Settings.Default.DtopPath, "dtop_inst.env.update");

                        if (File.Exists(newEnv))
                        {
                            bld.AppendLine().AppendLine("Do you want to update your env file?");
                            this.TopMost = false;
                            if (MessageBox.Show(bld.ToString(), this.Text, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                            {
                                File.Copy(newEnv, currentEnv, true);
                            }
                            this.TopMost = Properties.Settings.Default.TopMost;
                        }
                    }
                }
            }
        }

        private void lvwStats_ColumnClick(object sender, ColumnClickEventArgs e)
        {
            var colSorter = lvwStats.ListViewItemSorter as ListViewColumnSorter;
            if (e.Column == colSorter.SortColumn)
            {
                // Reverse the current sort direction for this column.
                if (colSorter.Order == SortOrder.Ascending)
                {
                    colSorter.Order = SortOrder.Descending;
                }
                else
                {
                    colSorter.Order = SortOrder.Ascending;
                }
            }
            else
            {
                // Set the column number that is to be sorted; default to ascending.
                colSorter.SortColumn = e.Column;
                colSorter.Order = SortOrder.Ascending;
                colSorter.NumericSort = (e.Column == 2 || e.Column == 3);
            }

            // Perform the sort with these new sort options.
            lvwStats.Sort();
        }

        private void ckxAutoRefresh_CheckedChanged(object sender, EventArgs e)
        {
            if (ckxAutoRefresh.Checked)
            {
                UpdateStats();
            }
            else
            {
                timerStat.Stop();
            }
        }

        private void timerStat_Tick(object sender, EventArgs e)
        {
            UpdateStats();
        }

        private void bgwStats_DoWork(object sender, DoWorkEventArgs e)
        {
            var stats = DockerCommand.Stats();
            e.Result = stats;
        }

        private void bgwStats_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            var stats = e.Result as DockerStat[];
            lvwStats.SuspendLayout();
            try
            {
                lvwStats.Items.Clear();
                float totalMemory = 0;
                foreach (var stat in stats)
                {
                    var details = stat.AsArray();
                    totalMemory += float.Parse(details[2]);
                    ListViewItem item = new ListViewItem(details);
                    item.ForeColor = stat.MemUsage >= Properties.Settings.Default.StatMemWarning ? Color.Red : lvwStats.ForeColor;
                    lvwStats.Items.Add(item);
                }
                lvwStats.AutoResizeColumns(ColumnHeaderAutoResizeStyle.ColumnContent);
                lblTotalMem.Text = String.Format("Total Memory = {0:F2} GB", (totalMemory / 1024));
            }
            finally
            {
                lvwStats.ResumeLayout(true);
            }

            if (ckxAutoRefresh.Checked) timerStat.Start();
        }

        private void btnLaunchDTO_Click(object sender, EventArgs e)
        {
            var module = m_modules.FirstOrDefault(m => m.ShortName == "mm");
            if (module != null) module.OpenModule();
        }

        private void btnOpenPortainer_Click(object sender, EventArgs e)
        {
            Utils.OpenUrl(Properties.Settings.Default.PortainerUrl);
        }

        private void lvwImages_ColumnClick(object sender, ColumnClickEventArgs e)
        {
            var colSorter = lvwImages.ListViewItemSorter as ListViewColumnSorter;
            if (e.Column == colSorter.SortColumn)
            {
                // Reverse the current sort direction for this column.
                if (colSorter.Order == SortOrder.Ascending)
                {
                    colSorter.Order = SortOrder.Descending;
                }
                else
                {
                    colSorter.Order = SortOrder.Ascending;
                }
            }
            else
            {
                // Set the column number that is to be sorted; default to ascending.
                colSorter.SortColumn = e.Column;
                colSorter.Order = SortOrder.Ascending;
                colSorter.NumericSort = (e.Column == 3);
            }

            // Perform the sort with these new sort options.
            lvwImages.Sort();
        }

        private void btnBackup_Click(object sender, EventArgs e)
        {
            if (this.TopMost) this.TopMost = false;
            using (FrmBackup frm = new FrmBackup(m_modules, FrmBackup.BackupMode.Backup))
            {
                frm.ShowDialog();
                RefreshBackups();
            }
            this.TopMost = Properties.Settings.Default.TopMost;
        }

        private void btnRestore_Click(object sender, EventArgs e)
        {
            if (lvwBackups.SelectedItems.Count == 0) return;
            string backup = lvwBackups.SelectedItems[0].Text;

            if (this.TopMost) this.TopMost = false;
            if (MessageBox.Show(String.Format("Are you sure you want to resotre backup '{0}'?\nAll current projects will be deleted!", backup),
                                this.Text,
                                MessageBoxButtons.YesNo,
                                MessageBoxIcon.Question) == DialogResult.Yes)
            {
                bool restore = true;
                var errors = ReadManifest(backup, m_modules);
                if (errors.Count > 0)
                {
                    StringBuilder message = new StringBuilder();
                    message.AppendLine("The version of the following modules are different than ones used to create this backup:");
                    foreach (string err in errors)
                    {
                        message.AppendLine(err);
                    }

                    message.AppendLine().AppendLine("Do you want to continue?");

                    if (MessageBox.Show(message.ToString(), this.Text, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                        restore = false;
                }

                if (restore)
                {
                    using (FrmBackup frm = new FrmBackup(m_modules, FrmBackup.BackupMode.Restore))
                    {
                        frm.BackupName = backup;
                        frm.ShowDialog();
                    }
                }
            }
            this.TopMost = Properties.Settings.Default.TopMost;
        }
        private List<string> ReadManifest(string name, IEnumerable<DtopModule> modules)
        {
            List<string> errors = new List<string>();

            string manifest = Path.Combine(Properties.Settings.Default.DtopPath, "_backup", name, "info.txt");
            using (var sr = new StreamReader(manifest))
            {
                string line;
                while ((line = sr.ReadLine()) != null)
                {
                    var elts = line.Trim().Split('=');
                    string code = elts[0];
                    string version = elts[1];
                    var module = modules.FirstOrDefault(m => m.ShortName == code);
                    if (module != null && module.Version != version)
                    {
                        errors.Add(String.Format("{0}: {1} != {2}", module.ShortName, module.Version, version));
                    }
                }
            }

            return errors;
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            if (this.TopMost) this.TopMost = false;
            if (MessageBox.Show("Are you sure you want to Reinitialize the application?\nAll current projects will be deleted!",
                                this.Text,
                                MessageBoxButtons.YesNo,
                                MessageBoxIcon.Question) == DialogResult.Yes)
            {
                using (FrmBackup frm = new FrmBackup(m_modules, FrmBackup.BackupMode.Init))
                {
                    frm.ShowDialog();
                }
            }
            this.TopMost = Properties.Settings.Default.TopMost;
        }

        private void RefreshBackups()
        {
            lvwBackups.Items.Clear();
            btnBackupDelete.Enabled = false;
            btnRestore.Enabled = false;
            btnArchive.Enabled = false;

            string backupDir = Path.Combine(Properties.Settings.Default.DtopPath, "_backup");
            if (!Directory.Exists(backupDir)) return;

            foreach (string dir in Directory.GetDirectories(backupDir))
            {
                DirectoryInfo info = new DirectoryInfo(dir);
                long size = Utils.GetBackupDirectorySize(info);

                ListViewItem item = new ListViewItem(new string[] { Path.GetFileName(dir), Utils.GetStrSize(size), info.LastWriteTime.ToString("yyyy-MM-dd HH:mm:ss") });
                item.Tag = dir;
                lvwBackups.Items.Add(item);
            }
        }

        private void btnBackupDelete_Click(object sender, EventArgs e)
        {
            if (lvwBackups.SelectedItems.Count == 0) return;
            string backup = lvwBackups.SelectedItems[0].Text;
            
            if (MessageBox.Show(String.Format("Are you sure you want to delete '{0}'?", backup),
                this.Text, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                string dir = lvwBackups.SelectedItems[0].Tag.ToString();
                Directory.Delete(dir, true);
                RefreshBackups();
            }
        }

        private void btnBackupRefresh_Click(object sender, EventArgs e)
        {
            RefreshBackups();
        }

        private void lvwBackups_SelectedIndexChanged(object sender, EventArgs e)
        {
            bool sel = (lvwBackups.SelectedItems.Count > 0);
            btnRestore.Enabled = sel;
            btnBackupDelete.Enabled = sel;
            btnArchive.Enabled = sel;
        }

        private void btnArchive_Click(object sender, EventArgs e)
        {
            if (lvwBackups.SelectedItems.Count == 0) return;
            string backup = lvwBackups.SelectedItems[0].Text;
            string backupFolder = lvwBackups.SelectedItems[0].Tag.ToString();

            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Title = "Create an archive";
            sfd.FileName = Path.GetFileName(backup) + ".zip";
            sfd.InitialDirectory = Properties.Settings.Default.DtopPath;
            sfd.Filter = "Archives (*.zip)|*.zip|All Files|*.*";

            if (sfd.ShowDialog() == DialogResult.OK)
            {
                using (FileStream fsOut = File.Create(sfd.FileName))
                using (var zipStream = new ZipOutputStream(fsOut))
                {
                    zipStream.SetLevel(3);
                    int folderOffset = backupFolder.Length + (backupFolder.EndsWith("\\") ? 0 : 1);
                    CompressFolder(backupFolder, zipStream, folderOffset);
                }
            }
        }
        private void CompressFolder(string path, ZipOutputStream zipStream, int folderOffset)
        {
            var files = Directory.GetFiles(path);

            foreach (var filename in files)
            {
                var fi = new FileInfo(filename);
                var entryName = filename.Substring(folderOffset);
                entryName = ZipEntry.CleanName(entryName);
                var newEntry = new ZipEntry(entryName);
                newEntry.DateTime = fi.LastWriteTime;

                newEntry.Size = fi.Length;
                zipStream.PutNextEntry(newEntry);

                var buffer = new byte[4096];
                using (FileStream fsInput = File.OpenRead(filename))
                {
                    StreamUtils.Copy(fsInput, zipStream, buffer);
                }
                zipStream.CloseEntry();
            }

            // Recursively call CompressFolder on all folders in path
            var folders = Directory.GetDirectories(path);
            foreach (var folder in folders)
            {
                CompressFolder(folder, zipStream, folderOffset);
            }
        }

        private void btnLoadArchive_Click(object sender, EventArgs e)
        {
            string outFolder = Path.Combine(Properties.Settings.Default.DtopPath, "_backup");
            using (OpenFileDialog ofd = new OpenFileDialog())
            {
                if (ofd.ShowDialog() == DialogResult.OK)
                {
                    string archivePath = ofd.FileName;
                    outFolder = Path.Combine(outFolder, Path.GetFileNameWithoutExtension(archivePath));

                    using (var fsInput = File.OpenRead(archivePath))
                    using (var zf = new ZipFile(fsInput))
                    {
                        foreach (ZipEntry zipEntry in zf)
                        {
                            if (!zipEntry.IsFile)
                            {
                                continue;
                            }
                            String entryFileName = zipEntry.Name;

                            // Manipulate the output filename here as desired.
                            var fullZipToPath = Path.Combine(outFolder, entryFileName);
                            var directoryName = Path.GetDirectoryName(fullZipToPath);
                            if (directoryName.Length > 0)
                            {
                                Directory.CreateDirectory(directoryName);
                            }

                            // 4K is optimum
                            var buffer = new byte[4096];

                            // Unzip file in buffered chunks. This is just as fast as unpacking
                            // to a buffer the full size of the file, but does not waste memory.
                            // The "using" will close the stream even if an exception occurs.
                            using (var zipStream = zf.GetInputStream(zipEntry))
                            using (Stream fsOutput = File.Create(fullZipToPath))
                            {
                                StreamUtils.Copy(zipStream, fsOutput, buffer);
                            }
                        }
                    }
                    RefreshBackups();
                }
            }
        }
    }

}
