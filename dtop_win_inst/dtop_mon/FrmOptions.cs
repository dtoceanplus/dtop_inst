// This is DTOceanPlus Monitor Tool.
// This tool monitors the different components of the DTOceanPlus modules on Windows.
// Copyright (C) 2021 OPEN CASCADE
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
// License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.
﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace dtop_mon
{
    public partial class FrmOptions : Form
    {
        public FrmOptions()
        {
            InitializeComponent();
        }

        private void FrmOptions_Load(object sender, EventArgs e)
        {
            tbxDtopInstall.Text = Properties.Settings.Default.DtopPath;
            tbxBrowser.Text = Properties.Settings.Default.BrowserExe;
            tbxUrlPattern.Text = Properties.Settings.Default.DtopDomain;
            ckxTopMost.Checked = Properties.Settings.Default.TopMost;
            ckxKeepBackground.Checked = Properties.Settings.Default.KeepInBackground;
            ckxDeactivateWarning.Checked = Properties.Settings.Default.DeActiveWarning;

            if (!String.IsNullOrEmpty(Properties.Settings.Default.DtopPath)) btnSelectDtopInstall.Visible = false;
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            Properties.Settings.Default.BrowserExe = tbxBrowser.Text;
            Properties.Settings.Default.DtopDomain = tbxUrlPattern.Text;
            Properties.Settings.Default.TopMost = ckxTopMost.Checked;
            Properties.Settings.Default.DtopPath = tbxDtopInstall.Text;
            Properties.Settings.Default.KeepInBackground = ckxKeepBackground.Checked;
            Properties.Settings.Default.DeActiveWarning = ckxDeactivateWarning.Checked;
            Properties.Settings.Default.Save();
            this.Close();
        }

        private void btnSelectBrowser_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrWhiteSpace(tbxBrowser.Text))
            {
                ofdSelectBrowser.InitialDirectory = Path.GetDirectoryName(tbxBrowser.Text);
                ofdSelectBrowser.FileName = Path.GetFileName(tbxBrowser.Text);
            }

            if (ofdSelectBrowser.ShowDialog() == DialogResult.OK)
            {
                tbxBrowser.Text = ofdSelectBrowser.FileName;
            }
        }

        private void btnSelectDtopInstall_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrWhiteSpace(tbxDtopInstall.Text))
            {
                fbdSelectInstallDir.SelectedPath = tbxDtopInstall.Text;
            }

            if (fbdSelectInstallDir.ShowDialog() == DialogResult.OK)
            {
                tbxDtopInstall.Text = fbdSelectInstallDir.SelectedPath;
            }

        }
    }
}
