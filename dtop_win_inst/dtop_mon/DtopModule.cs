// This is DTOceanPlus Monitor Tool.
// This tool monitors the different components of the DTOceanPlus modules on Windows.
// Copyright (C) 2021 OPEN CASCADE
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
// License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.
﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dtop_mon
{
    /// <summary>
    /// Represents a DTOceanPlus Module.
    /// </summary>
    public class DtopModule
    {
        /// <summary>Name of the module.</summary>
        public string Name { get; private set; }
        /// <summary>Short name of the module.</summary>
        public string ShortName { get; private set; }

        public ModuleBackup Backup { get; set; }

        public delegate void StackStatusChangedEventHandler(object sender, StackStatusEventArgs e);

        public event StackStatusChangedEventHandler StackStatusChanged;

        /// <summary>
        /// Version of the module.
        /// The version is given by one of the attached services.
        /// </summary>
        public string Version
        {
            get
            {
                if (m_services.Count == 0) return "";
                DtopService s = BackEnd;
                if (s == null)
                {
                    s = FrontEnd;
                    if (s == null)
                        s = m_services[0];
                }

                return s.Version;
            }
        }

        public string DefaultContainer
        {
            get { return ShortName + "_backend"; }
        }

        public DtopService BackEnd
        {
            get { return m_services.FirstOrDefault(s => s.Name.Contains("backend")); }
        }
        public DtopService FrontEnd
        {
            get { return m_services.FirstOrDefault(s => s.Name.Contains("frontend")); }
        }
        
        /// <summary>
        /// URL to open the module.
        /// </summary>
        public string Url
        {
            get { return String.Format("http://{0}.{1}", this.ShortName, Properties.Settings.Default.DtopDomain); }
        }

        /// <summary>
        /// List of services (backend, frontend) attached to this module.
        /// </summary>
        private List<DtopService> m_services = new List<DtopService>();

        /// <summary>
        /// Constructor of DtopModule.
        /// </summary>
        /// <param name="name">The name of the module.</param>
        /// <param name="shortName">The short name of the module.</param>
        /// <param name="services">The list of additional serivces.</param>
        public DtopModule(string name, string shortName, ModuleBackup backup, params string[] services)
        {
            Name = name;
            ShortName = shortName;
            Backup = backup;
            Backup.Module = this;

            AddService(new DtopService(String.Format("{0}_{0}_backend", shortName)));
            AddService(new DtopService(String.Format("{0}_{0}_frontend", shortName)));
            foreach (var service in services)
            {
                if (service == "backend" || service == "frontend") continue;
                AddService(new DtopService(String.Format("{0}_{1}", shortName, service)));
            }
        }

        /// <summary>
        /// Adds a service to this module.
        /// </summary>
        /// <param name="service"></param>
        public void AddService(DtopService service)
        {
            m_services.Add(service);
        }

        /// <summary>
        /// Remove all the services of this module.
        /// </summary>
        public void ClearServices()
        {
            m_services.Clear();
        }

        /// <summary>
        /// Opens a module using its URL.
        /// </summary>
        public void OpenModule()
        {
            Utils.OpenUrl(this.Url);
        }

        /// <summary>
        /// List of services of this module.
        /// </summary>
        public IEnumerable<DtopService> Services
        {
            get { return m_services.AsEnumerable(); }
        }

        internal bool HasService(string name)
        {
            return m_services.Any(s => s.Name == name);
        }

        internal void SetStackStatus(bool status)
        {
            if (StackStatusChanged != null)
            {
                StackStatusChanged(this, new StackStatusEventArgs(status));
            }
        }
    }

    public abstract class ModuleBackup
    {
        internal DtopModule Module { get; set; }
        public string ContainerName { get; set; }

        public ModuleBackup(string container)
        {
            ContainerName = container;
        }
        public abstract bool Backup(string backupDir);
        public abstract bool Restore(string backupDir);
        public abstract bool Reset(string backupDir);
    }

    public class ArchiveBackup : ModuleBackup
    {
        private string Folder { get; set; }
        private string WorkingDir { get; set; }
        public ArchiveBackup(string container, string workingDir, string path) : base(container)
        {
            WorkingDir = workingDir;
            Folder = path;
        }

        public override bool Backup(string backupDir)
        {
            if (Module == null) throw new NullReferenceException("Module not defined");
            string archive = Path.Combine(backupDir, Module.ShortName + ".tar.gz");
            return DockerCommand.BackupArchive(ContainerName, WorkingDir, Folder, archive);
        }

        public override bool Restore(string backupDir)
        {
            if (Module == null) throw new NullReferenceException("Module not defined");
            string archive = Path.Combine(backupDir, Module.ShortName + ".tar.gz");
            if (!File.Exists(archive)) throw new FileNotFoundException(String.Format("Restore archive missing: {0}", archive));
            return DockerCommand.RestoreArchive(ContainerName, archive, WorkingDir, Folder);
        }
        public override bool Reset(string backupDir)
        {
            if (Module == null) throw new NullReferenceException("Module not defined");
            string archive = Path.Combine(backupDir, Module.ShortName + ".tar.gz");
            if (!File.Exists(archive)) throw new FileNotFoundException(String.Format("Restore file missing: {0}", archive));
            return DockerCommand.ResetArchive(ContainerName, archive, WorkingDir, Folder);
        }
    }

    public class CopyFileBackup : ModuleBackup
    {
        private string FilePath { get; set; }
        private string Init { get; set; }
        public CopyFileBackup(string container, string path, string init) : base(container)
        {
            FilePath = path;
            Init = init;
        }
        public override bool Backup(string backupDir)
        {
            if (Module == null) throw new NullReferenceException("Module not defined");
            return DockerCommand.BackupFile(ContainerName, FilePath, Path.Combine(backupDir, Module.ShortName + ".db"));
        }

        public override bool Restore(string backupDir)
        {
            if (Module == null) throw new NullReferenceException("Module not defined");
            string restoreFile = Path.Combine(backupDir, Module.ShortName + ".db");
            if (!File.Exists(restoreFile)) throw new FileNotFoundException(String.Format("Restore file missing: {0}", restoreFile));

            return DockerCommand.RestoreFile(ContainerName, restoreFile, FilePath);
        }
        public override bool Reset(string dataDir)
        {
            if (Module == null) throw new NullReferenceException("Module not defined");
            string resetFile = Path.Combine(dataDir, Module.ShortName, "dumps", Init);
            if (!File.Exists(resetFile)) throw new FileNotFoundException(String.Format("Reset file missing: {0}", resetFile));

            return DockerCommand.ResetFile(ContainerName, resetFile, FilePath);
        }

    }
    public class SqlBackup : ModuleBackup
    {
        private string DumpCommand { get; set; }
        private string RestoreCommand { get; set; }
        private string InitScript { get; set; }
        public SqlBackup(string container, string dump, string restore, string init) : base(container)
        {
            DumpCommand = dump;
            RestoreCommand = restore;
            InitScript = init;
        }
        public override bool Backup(string backupDir)
        {
            if (Module == null) throw new NullReferenceException("Module not defined");
            return DockerCommand.BackupSqlDump(ContainerName, DumpCommand, Path.Combine(backupDir, Module.ShortName + ".sql"));
        }

        public override bool Restore(string backupDir)
        {
            if (Module == null) throw new NullReferenceException("Module not defined");
            string sql = Path.Combine(backupDir, Module.ShortName + ".sql");
            if (!File.Exists(sql)) throw new FileNotFoundException(String.Format("Restore file missing: {0}", sql));

            return DockerCommand.RestoreSqlDump(ContainerName, sql, RestoreCommand);
        }
        public override bool Reset(string backupDir)
        {
            if (Module == null) throw new NullReferenceException("Module not defined");
            if (!File.Exists(InitScript)) throw new FileNotFoundException(String.Format("Reset file missing: {0}", InitScript));

            return DockerCommand.ResetSql(ContainerName, InitScript, RestoreCommand);
        }
    }

    public class StackStatusEventArgs : EventArgs
    {
        public bool Status { get; private set; }
        public StackStatusEventArgs(bool status)
        {
            Status = status;
        }
    }
}
