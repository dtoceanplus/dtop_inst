﻿// This is DTOceanPlus Monitor Tool.
// This tool monitors the different components of the DTOceanPlus modules on Windows.
// Copyright (C) 2021 OPEN CASCADE
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
// License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dtop_mon
{
    public class DtopImage
    {
        public string ID{ get; set; }
        public string Repository { get; set; }
        public string Tag { get; set; }
        public double Size { get; set; }
        public string CreatedAt { get; set; }
        public string Digest { get; set; }

        public string ShortDigest
        {
            get
            {
                int max = 10;
                if (Digest.Length < max) return Digest;
                int index = Digest.IndexOf(':');
                if (index == -1) return Digest.Substring(0, max);
                return Digest.Substring(index+1, max);
            }
        }

        public string ShortRepository
        {
            get
            {
                return Repository.Split('/').Last();
            }
        }

        public static DtopImage Parse(string line)
        {
            var elts = line.Split(new char[] { ';' }, StringSplitOptions.None);

            DtopImage image = new DtopImage()
            {
                ID = elts[0],
                Repository = elts[1],
                Tag= elts[2],
                Size = Utils.ParseSize(elts[3]),
                CreatedAt = ParseDate(elts[4]),
                Digest = elts[5]
            };

            return image;
        }

        public string[] AsStringArray()
        {
            string[] res = new string[] { ID, ShortRepository, Tag, Size.ToString("F1"), CreatedAt, ShortDigest };
            return res;
        }

        private static string ParseDate(string str)
        {
            if (str.Length < 19) return str;
            return str.Substring(0, 19);
        }
    }
}
