﻿// This is DTOceanPlus Monitor Tool.
// This tool monitors the different components of the DTOceanPlus modules on Windows.
// Copyright (C) 2021 OPEN CASCADE
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
// License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dtop_mon
{
    public class BackupManager
    {
        public delegate void ModuleBackupedEventHandler(object sender, BackupedModuleEventArgs e);

        public event ModuleBackupedEventHandler ModuleBackedUp;

        public int Backup(IEnumerable<DtopModule> modules, string backupDir)
        {
            // ensure directory exists
            if (!Directory.Exists(backupDir))
                Directory.CreateDirectory(backupDir);
            WriteManifest(backupDir, modules);

            int nb = 0;
            //
            foreach (var module in modules)
            {
                var db = module.Backup;
                if (db == null) continue;

                bool res = db.Backup(backupDir);
                RaiseModuleRestored(module.Name, res);
                if (res) nb++;
            }
            return nb;
        }

        private void WriteManifest(string name, IEnumerable<DtopModule> modules)
        {
            string manifest = Path.Combine(Properties.Settings.Default.DtopPath, "_backup", name, "info.txt");
            using (var sw = new StreamWriter(manifest))
            {
                foreach (var module in modules)
                {
                    sw.WriteLine("{0}={1}", module.ShortName, module.Version);
                }
            }
        }

        public int Restore(List<DtopModule> modules, string backupDir, out string errors)
        {
            errors = "";
            // ensure directory exists
            if (!Directory.Exists(backupDir)) return -1;

            int nb = 0;
            //
            foreach (var module in modules)
            {
                var db = module.Backup;
                if (db == null) continue;

                try
                {
                    bool res = db.Restore(backupDir);
                    RaiseModuleRestored(module.Name, res);
                    if (res) nb++;
                }
                catch (Exception exc)
                {
                    errors += exc.Message + Environment.NewLine;
                    RaiseModuleRestored(module.Name, false);
                }
            }
            return nb;
        }
        public int Reset(List<DtopModule> modules, string dataDir, out string errors)
        {
            errors = "";
            // ensure directory exists
            if (!Directory.Exists(dataDir)) return -1;

            int nb = 0;
            //
            foreach (var module in modules)
            {

                var db = module.Backup;
                if (db == null) continue;

                try
                {
                    bool res = db.Reset(dataDir);
                    RaiseModuleRestored(module.Name, res);
                    if (res) nb++;
                }
                catch (Exception exc)
                {
                    errors += exc.Message + Environment.NewLine;
                    RaiseModuleRestored(module.Name, false);
                }
            }
            return nb;
        }

        private void RaiseModuleBackuped(string module, bool succeed, string error="")
        {
            if (ModuleBackedUp != null)
            {
                ModuleBackedUp(this, new BackupedModuleEventArgs(module, succeed, error));
            }
        }
        private void RaiseModuleRestored(string module, bool succeed, string error="")
        {
            if (ModuleBackedUp != null)
            {
                ModuleBackedUp(this, new BackupedModuleEventArgs(module, succeed, error));
            }
        }
    }

    public class BackupedModuleEventArgs : EventArgs
    {
        public string Module { get; set; }
        public bool Succeed { get; set; }
        public string Error { get; set; }

        public BackupedModuleEventArgs(string name, bool succeed, string error)
        {
            Module = name;
            Succeed = succeed;
            Error = error;
        }
    }
}
