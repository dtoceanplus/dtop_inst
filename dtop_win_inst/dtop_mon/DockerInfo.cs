// This is DTOceanPlus Monitor Tool.
// This tool monitors the different components of the DTOceanPlus modules on Windows.
// Copyright (C) 2021 OPEN CASCADE
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
// License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dtop_mon
{
    /// <summary>
    /// Class to manage general Docker Information.
    /// </summary>
    public class DockerInfo
    {
        /// <summary>Version of Docker.</summary>
        public string Version{ get; set; }

        /// <summary>Available Memory.</summary>
        public string Memory { get; set; }
        
        /// <summary>Available CPUs..</summary>
        public string CPUs { get; set; }

        public string Swarm { get; set; }
        public bool SwarmActive { get { return Swarm == "active"; } }

    }
}
