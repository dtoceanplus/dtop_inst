// This is DTOceanPlus Windows Installation.
// This program can be used to install, update or uninstall the DTOceanPlus suite of tools.
// Copyright (C) 2021 OPEN CASCADE
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
// License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.
﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dtop_win_inst
{
    /// <summary>
    /// Manage Docker Commands.
    /// </summary>
    public static class DockerCommand
    {
        /// <summary>
        /// Runs a Docker command.
        /// </summary>
        /// <param name="command">The command with all the parameters.</param>
        /// <returns>The output.</returns>
        private static string RunCommand(string command, out int exitCode)
        {
            return RunCommand(command, null, out exitCode);
        }
        private static string RunCommand(string command, Dictionary<string, string> env, out int exitCode)
        {
            Process proc = new Process()
            {
                StartInfo = new ProcessStartInfo()
                {
                    FileName = "docker", // TODO parameter
                    Arguments = command,
                    UseShellExecute = false,
                    CreateNoWindow = true,
                    RedirectStandardError = true,
                    RedirectStandardOutput = true,
                },
            };

            if (env != null)
            {
                foreach (var kv in env)
                {
                    proc.StartInfo.EnvironmentVariables.Add(kv.Key, kv.Value);
                }
            }

            proc.Start();
            string res = proc.StandardOutput.ReadToEnd();
            string error = proc.StandardError.ReadToEnd();
            proc.WaitForExit();
            exitCode = proc.ExitCode;

            if (String.IsNullOrEmpty(res)) return error;
            return res;
        }
        private static string RunCommandWithStdin(string command, string stdin, out int exitCode)
        {
            string res = "", error = "";
            Process proc = new Process()
            {
                StartInfo = new ProcessStartInfo()
                {
                    FileName = "docker", // TODO parameter
                    Arguments = command,
                    UseShellExecute = false,
                    CreateNoWindow = true,
                    RedirectStandardError = true,
                    RedirectStandardOutput = true,
                    RedirectStandardInput = true,
                },
            };

            proc.Start();
            proc.StandardInput.WriteLine(stdin);
            proc.StandardInput.Close();

            res = proc.StandardOutput.ReadToEnd();
            error = proc.StandardError.ReadToEnd();
            proc.WaitForExit();
            exitCode = proc.ExitCode;

            if (String.IsNullOrEmpty(res)) return error;
            return res;
        }

        public static bool IsDockerRunning()
        {
            RunCommand("info", out int code);
            return code == 0;
        }

        public static DockerInfo GetInfo()
        {
            string res = RunCommand("info --format \"{{.NCPU}};{{.MemTotal}}\"", out int code);
            DockerInfo info = null;
            if (code == 0)
            {
                info = new DockerInfo();
                var elts = res.Trim().Split(';');
                info.NbProc = int.Parse(elts[0]);
                info.TotalMemory = long.Parse(elts[1]);
            }

            return info;
        }
        public static bool LoginRegistry(string registry, out string output)
        {
            output = RunCommandWithStdin(String.Format("login -u {0} --password-stdin {1}", Properties.Settings.Default.DLUser, registry),
                Properties.Settings.Default.DLToken, out int code);
            return code == 0;
        }

        public static bool LogoutRegistry(string registry, out string output)
        {
            output = RunCommand(String.Format("logout {0}", registry), out int code);
            return code == 0;
        }

        public static bool PullImage(string url, bool verbose, out string output)
        {
            string quiet = verbose ? "" : "-q";
            output = RunCommand(String.Format("pull {1} {0}", url, quiet), out int code);
            return code == 0;
        }

        public static bool CheckManifestInspect(string url)
        {
            string res = RunCommand(String.Format("manifest inspect {0}", url), out int code);
            if (res.StartsWith("no such manifest")) return false;
            return true;
        }

        public static List<string[]> ListImages()
        {
            List<string[]> images = new List<string[]>();
            string res = RunCommand("images --format {{.CreatedAt}};{{.Repository}};{{.Tag}};{{.Size}}", out int code);
            if (code != 0) return images;

            string[] lines = res.Split('\n');
            foreach (string line in lines)
            {
                if (String.IsNullOrWhiteSpace(line)) continue;
                images.Add(line.Split(';'));
            }

            return images;
        }

        public static List<string[]> ListServices()
        {
            List<string[]> services = new List<string[]>();
            string res = RunCommand("service ls --format {{.Image}};{{.Name}};{{.Replicas}}", out int code);
            if (code != 0) return services;

            string[] lines = res.Split('\n');
            foreach (string line in lines)
            {
                if (String.IsNullOrWhiteSpace(line)) continue;
                services.Add(line.Split(';'));
            }

            return services;
        }

        public static bool IsSwarMode()
        {
            RunCommand("node ls -q", out int code);
            return code == 0;
        }

        public static bool InitSwarm(out string output)
        {
            output = RunCommand("swarm init", out int code);
            return code == 0;
        }
        public static bool LeaveSwarm(bool force, out string output)
        {
            string forceOpt = force ? "--force" : "";
            output = RunCommand(String.Format("swarm leave {0}", forceOpt), out int code);
            return code == 0;
        }

        public static bool CheckNetwork(string network)
        {
            RunCommand(String.Format("network inspect {0}", network), out int code);
            return code == 0;
        }

        public static bool CreateNetwork(string network, out string output)
        {
            output = RunCommand(String.Format("network create --driver overlay --attachable {0}", network), out int code);
            return code == 0;
        }

        public static bool RemoveNetwork(string network, out string output)
        {
            output = RunCommand(String.Format("network rm {0}", network), out int code);
            return code == 0;
        }

        public static bool StackPs(string stack)
        {
            RunCommand(String.Format("stack ps {0} -q", stack), out int code);
            return code == 0;
        }

        public static bool RemoveStack(string module, out string output)
        {
            output = RunCommand(String.Format("stack rm {0}", module), out int code);
            return code == 0;
        }

        public static bool DeployStack(string stack, string stackFile, Dictionary<string, string> env, out string output)
        {
            output = RunCommand(String.Format("stack deploy --compose-file \"{0}\" {1} --with-registry-auth --prune", stackFile, stack), env, out int code);
            return code == 0;
        }

        public static bool InspectVolume(string volume)
        {
            RunCommand(String.Format("volume inspect {0}", volume), out int code);
            return code == 0;
        }
        public static bool CreateVolume(string volume)
        {
            RunCommand(String.Format("volume create {0}", volume), out int code);
            return code == 0;
        }

        public static bool RemoveVolume(string volume, out string output)
        {
            output = RunCommand(String.Format("volume rm {0}", volume), out int code);
            return code == 0;
        }

        public static string CleanupUnusedContainers()
        {
            // get list of containers
            string idList = RunCommand("container ls --filter status=exited -aq", out int code);
            if (code != 0) return "Error: " + idList;

            string[] ids = idList.Split('\n');
            if (ids.Length == 0) return "Nothing to clean";

            string allIds = String.Join(" ", ids);
            if (allIds.Length == 0) return "Nothing to clean";
            return RunCommand(String.Format("container rm {0}", allIds), out code);
        }

        public static Dictionary<string, string> InstalledModules()
        {
            string res = RunCommand("ps --format {{.Names}};{{.Image}}", out int code);
            string[] lines = res.Split('\n');

            Dictionary<string, string> ps = new Dictionary<string, string>();
            foreach (string line in lines)
            {
                string[] elts = line.Split(';');
                if (elts.Length < 2) continue;

                string name = elts[0], image = elts[1];
                string serviceType = name.Split('.')[0];
                if (!serviceType.EndsWith("backend") && !serviceType.EndsWith("frontend")) continue; // ignore other services

                string id = name.Split('_')[0]; // get shortname
                string version = image.Split(':')[1]; // get version

                if (!ps.ContainsKey(id)) ps.Add(id, version);
            }

            return ps;
        }

        public static string GetContainerId(string container)
        {
            string res = RunCommand(String.Format("ps -f name={0} -q", container), out int code);
            if (code == 0) return res.Trim();
            return String.Empty;
        }

        public static bool Copy(string src, string dest, out string output)
        {
            output = RunCommand(String.Format("cp \"{0}\" \"{1}\"", src, dest), out int code);
            return code == 0;
        }

        public static string PruneAll()
        {
            return RunCommand("system prune -a -f --volumes", out int code);
        }
    }
}
