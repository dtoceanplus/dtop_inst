// This is DTOceanPlus Windows Installation.
// This program can be used to install, update or uninstall the DTOceanPlus suite of tools.
// Copyright (C) 2021 OPEN CASCADE
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
// License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.
﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dtop_win_inst
{
    public class InstallConfig
    {
        /// <summary>
        /// Singleton.
        /// </summary>
        private static InstallConfig m_config = new InstallConfig();

        #region Properties
        public string InstallPath { get; set; }
        public string Domain { get; set; }
        public string Data { get; set; }
        public string SiteDatabaseArchive { get; set; }

        public bool DataDefined
        {
            get { return !String.IsNullOrWhiteSpace(Data); }
        }
        public bool DomainDefined
        {
            get { return !String.IsNullOrWhiteSpace(Domain); }
        }
        public bool SiteDatabaseArchiveDefined
        {
            get { return !String.IsNullOrWhiteSpace(SiteDatabaseArchive); }
        }

        public bool IsLocalDomain
        {
            get { return Domain == "dtop.localhost"; }
        }

        private List<DtopModule> m_modules = new List<DtopModule>();

        /// <summary>
        /// Dry run is used in Debug to not pull images.
        /// </summary>
        public bool DryRun { get; set; }
        #endregion

        private InstallConfig()
        {
            Data = "";
            Domain = "";
            SiteDatabaseArchive = "";

            DryRun = false;
        }

        public static InstallConfig Instance()
        {
            return m_config;
        }

        public static void Reset()
        {
            m_config = new InstallConfig();
        }

        public static void ParseEnvFile(string path, Dictionary<string, string> names)
        {
            if (!File.Exists(path))
                throw new FileNotFoundException(String.Format("Env file not found: {0}", path));

            string envFile = "";
            using (StreamReader sr = new StreamReader(path))
            {
                envFile = sr.ReadToEnd();
            }

            var lines = envFile.Split('\n');
            foreach (var line in lines)
            {
                // skip emty lines & comments
                if (String.IsNullOrWhiteSpace(line) || line.TrimStart().StartsWith("#")) continue;

                if (line.StartsWith("DTOP_DOMAIN"))
                {
                    m_config.Domain = InstallConfig.ParseParam(line);
                }
                else if (line.StartsWith("DTOP_DATA"))
                {
                    m_config.Data = InstallConfig.ParseParam(line);
                }
                else if (line.Contains("IMAGE_TAG") || line.Contains("CI_REGISTRY"))
                {
                    // split with '='
                    var elts = line.Trim().Split('=');
                    // find module code
                    string code = elts[0].Split('_')[0];

                    DtopModule module = m_config.Modules.FirstOrDefault(m => m.ShortName == code);
                    if (module == null)
                    {
                        string moduleName = names.ContainsKey(code) ? names[code] : "?";
                        module = new DtopModule(moduleName, code);
                        m_config.Add(module);
                    }

                    if (elts[0].EndsWith("_TAG")) module.Tag = elts[1];
                    else if (elts[0].EndsWith("_CI_REGISTRY")) module.Registry = elts[1];
                }
                else if (line.StartsWith("SC_databases_archive_version"))
                {
                    m_config.SiteDatabaseArchive = InstallConfig.ParseParam(line);
                }
            }
        }

        private static string ParseParam(string line)
        {
            return line.Split(new char[] { '=' })[1].Trim();
        }

        public void Add(DtopModule module)
        {
            m_modules.Add(module);
        }

        public IEnumerable<DtopModule> Modules
        {
            get { return m_modules.AsEnumerable(); }
        }

        public Dictionary<string, string> Environment(string code = "")
        {
            Dictionary<string, string> env = new Dictionary<string, string>();
            env["DTOP_DOMAIN"] = Domain;
            env["DTOP_DATA"] = Data;

            if (!String.IsNullOrEmpty(code))
            {
                var module = m_modules.FirstOrDefault(m => m.ShortName == code);
                if (module != null)
                {
                    env[code + "_CI_REGISTRY"] = module.Registry;
                    env[code + "_IMAGE_TAG"] = module.Tag;
                }
                //else throw??
            }
            return env;
        }
    }
}
