// This is DTOceanPlus Windows Installation.
// This program can be used to install, update or uninstall the DTOceanPlus suite of tools.
// Copyright (C) 2021 OPEN CASCADE
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
// License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.
﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace dtop_win_inst
{
    public partial class FrmPullImage : Form
    {
        private DtopModule m_module;

#if DEBUG
        private bool m_verbose = true;
#else
        private bool m_verbose = false;
#endif
        public FrmPullImage(DtopModule module)
        {
            InitializeComponent();
            m_module = module;
            label1.Text = String.Format("{0} {1} - please wait while pulling...", m_module.ShortName, m_module.Tag);
        }

        public void PullImage()
        {
            bgwPulling.RunWorkerAsync();
        }
        private void PullUrl(string url)
        {
            if (DockerCommand.CheckManifestInspect(url))
            {
                Logger.Write("  Pulling: {0} ... ", url);
                m_module.Succeed = DockerCommand.PullImage(url, m_verbose, out string err);
                if (m_module.Succeed) Logger.WriteLine("Done");
                else Logger.WriteError(err);
            }
            else
            {
                Logger.WriteError("Image {0} is not accessible. The module {1} is skipped.", url, m_module.ShortName);
                m_module.Succeed = false;
            }
        }

        private bool DoCancel(DoWorkEventArgs e)
        {
            if (bgwPulling.CancellationPending)
            {
                e.Cancel = true;
            }
            return e.Cancel;
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
#if DEBUG
            if (InstallConfig.Instance().DryRun)
            {
                for (int i = 0; i < 10; i++)
                {
                    bgwPulling.ReportProgress(i * 10, String.Format("[Dry Run] {0} / 10", i));
                    Thread.Sleep(i * 50);
                }
                m_module.Succeed = true;
                bgwPulling.ReportProgress(100, "Finish");
            }
            else
#endif
            {
                Logger.WriteLine("Pulling module {0}", m_module.ShortName);
                
                bgwPulling.ReportProgress(0, "Login to registry...");
                if (Properties.Settings.Default.AuthRequired)
                {
                    Logger.Write("  Login to {0}... ", m_module.Registry);
                    if (DockerCommand.LoginRegistry(m_module.Registry, out string err)) Logger.WriteLine("Done");
                    else Logger.WriteError(err);
                }
                if (DoCancel(e)) return;
                
                bgwPulling.ReportProgress(10, "Pulling Backend...");
                PullUrl(m_module.BackEndUrl);
                if (DoCancel(e)) return;

                bgwPulling.ReportProgress(50, "Pulling Frontend...");
                PullUrl(m_module.FrontEndUrl);
                if (DoCancel(e)) return;

                bgwPulling.ReportProgress(90, "Logout from registry...");
                if (Properties.Settings.Default.AuthRequired)
                {
                    Logger.Write("  Logout from {0}... ", m_module.Registry);
                    if (DockerCommand.LogoutRegistry(m_module.Registry, out string err)) Logger.WriteLine("Done");
                    else Logger.WriteError(err);
                }
                if (DoCancel(e)) return;

                bgwPulling.ReportProgress(100, "Finished.");
            }
        }

        private delegate void SetProgressDelegate(int p, object status);
        private void UpdateProgress(int p, object status)
        {
            if (progressBar1.InvokeRequired)
            {
                progressBar1.BeginInvoke(new SetProgressDelegate(UpdateProgress), p, status);
            }
            else
            {
                progressBar1.Value = p <= 100 ? p : 100;
                lblDetails.Text = status != null ? status.ToString() : "???";
            }
        }

        private void backgroundWorker1_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            UpdateProgress(e.ProgressPercentage, e.UserState);
        }

        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            this.DialogResult = e.Cancelled ? DialogResult.Cancel : DialogResult.OK;
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            if (bgwPulling.IsBusy)
                bgwPulling.CancelAsync();
        }
    }
}
