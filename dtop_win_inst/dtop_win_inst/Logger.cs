// This is DTOceanPlus Windows Installation.
// This program can be used to install, update or uninstall the DTOceanPlus suite of tools.
// Copyright (C) 2021 OPEN CASCADE
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
// License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.
﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dtop_win_inst
{
    public static class Logger
    {
        private static StreamWriter m_writer;

        public static void Start(string path, bool append)
        {
            if (m_writer != null) m_writer.Close();
            m_writer = new StreamWriter(path, append);
        }

        public static void Stop()
        {
            if (m_writer == null) return;
            m_writer.Close();
        }

        public static void Write(string format, params object[] args)
        {
            if (m_writer == null) return;
            m_writer.Write(format, args);
            m_writer.Flush();
        }
        public static void WriteLine()
        {
            if (m_writer == null) return;
            m_writer.WriteLine();
            m_writer.Flush();
        }
        public static void WriteLine(string format, params object[] args)
        {
            if (m_writer == null) return;
            m_writer.WriteLine(format, args);
            m_writer.Flush();
        }

        public static void WriteError(string format, params object[] args)
        {
            if (m_writer == null) return;
            m_writer.WriteLine();
            m_writer.Write("**** ERROR **** ");
            m_writer.WriteLine(format, args);
            m_writer.Flush();
        }
    }
}
