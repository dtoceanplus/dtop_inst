// This is DTOceanPlus Windows Installation.
// This program can be used to install, update or uninstall the DTOceanPlus suite of tools.
// Copyright (C) 2021 OPEN CASCADE
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
// License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.
﻿
namespace dtop_win_inst
{
    partial class FrmInstall
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmInstall));
            this.pnlHeader = new System.Windows.Forms.Panel();
            this.errDockerCheck = new System.Windows.Forms.ErrorProvider(this.components);
            this.tbxDockerProc = new System.Windows.Forms.TextBox();
            this.tbxDockerTotalMem = new System.Windows.Forms.TextBox();
            this.tbxDiskSpace = new System.Windows.Forms.TextBox();
            this.tclWizard = new dtop_win_inst.WizardTabControl();
            this.tpgConfiguration = new System.Windows.Forms.TabPage();
            this.btnCheckUpdates = new System.Windows.Forms.Button();
            this.gbxDockerInfo = new System.Windows.Forms.GroupBox();
            this.pnlDockerInfo = new System.Windows.Forms.Panel();
            this.lblDiskSpace = new System.Windows.Forms.Label();
            this.lblCPUs = new System.Windows.Forms.Label();
            this.lblMemory = new System.Windows.Forms.Label();
            this.lblDockerStatus = new System.Windows.Forms.Label();
            this.ckxDryRun = new System.Windows.Forms.CheckBox();
            this.lblInstallationType = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lblDtopDomain = new System.Windows.Forms.Label();
            this.lblDtopData = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnRedeploy = new System.Windows.Forms.Button();
            this.btnUninstall = new System.Windows.Forms.Button();
            this.btnConfigQuit = new System.Windows.Forms.Button();
            this.btnConfigContinue = new System.Windows.Forms.Button();
            this.tbxDomain = new System.Windows.Forms.TextBox();
            this.tbxData = new System.Windows.Forms.TextBox();
            this.lblTitleConfiguration = new System.Windows.Forms.Label();
            this.tpgLicense = new System.Windows.Forms.TabPage();
            this.panel2 = new System.Windows.Forms.Panel();
            this.tbxLicense = new System.Windows.Forms.TextBox();
            this.pnlLicenseButtons = new System.Windows.Forms.Panel();
            this.btnLicenseOK = new System.Windows.Forms.Button();
            this.lblTitleLicense = new System.Windows.Forms.Label();
            this.tpgStep1 = new System.Windows.Forms.TabPage();
            this.lblStep1 = new System.Windows.Forms.Label();
            this.pnlStep1Buttons = new System.Windows.Forms.Panel();
            this.btnStep1Reject = new System.Windows.Forms.Button();
            this.btnStep1Accept = new System.Windows.Forms.Button();
            this.lblTitleStep1 = new System.Windows.Forms.Label();
            this.tpgStep2 = new System.Windows.Forms.TabPage();
            this.pnlStep2 = new System.Windows.Forms.Panel();
            this.lblStep2Select = new System.Windows.Forms.Label();
            this.lvwSelectModules = new System.Windows.Forms.ListView();
            this.colShort = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colModule = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colModuleVersion = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colModuleStatus = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.pnlStep2Buttons = new System.Windows.Forms.Panel();
            this.btnSelectNone = new System.Windows.Forms.Button();
            this.btnSelectAll = new System.Windows.Forms.Button();
            this.btnStep2Cancel = new System.Windows.Forms.Button();
            this.btnStep2OK = new System.Windows.Forms.Button();
            this.lblTitleStep2 = new System.Windows.Forms.Label();
            this.tpgStep3 = new System.Windows.Forms.TabPage();
            this.pnlStep3 = new System.Windows.Forms.Panel();
            this.lvwPulledImages = new System.Windows.Forms.ListView();
            this.colImageCreate = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colImageRepository = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colImageTag = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colImageSize = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.pnlStep3Buttons = new System.Windows.Forms.Panel();
            this.btnStep3Cancel = new System.Windows.Forms.Button();
            this.btnStep3Continue = new System.Windows.Forms.Button();
            this.lblTitleStep3 = new System.Windows.Forms.Label();
            this.tpgStep4 = new System.Windows.Forms.TabPage();
            this.lvwServices = new System.Windows.Forms.ListView();
            this.colServiceImage = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colServiceName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colServiceReplicas = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.pnlStep4Buttons = new System.Windows.Forms.Panel();
            this.btnStep4Cancel = new System.Windows.Forms.Button();
            this.btnStep4Continue = new System.Windows.Forms.Button();
            this.lblTitleStep4 = new System.Windows.Forms.Label();
            this.tpgStep5 = new System.Windows.Forms.TabPage();
            this.lblStep5End = new System.Windows.Forms.Label();
            this.pnlStep5Buttons = new System.Windows.Forms.Panel();
            this.btnOpenPortainer = new System.Windows.Forms.Button();
            this.btnStep5Quit = new System.Windows.Forms.Button();
            this.btnLaunchDTO = new System.Windows.Forms.Button();
            this.lblTitleStep5 = new System.Windows.Forms.Label();
            this.tpgUninstall = new System.Windows.Forms.TabPage();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.lvwUninstall = new System.Windows.Forms.ListView();
            this.colUnistallShort = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colUninstallName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.panel3 = new System.Windows.Forms.Panel();
            this.btnUninstallClearAll = new System.Windows.Forms.Button();
            this.btnUninstallSelectAll = new System.Windows.Forms.Button();
            this.btnUninstallCancel = new System.Windows.Forms.Button();
            this.btnUninstallContinue = new System.Windows.Forms.Button();
            this.lblTitleUninstall = new System.Windows.Forms.Label();
            this.tpgRedeploy = new System.Windows.Forms.TabPage();
            this.panel5 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.lvwRedeploy = new System.Windows.Forms.ListView();
            this.colRedeployShort = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colRedeployName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.panel6 = new System.Windows.Forms.Panel();
            this.btnRedeployClearAll = new System.Windows.Forms.Button();
            this.btnRedeploySelectAll = new System.Windows.Forms.Button();
            this.btnRedeployCancel = new System.Windows.Forms.Button();
            this.btnRedeployContinue = new System.Windows.Forms.Button();
            this.lblTitleRedeploy = new System.Windows.Forms.Label();
            this.btnClearLog = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.errDockerCheck)).BeginInit();
            this.tclWizard.SuspendLayout();
            this.tpgConfiguration.SuspendLayout();
            this.gbxDockerInfo.SuspendLayout();
            this.pnlDockerInfo.SuspendLayout();
            this.panel1.SuspendLayout();
            this.tpgLicense.SuspendLayout();
            this.panel2.SuspendLayout();
            this.pnlLicenseButtons.SuspendLayout();
            this.tpgStep1.SuspendLayout();
            this.pnlStep1Buttons.SuspendLayout();
            this.tpgStep2.SuspendLayout();
            this.pnlStep2.SuspendLayout();
            this.pnlStep2Buttons.SuspendLayout();
            this.tpgStep3.SuspendLayout();
            this.pnlStep3.SuspendLayout();
            this.pnlStep3Buttons.SuspendLayout();
            this.tpgStep4.SuspendLayout();
            this.pnlStep4Buttons.SuspendLayout();
            this.tpgStep5.SuspendLayout();
            this.pnlStep5Buttons.SuspendLayout();
            this.tpgUninstall.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel3.SuspendLayout();
            this.tpgRedeploy.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel6.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlHeader
            // 
            this.pnlHeader.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(2)))), ((int)(((byte)(44)))), ((int)(((byte)(95)))));
            this.pnlHeader.BackgroundImage = global::dtop_win_inst.Properties.Resources.DTOcean_Logo_Fond_Unis;
            this.pnlHeader.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pnlHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlHeader.Location = new System.Drawing.Point(0, 0);
            this.pnlHeader.Name = "pnlHeader";
            this.pnlHeader.Size = new System.Drawing.Size(762, 55);
            this.pnlHeader.TabIndex = 0;
            // 
            // errDockerCheck
            // 
            this.errDockerCheck.BlinkRate = 0;
            this.errDockerCheck.BlinkStyle = System.Windows.Forms.ErrorBlinkStyle.NeverBlink;
            this.errDockerCheck.ContainerControl = this;
            this.errDockerCheck.Icon = ((System.Drawing.Icon)(resources.GetObject("errDockerCheck.Icon")));
            // 
            // tbxDockerProc
            // 
            this.tbxDockerProc.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(2)))), ((int)(((byte)(44)))), ((int)(((byte)(95)))));
            this.tbxDockerProc.Font = new System.Drawing.Font("Corbel", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbxDockerProc.ForeColor = System.Drawing.Color.White;
            this.errDockerCheck.SetIconPadding(this.tbxDockerProc, 5);
            this.tbxDockerProc.Location = new System.Drawing.Point(112, 44);
            this.tbxDockerProc.MaxLength = 100;
            this.tbxDockerProc.Name = "tbxDockerProc";
            this.tbxDockerProc.ReadOnly = true;
            this.tbxDockerProc.Size = new System.Drawing.Size(121, 27);
            this.tbxDockerProc.TabIndex = 3;
            this.tbxDockerProc.TabStop = false;
            this.tbxDockerProc.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tbxDockerTotalMem
            // 
            this.tbxDockerTotalMem.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(2)))), ((int)(((byte)(44)))), ((int)(((byte)(95)))));
            this.tbxDockerTotalMem.Font = new System.Drawing.Font("Corbel", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbxDockerTotalMem.ForeColor = System.Drawing.Color.White;
            this.errDockerCheck.SetIconPadding(this.tbxDockerTotalMem, 5);
            this.tbxDockerTotalMem.Location = new System.Drawing.Point(112, 11);
            this.tbxDockerTotalMem.MaxLength = 100;
            this.tbxDockerTotalMem.Name = "tbxDockerTotalMem";
            this.tbxDockerTotalMem.ReadOnly = true;
            this.tbxDockerTotalMem.Size = new System.Drawing.Size(121, 27);
            this.tbxDockerTotalMem.TabIndex = 1;
            this.tbxDockerTotalMem.TabStop = false;
            this.tbxDockerTotalMem.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tbxDiskSpace
            // 
            this.tbxDiskSpace.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(2)))), ((int)(((byte)(44)))), ((int)(((byte)(95)))));
            this.tbxDiskSpace.Font = new System.Drawing.Font("Corbel", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbxDiskSpace.ForeColor = System.Drawing.Color.White;
            this.errDockerCheck.SetIconPadding(this.tbxDiskSpace, 5);
            this.tbxDiskSpace.Location = new System.Drawing.Point(112, 76);
            this.tbxDiskSpace.MaxLength = 100;
            this.tbxDiskSpace.Name = "tbxDiskSpace";
            this.tbxDiskSpace.ReadOnly = true;
            this.tbxDiskSpace.Size = new System.Drawing.Size(121, 27);
            this.tbxDiskSpace.TabIndex = 5;
            this.tbxDiskSpace.TabStop = false;
            this.tbxDiskSpace.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tclWizard
            // 
            this.tclWizard.Controls.Add(this.tpgConfiguration);
            this.tclWizard.Controls.Add(this.tpgLicense);
            this.tclWizard.Controls.Add(this.tpgStep1);
            this.tclWizard.Controls.Add(this.tpgStep2);
            this.tclWizard.Controls.Add(this.tpgStep3);
            this.tclWizard.Controls.Add(this.tpgStep4);
            this.tclWizard.Controls.Add(this.tpgStep5);
            this.tclWizard.Controls.Add(this.tpgUninstall);
            this.tclWizard.Controls.Add(this.tpgRedeploy);
            this.tclWizard.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tclWizard.Location = new System.Drawing.Point(0, 55);
            this.tclWizard.Multiline = true;
            this.tclWizard.Name = "tclWizard";
            this.tclWizard.SelectedIndex = 0;
            this.tclWizard.SimpleMode = true;
            this.tclWizard.Size = new System.Drawing.Size(762, 463);
            this.tclWizard.TabIndex = 1;
            // 
            // tpgConfiguration
            // 
            this.tpgConfiguration.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(2)))), ((int)(((byte)(44)))), ((int)(((byte)(95)))));
            this.tpgConfiguration.Controls.Add(this.btnCheckUpdates);
            this.tpgConfiguration.Controls.Add(this.gbxDockerInfo);
            this.tpgConfiguration.Controls.Add(this.ckxDryRun);
            this.tpgConfiguration.Controls.Add(this.lblInstallationType);
            this.tpgConfiguration.Controls.Add(this.label1);
            this.tpgConfiguration.Controls.Add(this.lblDtopDomain);
            this.tpgConfiguration.Controls.Add(this.lblDtopData);
            this.tpgConfiguration.Controls.Add(this.panel1);
            this.tpgConfiguration.Controls.Add(this.tbxDomain);
            this.tpgConfiguration.Controls.Add(this.tbxData);
            this.tpgConfiguration.Controls.Add(this.lblTitleConfiguration);
            this.tpgConfiguration.Location = new System.Drawing.Point(4, 22);
            this.tpgConfiguration.Name = "tpgConfiguration";
            this.tpgConfiguration.Padding = new System.Windows.Forms.Padding(3, 3, 3, 3);
            this.tpgConfiguration.Size = new System.Drawing.Size(754, 437);
            this.tpgConfiguration.TabIndex = 6;
            this.tpgConfiguration.Text = "Configuration";
            // 
            // btnCheckUpdates
            // 
            this.btnCheckUpdates.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCheckUpdates.Font = new System.Drawing.Font("Corbel", 12F);
            this.btnCheckUpdates.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnCheckUpdates.Location = new System.Drawing.Point(603, 51);
            this.btnCheckUpdates.Name = "btnCheckUpdates";
            this.btnCheckUpdates.Size = new System.Drawing.Size(142, 41);
            this.btnCheckUpdates.TabIndex = 2;
            this.btnCheckUpdates.Text = "Check for Updates";
            this.btnCheckUpdates.UseVisualStyleBackColor = true;
            this.btnCheckUpdates.Click += new System.EventHandler(this.btnCheckUpdates_Click);
            // 
            // gbxDockerInfo
            // 
            this.gbxDockerInfo.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbxDockerInfo.Controls.Add(this.pnlDockerInfo);
            this.gbxDockerInfo.Controls.Add(this.lblDockerStatus);
            this.gbxDockerInfo.Font = new System.Drawing.Font("Corbel", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbxDockerInfo.ForeColor = System.Drawing.Color.White;
            this.gbxDockerInfo.Location = new System.Drawing.Point(43, 237);
            this.gbxDockerInfo.Name = "gbxDockerInfo";
            this.gbxDockerInfo.Size = new System.Drawing.Size(567, 141);
            this.gbxDockerInfo.TabIndex = 9;
            this.gbxDockerInfo.TabStop = false;
            this.gbxDockerInfo.Text = "Docker Info";
            // 
            // pnlDockerInfo
            // 
            this.pnlDockerInfo.Controls.Add(this.lblDiskSpace);
            this.pnlDockerInfo.Controls.Add(this.tbxDiskSpace);
            this.pnlDockerInfo.Controls.Add(this.tbxDockerProc);
            this.pnlDockerInfo.Controls.Add(this.tbxDockerTotalMem);
            this.pnlDockerInfo.Controls.Add(this.lblCPUs);
            this.pnlDockerInfo.Controls.Add(this.lblMemory);
            this.pnlDockerInfo.Location = new System.Drawing.Point(6, 24);
            this.pnlDockerInfo.Name = "pnlDockerInfo";
            this.pnlDockerInfo.Size = new System.Drawing.Size(450, 110);
            this.pnlDockerInfo.TabIndex = 0;
            // 
            // lblDiskSpace
            // 
            this.lblDiskSpace.AutoSize = true;
            this.lblDiskSpace.Location = new System.Drawing.Point(8, 79);
            this.lblDiskSpace.Name = "lblDiskSpace";
            this.lblDiskSpace.Size = new System.Drawing.Size(82, 19);
            this.lblDiskSpace.TabIndex = 4;
            this.lblDiskSpace.Text = "Disk Space";
            // 
            // lblCPUs
            // 
            this.lblCPUs.AutoSize = true;
            this.lblCPUs.Location = new System.Drawing.Point(8, 46);
            this.lblCPUs.Name = "lblCPUs";
            this.lblCPUs.Size = new System.Drawing.Size(44, 19);
            this.lblCPUs.TabIndex = 2;
            this.lblCPUs.Text = "CPUs";
            // 
            // lblMemory
            // 
            this.lblMemory.AutoSize = true;
            this.lblMemory.Location = new System.Drawing.Point(3, 14);
            this.lblMemory.Name = "lblMemory";
            this.lblMemory.Size = new System.Drawing.Size(103, 19);
            this.lblMemory.TabIndex = 0;
            this.lblMemory.Text = "Total Memory";
            // 
            // lblDockerStatus
            // 
            this.lblDockerStatus.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblDockerStatus.Location = new System.Drawing.Point(462, 50);
            this.lblDockerStatus.Name = "lblDockerStatus";
            this.lblDockerStatus.Size = new System.Drawing.Size(102, 77);
            this.lblDockerStatus.TabIndex = 0;
            this.lblDockerStatus.Text = "Docker is not running\r\nPlease check your configuration\r\nor wait for Docker to be " +
    "fully started";
            this.lblDockerStatus.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // ckxDryRun
            // 
            this.ckxDryRun.AutoSize = true;
            this.ckxDryRun.ForeColor = System.Drawing.Color.White;
            this.ckxDryRun.Location = new System.Drawing.Point(670, 361);
            this.ckxDryRun.Name = "ckxDryRun";
            this.ckxDryRun.Size = new System.Drawing.Size(65, 17);
            this.ckxDryRun.TabIndex = 0;
            this.ckxDryRun.Text = "Dry Run";
            this.ckxDryRun.UseVisualStyleBackColor = true;
            this.ckxDryRun.Visible = false;
            // 
            // lblInstallationType
            // 
            this.lblInstallationType.AutoSize = true;
            this.lblInstallationType.Font = new System.Drawing.Font("Corbel", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblInstallationType.ForeColor = System.Drawing.Color.White;
            this.lblInstallationType.Location = new System.Drawing.Point(417, 183);
            this.lblInstallationType.Name = "lblInstallationType";
            this.lblInstallationType.Size = new System.Drawing.Size(123, 19);
            this.lblInstallationType.TabIndex = 7;
            this.lblInstallationType.Text = "Local Installation";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Corbel", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(39, 54);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(544, 38);
            this.label1.TabIndex = 1;
            this.label1.Text = "The following configuration is defined in the file dtop_inst.env.\r\nIf you need to" +
    " change theses values, please exit then installation and edit the file.";
            // 
            // lblDtopDomain
            // 
            this.lblDtopDomain.AutoSize = true;
            this.lblDtopDomain.Font = new System.Drawing.Font("Corbel", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDtopDomain.ForeColor = System.Drawing.Color.White;
            this.lblDtopDomain.Location = new System.Drawing.Point(39, 183);
            this.lblDtopDomain.Name = "lblDtopDomain";
            this.lblDtopDomain.Size = new System.Drawing.Size(62, 19);
            this.lblDtopDomain.TabIndex = 5;
            this.lblDtopDomain.Text = "Domain";
            // 
            // lblDtopData
            // 
            this.lblDtopData.AutoSize = true;
            this.lblDtopData.Font = new System.Drawing.Font("Corbel", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDtopData.ForeColor = System.Drawing.Color.White;
            this.lblDtopData.Location = new System.Drawing.Point(39, 136);
            this.lblDtopData.Name = "lblDtopData";
            this.lblDtopData.Size = new System.Drawing.Size(108, 19);
            this.lblDtopData.TabIndex = 3;
            this.lblDtopData.Text = "Data Directory";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnClearLog);
            this.panel1.Controls.Add(this.btnRedeploy);
            this.panel1.Controls.Add(this.btnUninstall);
            this.panel1.Controls.Add(this.btnConfigQuit);
            this.panel1.Controls.Add(this.btnConfigContinue);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(3, 384);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(748, 50);
            this.panel1.TabIndex = 10;
            // 
            // btnRedeploy
            // 
            this.btnRedeploy.Font = new System.Drawing.Font("Corbel", 12F);
            this.btnRedeploy.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnRedeploy.Location = new System.Drawing.Point(93, 6);
            this.btnRedeploy.Name = "btnRedeploy";
            this.btnRedeploy.Size = new System.Drawing.Size(84, 41);
            this.btnRedeploy.TabIndex = 1;
            this.btnRedeploy.Text = "Redeploy";
            this.btnRedeploy.UseVisualStyleBackColor = true;
            this.btnRedeploy.Click += new System.EventHandler(this.btnRedeploy_Click);
            // 
            // btnUninstall
            // 
            this.btnUninstall.Font = new System.Drawing.Font("Corbel", 12F);
            this.btnUninstall.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnUninstall.Location = new System.Drawing.Point(3, 6);
            this.btnUninstall.Name = "btnUninstall";
            this.btnUninstall.Size = new System.Drawing.Size(84, 41);
            this.btnUninstall.TabIndex = 0;
            this.btnUninstall.Text = "Uninstall";
            this.btnUninstall.UseVisualStyleBackColor = true;
            this.btnUninstall.Click += new System.EventHandler(this.btnUninstall_Click);
            // 
            // btnConfigQuit
            // 
            this.btnConfigQuit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnConfigQuit.Font = new System.Drawing.Font("Corbel", 12F);
            this.btnConfigQuit.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnConfigQuit.Location = new System.Drawing.Point(667, 6);
            this.btnConfigQuit.Name = "btnConfigQuit";
            this.btnConfigQuit.Size = new System.Drawing.Size(75, 41);
            this.btnConfigQuit.TabIndex = 3;
            this.btnConfigQuit.Text = "Cancel";
            this.btnConfigQuit.UseVisualStyleBackColor = true;
            this.btnConfigQuit.Click += new System.EventHandler(this.btnConfigQuit_Click);
            // 
            // btnConfigContinue
            // 
            this.btnConfigContinue.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnConfigContinue.Font = new System.Drawing.Font("Corbel", 12F);
            this.btnConfigContinue.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnConfigContinue.Location = new System.Drawing.Point(577, 6);
            this.btnConfigContinue.Name = "btnConfigContinue";
            this.btnConfigContinue.Size = new System.Drawing.Size(84, 41);
            this.btnConfigContinue.TabIndex = 2;
            this.btnConfigContinue.Text = "&Continue";
            this.btnConfigContinue.UseVisualStyleBackColor = true;
            this.btnConfigContinue.Click += new System.EventHandler(this.btnConfigContinue_Click);
            // 
            // tbxDomain
            // 
            this.tbxDomain.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(2)))), ((int)(((byte)(44)))), ((int)(((byte)(95)))));
            this.tbxDomain.Font = new System.Drawing.Font("Corbel", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbxDomain.ForeColor = System.Drawing.Color.White;
            this.tbxDomain.Location = new System.Drawing.Point(161, 180);
            this.tbxDomain.MaxLength = 100;
            this.tbxDomain.Name = "tbxDomain";
            this.tbxDomain.ReadOnly = true;
            this.tbxDomain.Size = new System.Drawing.Size(250, 27);
            this.tbxDomain.TabIndex = 6;
            this.tbxDomain.TabStop = false;
            // 
            // tbxData
            // 
            this.tbxData.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(2)))), ((int)(((byte)(44)))), ((int)(((byte)(95)))));
            this.tbxData.Font = new System.Drawing.Font("Corbel", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbxData.ForeColor = System.Drawing.Color.White;
            this.tbxData.Location = new System.Drawing.Point(161, 133);
            this.tbxData.MaxLength = 300;
            this.tbxData.Name = "tbxData";
            this.tbxData.ReadOnly = true;
            this.tbxData.Size = new System.Drawing.Size(449, 27);
            this.tbxData.TabIndex = 4;
            this.tbxData.TabStop = false;
            // 
            // lblTitleConfiguration
            // 
            this.lblTitleConfiguration.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblTitleConfiguration.Font = new System.Drawing.Font("Corbel", 14.25F);
            this.lblTitleConfiguration.ForeColor = System.Drawing.Color.White;
            this.lblTitleConfiguration.Location = new System.Drawing.Point(3, 3);
            this.lblTitleConfiguration.Name = "lblTitleConfiguration";
            this.lblTitleConfiguration.Size = new System.Drawing.Size(748, 33);
            this.lblTitleConfiguration.TabIndex = 0;
            this.lblTitleConfiguration.Text = "Configuration";
            this.lblTitleConfiguration.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tpgLicense
            // 
            this.tpgLicense.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(2)))), ((int)(((byte)(44)))), ((int)(((byte)(95)))));
            this.tpgLicense.Controls.Add(this.panel2);
            this.tpgLicense.Controls.Add(this.pnlLicenseButtons);
            this.tpgLicense.Controls.Add(this.lblTitleLicense);
            this.tpgLicense.Location = new System.Drawing.Point(4, 22);
            this.tpgLicense.Name = "tpgLicense";
            this.tpgLicense.Padding = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.tpgLicense.Size = new System.Drawing.Size(754, 437);
            this.tpgLicense.TabIndex = 0;
            this.tpgLicense.Text = "License";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.tbxLicense);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(5, 38);
            this.panel2.Name = "panel2";
            this.panel2.Padding = new System.Windows.Forms.Padding(20, 0, 20, 0);
            this.panel2.Size = new System.Drawing.Size(744, 344);
            this.panel2.TabIndex = 3;
            // 
            // tbxLicense
            // 
            this.tbxLicense.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbxLicense.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.tbxLicense.HideSelection = false;
            this.tbxLicense.Location = new System.Drawing.Point(20, 0);
            this.tbxLicense.Multiline = true;
            this.tbxLicense.Name = "tbxLicense";
            this.tbxLicense.ReadOnly = true;
            this.tbxLicense.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.tbxLicense.Size = new System.Drawing.Size(704, 344);
            this.tbxLicense.TabIndex = 2;
            this.tbxLicense.TabStop = false;
            // 
            // pnlLicenseButtons
            // 
            this.pnlLicenseButtons.Controls.Add(this.btnLicenseOK);
            this.pnlLicenseButtons.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlLicenseButtons.Location = new System.Drawing.Point(5, 382);
            this.pnlLicenseButtons.Name = "pnlLicenseButtons";
            this.pnlLicenseButtons.Size = new System.Drawing.Size(744, 50);
            this.pnlLicenseButtons.TabIndex = 0;
            // 
            // btnLicenseOK
            // 
            this.btnLicenseOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnLicenseOK.AutoSize = true;
            this.btnLicenseOK.Font = new System.Drawing.Font("Corbel", 12F);
            this.btnLicenseOK.Location = new System.Drawing.Point(555, 6);
            this.btnLicenseOK.Name = "btnLicenseOK";
            this.btnLicenseOK.Size = new System.Drawing.Size(183, 41);
            this.btnLicenseOK.TabIndex = 0;
            this.btnLicenseOK.Text = "Proceed to &Confirm";
            this.btnLicenseOK.UseVisualStyleBackColor = true;
            this.btnLicenseOK.Click += new System.EventHandler(this.btnLicenseOK_Click);
            // 
            // lblTitleLicense
            // 
            this.lblTitleLicense.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblTitleLicense.Font = new System.Drawing.Font("Corbel", 14.25F);
            this.lblTitleLicense.ForeColor = System.Drawing.Color.White;
            this.lblTitleLicense.Location = new System.Drawing.Point(5, 5);
            this.lblTitleLicense.Name = "lblTitleLicense";
            this.lblTitleLicense.Size = new System.Drawing.Size(744, 33);
            this.lblTitleLicense.TabIndex = 1;
            this.lblTitleLicense.Text = "DTOceanPlus (DTOP) Project License";
            this.lblTitleLicense.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tpgStep1
            // 
            this.tpgStep1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(2)))), ((int)(((byte)(44)))), ((int)(((byte)(95)))));
            this.tpgStep1.Controls.Add(this.lblStep1);
            this.tpgStep1.Controls.Add(this.pnlStep1Buttons);
            this.tpgStep1.Controls.Add(this.lblTitleStep1);
            this.tpgStep1.Location = new System.Drawing.Point(4, 22);
            this.tpgStep1.Name = "tpgStep1";
            this.tpgStep1.Padding = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.tpgStep1.Size = new System.Drawing.Size(754, 437);
            this.tpgStep1.TabIndex = 1;
            this.tpgStep1.Text = "Step 1";
            // 
            // lblStep1
            // 
            this.lblStep1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblStep1.Font = new System.Drawing.Font("Corbel", 14.25F);
            this.lblStep1.ForeColor = System.Drawing.Color.White;
            this.lblStep1.Location = new System.Drawing.Point(5, 38);
            this.lblStep1.Name = "lblStep1";
            this.lblStep1.Size = new System.Drawing.Size(744, 344);
            this.lblStep1.TabIndex = 1;
            this.lblStep1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pnlStep1Buttons
            // 
            this.pnlStep1Buttons.Controls.Add(this.btnStep1Reject);
            this.pnlStep1Buttons.Controls.Add(this.btnStep1Accept);
            this.pnlStep1Buttons.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlStep1Buttons.Location = new System.Drawing.Point(5, 382);
            this.pnlStep1Buttons.Name = "pnlStep1Buttons";
            this.pnlStep1Buttons.Size = new System.Drawing.Size(744, 50);
            this.pnlStep1Buttons.TabIndex = 0;
            // 
            // btnStep1Reject
            // 
            this.btnStep1Reject.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnStep1Reject.Font = new System.Drawing.Font("Corbel", 12F);
            this.btnStep1Reject.Location = new System.Drawing.Point(663, 6);
            this.btnStep1Reject.Name = "btnStep1Reject";
            this.btnStep1Reject.Size = new System.Drawing.Size(75, 41);
            this.btnStep1Reject.TabIndex = 1;
            this.btnStep1Reject.Text = "&Reject";
            this.btnStep1Reject.UseVisualStyleBackColor = true;
            this.btnStep1Reject.Click += new System.EventHandler(this.btnStep1Reject_Click);
            // 
            // btnStep1Accept
            // 
            this.btnStep1Accept.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnStep1Accept.Font = new System.Drawing.Font("Corbel", 12F);
            this.btnStep1Accept.Location = new System.Drawing.Point(582, 6);
            this.btnStep1Accept.Name = "btnStep1Accept";
            this.btnStep1Accept.Size = new System.Drawing.Size(75, 41);
            this.btnStep1Accept.TabIndex = 0;
            this.btnStep1Accept.Text = "&Accept";
            this.btnStep1Accept.UseVisualStyleBackColor = true;
            this.btnStep1Accept.Click += new System.EventHandler(this.btnStep1Accept_Click);
            // 
            // lblTitleStep1
            // 
            this.lblTitleStep1.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblTitleStep1.Font = new System.Drawing.Font("Corbel", 14.25F);
            this.lblTitleStep1.ForeColor = System.Drawing.Color.White;
            this.lblTitleStep1.Location = new System.Drawing.Point(5, 5);
            this.lblTitleStep1.Name = "lblTitleStep1";
            this.lblTitleStep1.Size = new System.Drawing.Size(744, 33);
            this.lblTitleStep1.TabIndex = 2;
            this.lblTitleStep1.Text = "Step 1/5 - DTOceanPlus (DTOP) Project License Acceptance";
            this.lblTitleStep1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tpgStep2
            // 
            this.tpgStep2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(2)))), ((int)(((byte)(44)))), ((int)(((byte)(95)))));
            this.tpgStep2.Controls.Add(this.pnlStep2);
            this.tpgStep2.Controls.Add(this.pnlStep2Buttons);
            this.tpgStep2.Controls.Add(this.lblTitleStep2);
            this.tpgStep2.Location = new System.Drawing.Point(4, 22);
            this.tpgStep2.Name = "tpgStep2";
            this.tpgStep2.Padding = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.tpgStep2.Size = new System.Drawing.Size(754, 437);
            this.tpgStep2.TabIndex = 2;
            this.tpgStep2.Text = "Step 2";
            // 
            // pnlStep2
            // 
            this.pnlStep2.Controls.Add(this.lblStep2Select);
            this.pnlStep2.Controls.Add(this.lvwSelectModules);
            this.pnlStep2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlStep2.Location = new System.Drawing.Point(5, 38);
            this.pnlStep2.Name = "pnlStep2";
            this.pnlStep2.Size = new System.Drawing.Size(744, 344);
            this.pnlStep2.TabIndex = 1;
            // 
            // lblStep2Select
            // 
            this.lblStep2Select.AutoSize = true;
            this.lblStep2Select.Font = new System.Drawing.Font("Corbel", 12F);
            this.lblStep2Select.ForeColor = System.Drawing.Color.White;
            this.lblStep2Select.Location = new System.Drawing.Point(14, 15);
            this.lblStep2Select.Name = "lblStep2Select";
            this.lblStep2Select.Size = new System.Drawing.Size(319, 19);
            this.lblStep2Select.TabIndex = 0;
            this.lblStep2Select.Text = "Please select the modules to install and deploy";
            // 
            // lvwSelectModules
            // 
            this.lvwSelectModules.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lvwSelectModules.CheckBoxes = true;
            this.lvwSelectModules.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colShort,
            this.colModule,
            this.colModuleVersion,
            this.colModuleStatus});
            this.lvwSelectModules.Font = new System.Drawing.Font("Corbel", 9.75F);
            this.lvwSelectModules.FullRowSelect = true;
            this.lvwSelectModules.GridLines = true;
            this.lvwSelectModules.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None;
            this.lvwSelectModules.HideSelection = false;
            this.lvwSelectModules.Location = new System.Drawing.Point(0, 37);
            this.lvwSelectModules.MultiSelect = false;
            this.lvwSelectModules.Name = "lvwSelectModules";
            this.lvwSelectModules.Size = new System.Drawing.Size(742, 299);
            this.lvwSelectModules.TabIndex = 1;
            this.lvwSelectModules.UseCompatibleStateImageBehavior = false;
            this.lvwSelectModules.View = System.Windows.Forms.View.Details;
            // 
            // colShort
            // 
            this.colShort.Text = "Short";
            // 
            // colModule
            // 
            this.colModule.Text = "Module";
            // 
            // colModuleVersion
            // 
            this.colModuleVersion.Text = "Version";
            // 
            // colModuleStatus
            // 
            this.colModuleStatus.Text = "Status";
            // 
            // pnlStep2Buttons
            // 
            this.pnlStep2Buttons.Controls.Add(this.btnSelectNone);
            this.pnlStep2Buttons.Controls.Add(this.btnSelectAll);
            this.pnlStep2Buttons.Controls.Add(this.btnStep2Cancel);
            this.pnlStep2Buttons.Controls.Add(this.btnStep2OK);
            this.pnlStep2Buttons.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlStep2Buttons.Location = new System.Drawing.Point(5, 382);
            this.pnlStep2Buttons.Name = "pnlStep2Buttons";
            this.pnlStep2Buttons.Size = new System.Drawing.Size(744, 50);
            this.pnlStep2Buttons.TabIndex = 2;
            // 
            // btnSelectNone
            // 
            this.btnSelectNone.Font = new System.Drawing.Font("Corbel", 12F);
            this.btnSelectNone.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnSelectNone.Location = new System.Drawing.Point(98, 6);
            this.btnSelectNone.Name = "btnSelectNone";
            this.btnSelectNone.Size = new System.Drawing.Size(124, 41);
            this.btnSelectNone.TabIndex = 1;
            this.btnSelectNone.Text = "Clea&r Selection";
            this.btnSelectNone.UseVisualStyleBackColor = true;
            this.btnSelectNone.Click += new System.EventHandler(this.btnSelectNone_Click);
            // 
            // btnSelectAll
            // 
            this.btnSelectAll.Font = new System.Drawing.Font("Corbel", 12F);
            this.btnSelectAll.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnSelectAll.Location = new System.Drawing.Point(3, 6);
            this.btnSelectAll.Name = "btnSelectAll";
            this.btnSelectAll.Size = new System.Drawing.Size(89, 41);
            this.btnSelectAll.TabIndex = 0;
            this.btnSelectAll.Text = "Select &All";
            this.btnSelectAll.UseVisualStyleBackColor = true;
            this.btnSelectAll.Click += new System.EventHandler(this.btnSelectAll_Click);
            // 
            // btnStep2Cancel
            // 
            this.btnStep2Cancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnStep2Cancel.Font = new System.Drawing.Font("Corbel", 12F);
            this.btnStep2Cancel.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnStep2Cancel.Location = new System.Drawing.Point(663, 6);
            this.btnStep2Cancel.Name = "btnStep2Cancel";
            this.btnStep2Cancel.Size = new System.Drawing.Size(75, 41);
            this.btnStep2Cancel.TabIndex = 3;
            this.btnStep2Cancel.Text = "Cancel";
            this.btnStep2Cancel.UseVisualStyleBackColor = true;
            this.btnStep2Cancel.Click += new System.EventHandler(this.btnStep2Cancel_Click);
            // 
            // btnStep2OK
            // 
            this.btnStep2OK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnStep2OK.Font = new System.Drawing.Font("Corbel", 12F);
            this.btnStep2OK.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnStep2OK.Location = new System.Drawing.Point(576, 6);
            this.btnStep2OK.Name = "btnStep2OK";
            this.btnStep2OK.Size = new System.Drawing.Size(82, 41);
            this.btnStep2OK.TabIndex = 2;
            this.btnStep2OK.Text = "&Continue";
            this.btnStep2OK.UseVisualStyleBackColor = true;
            this.btnStep2OK.Click += new System.EventHandler(this.btnStep2OK_Click);
            // 
            // lblTitleStep2
            // 
            this.lblTitleStep2.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblTitleStep2.Font = new System.Drawing.Font("Corbel", 14.25F);
            this.lblTitleStep2.ForeColor = System.Drawing.Color.White;
            this.lblTitleStep2.Location = new System.Drawing.Point(5, 5);
            this.lblTitleStep2.Name = "lblTitleStep2";
            this.lblTitleStep2.Size = new System.Drawing.Size(744, 33);
            this.lblTitleStep2.TabIndex = 0;
            this.lblTitleStep2.Text = "Step 2/5 - Selection of DTOP modules to be installed and deployed";
            this.lblTitleStep2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tpgStep3
            // 
            this.tpgStep3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(2)))), ((int)(((byte)(44)))), ((int)(((byte)(95)))));
            this.tpgStep3.Controls.Add(this.pnlStep3);
            this.tpgStep3.Controls.Add(this.pnlStep3Buttons);
            this.tpgStep3.Controls.Add(this.lblTitleStep3);
            this.tpgStep3.Location = new System.Drawing.Point(4, 22);
            this.tpgStep3.Name = "tpgStep3";
            this.tpgStep3.Padding = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.tpgStep3.Size = new System.Drawing.Size(754, 437);
            this.tpgStep3.TabIndex = 3;
            this.tpgStep3.Text = "Step 3";
            // 
            // pnlStep3
            // 
            this.pnlStep3.Controls.Add(this.lvwPulledImages);
            this.pnlStep3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlStep3.Location = new System.Drawing.Point(5, 38);
            this.pnlStep3.Name = "pnlStep3";
            this.pnlStep3.Size = new System.Drawing.Size(744, 344);
            this.pnlStep3.TabIndex = 4;
            // 
            // lvwPulledImages
            // 
            this.lvwPulledImages.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colImageCreate,
            this.colImageRepository,
            this.colImageTag,
            this.colImageSize});
            this.lvwPulledImages.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lvwPulledImages.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.lvwPulledImages.HideSelection = false;
            this.lvwPulledImages.Location = new System.Drawing.Point(0, 0);
            this.lvwPulledImages.MultiSelect = false;
            this.lvwPulledImages.Name = "lvwPulledImages";
            this.lvwPulledImages.Size = new System.Drawing.Size(744, 344);
            this.lvwPulledImages.TabIndex = 0;
            this.lvwPulledImages.UseCompatibleStateImageBehavior = false;
            this.lvwPulledImages.View = System.Windows.Forms.View.Details;
            // 
            // colImageCreate
            // 
            this.colImageCreate.Text = "Created at";
            this.colImageCreate.Width = 91;
            // 
            // colImageRepository
            // 
            this.colImageRepository.Text = "Repository";
            this.colImageRepository.Width = 507;
            // 
            // colImageTag
            // 
            this.colImageTag.Text = "Tag";
            this.colImageTag.Width = 70;
            // 
            // colImageSize
            // 
            this.colImageSize.Text = "Size";
            this.colImageSize.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.colImageSize.Width = 62;
            // 
            // pnlStep3Buttons
            // 
            this.pnlStep3Buttons.Controls.Add(this.btnStep3Cancel);
            this.pnlStep3Buttons.Controls.Add(this.btnStep3Continue);
            this.pnlStep3Buttons.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlStep3Buttons.Location = new System.Drawing.Point(5, 382);
            this.pnlStep3Buttons.Name = "pnlStep3Buttons";
            this.pnlStep3Buttons.Size = new System.Drawing.Size(744, 50);
            this.pnlStep3Buttons.TabIndex = 1;
            // 
            // btnStep3Cancel
            // 
            this.btnStep3Cancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnStep3Cancel.Font = new System.Drawing.Font("Corbel", 12F);
            this.btnStep3Cancel.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnStep3Cancel.Location = new System.Drawing.Point(663, 6);
            this.btnStep3Cancel.Name = "btnStep3Cancel";
            this.btnStep3Cancel.Size = new System.Drawing.Size(75, 41);
            this.btnStep3Cancel.TabIndex = 1;
            this.btnStep3Cancel.Text = "Cancel";
            this.btnStep3Cancel.UseVisualStyleBackColor = true;
            this.btnStep3Cancel.Click += new System.EventHandler(this.btnStep3Cancel_Click);
            // 
            // btnStep3Continue
            // 
            this.btnStep3Continue.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnStep3Continue.Font = new System.Drawing.Font("Corbel", 12F);
            this.btnStep3Continue.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnStep3Continue.Location = new System.Drawing.Point(573, 6);
            this.btnStep3Continue.Name = "btnStep3Continue";
            this.btnStep3Continue.Size = new System.Drawing.Size(84, 41);
            this.btnStep3Continue.TabIndex = 0;
            this.btnStep3Continue.Text = "&Continue";
            this.btnStep3Continue.UseVisualStyleBackColor = true;
            this.btnStep3Continue.Click += new System.EventHandler(this.btnStep3Continue_Click);
            // 
            // lblTitleStep3
            // 
            this.lblTitleStep3.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblTitleStep3.Font = new System.Drawing.Font("Corbel", 14.25F);
            this.lblTitleStep3.ForeColor = System.Drawing.Color.White;
            this.lblTitleStep3.Location = new System.Drawing.Point(5, 5);
            this.lblTitleStep3.Name = "lblTitleStep3";
            this.lblTitleStep3.Size = new System.Drawing.Size(744, 33);
            this.lblTitleStep3.TabIndex = 0;
            this.lblTitleStep3.Text = "Step 3/5 - DTOP Modules\' Docker Images Downloading / Pulling";
            this.lblTitleStep3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tpgStep4
            // 
            this.tpgStep4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(2)))), ((int)(((byte)(44)))), ((int)(((byte)(95)))));
            this.tpgStep4.Controls.Add(this.lvwServices);
            this.tpgStep4.Controls.Add(this.pnlStep4Buttons);
            this.tpgStep4.Controls.Add(this.lblTitleStep4);
            this.tpgStep4.Location = new System.Drawing.Point(4, 22);
            this.tpgStep4.Name = "tpgStep4";
            this.tpgStep4.Padding = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.tpgStep4.Size = new System.Drawing.Size(754, 437);
            this.tpgStep4.TabIndex = 4;
            this.tpgStep4.Text = "Step 4";
            // 
            // lvwServices
            // 
            this.lvwServices.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colServiceImage,
            this.colServiceName,
            this.colServiceReplicas});
            this.lvwServices.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lvwServices.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.lvwServices.HideSelection = false;
            this.lvwServices.Location = new System.Drawing.Point(5, 38);
            this.lvwServices.MultiSelect = false;
            this.lvwServices.Name = "lvwServices";
            this.lvwServices.Size = new System.Drawing.Size(744, 344);
            this.lvwServices.TabIndex = 1;
            this.lvwServices.UseCompatibleStateImageBehavior = false;
            this.lvwServices.View = System.Windows.Forms.View.Details;
            // 
            // colServiceImage
            // 
            this.colServiceImage.Text = "Image";
            this.colServiceImage.Width = 511;
            // 
            // colServiceName
            // 
            this.colServiceName.Text = "Name";
            this.colServiceName.Width = 70;
            // 
            // colServiceReplicas
            // 
            this.colServiceReplicas.Text = "Replicas";
            this.colServiceReplicas.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.colServiceReplicas.Width = 62;
            // 
            // pnlStep4Buttons
            // 
            this.pnlStep4Buttons.Controls.Add(this.btnStep4Cancel);
            this.pnlStep4Buttons.Controls.Add(this.btnStep4Continue);
            this.pnlStep4Buttons.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlStep4Buttons.Location = new System.Drawing.Point(5, 382);
            this.pnlStep4Buttons.Name = "pnlStep4Buttons";
            this.pnlStep4Buttons.Size = new System.Drawing.Size(744, 50);
            this.pnlStep4Buttons.TabIndex = 2;
            // 
            // btnStep4Cancel
            // 
            this.btnStep4Cancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnStep4Cancel.Font = new System.Drawing.Font("Corbel", 12F);
            this.btnStep4Cancel.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnStep4Cancel.Location = new System.Drawing.Point(663, 6);
            this.btnStep4Cancel.Name = "btnStep4Cancel";
            this.btnStep4Cancel.Size = new System.Drawing.Size(75, 41);
            this.btnStep4Cancel.TabIndex = 1;
            this.btnStep4Cancel.Text = "Cancel";
            this.btnStep4Cancel.UseVisualStyleBackColor = true;
            this.btnStep4Cancel.Click += new System.EventHandler(this.btnStep4Cancel_Click);
            // 
            // btnStep4Continue
            // 
            this.btnStep4Continue.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnStep4Continue.Font = new System.Drawing.Font("Corbel", 12F);
            this.btnStep4Continue.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnStep4Continue.Location = new System.Drawing.Point(573, 6);
            this.btnStep4Continue.Name = "btnStep4Continue";
            this.btnStep4Continue.Size = new System.Drawing.Size(84, 41);
            this.btnStep4Continue.TabIndex = 0;
            this.btnStep4Continue.Text = "&Continue";
            this.btnStep4Continue.UseVisualStyleBackColor = true;
            this.btnStep4Continue.Click += new System.EventHandler(this.btnStep4Continue_Click);
            // 
            // lblTitleStep4
            // 
            this.lblTitleStep4.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblTitleStep4.Font = new System.Drawing.Font("Corbel", 14.25F);
            this.lblTitleStep4.ForeColor = System.Drawing.Color.White;
            this.lblTitleStep4.Location = new System.Drawing.Point(5, 5);
            this.lblTitleStep4.Name = "lblTitleStep4";
            this.lblTitleStep4.Size = new System.Drawing.Size(744, 33);
            this.lblTitleStep4.TabIndex = 0;
            this.lblTitleStep4.Text = "Step 4/5 - DTOP Modules\' Docker Stacks Deployment";
            this.lblTitleStep4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tpgStep5
            // 
            this.tpgStep5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(2)))), ((int)(((byte)(44)))), ((int)(((byte)(95)))));
            this.tpgStep5.Controls.Add(this.lblStep5End);
            this.tpgStep5.Controls.Add(this.pnlStep5Buttons);
            this.tpgStep5.Controls.Add(this.lblTitleStep5);
            this.tpgStep5.Location = new System.Drawing.Point(4, 22);
            this.tpgStep5.Name = "tpgStep5";
            this.tpgStep5.Padding = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.tpgStep5.Size = new System.Drawing.Size(754, 437);
            this.tpgStep5.TabIndex = 5;
            this.tpgStep5.Text = "Step 5";
            // 
            // lblStep5End
            // 
            this.lblStep5End.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblStep5End.Font = new System.Drawing.Font("Corbel", 12F);
            this.lblStep5End.ForeColor = System.Drawing.Color.White;
            this.lblStep5End.Location = new System.Drawing.Point(5, 38);
            this.lblStep5End.Name = "lblStep5End";
            this.lblStep5End.Size = new System.Drawing.Size(744, 344);
            this.lblStep5End.TabIndex = 1;
            this.lblStep5End.Text = "**";
            this.lblStep5End.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pnlStep5Buttons
            // 
            this.pnlStep5Buttons.Controls.Add(this.btnOpenPortainer);
            this.pnlStep5Buttons.Controls.Add(this.btnStep5Quit);
            this.pnlStep5Buttons.Controls.Add(this.btnLaunchDTO);
            this.pnlStep5Buttons.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlStep5Buttons.Location = new System.Drawing.Point(5, 382);
            this.pnlStep5Buttons.Name = "pnlStep5Buttons";
            this.pnlStep5Buttons.Size = new System.Drawing.Size(744, 50);
            this.pnlStep5Buttons.TabIndex = 2;
            // 
            // btnOpenPortainer
            // 
            this.btnOpenPortainer.Font = new System.Drawing.Font("Corbel", 12F);
            this.btnOpenPortainer.Image = global::dtop_win_inst.Properties.Resources.portainer;
            this.btnOpenPortainer.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnOpenPortainer.Location = new System.Drawing.Point(151, 6);
            this.btnOpenPortainer.Name = "btnOpenPortainer";
            this.btnOpenPortainer.Size = new System.Drawing.Size(110, 41);
            this.btnOpenPortainer.TabIndex = 1;
            this.btnOpenPortainer.Text = "&Portainer";
            this.btnOpenPortainer.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnOpenPortainer.UseVisualStyleBackColor = true;
            this.btnOpenPortainer.Click += new System.EventHandler(this.btnOpenPortainer_Click);
            // 
            // btnStep5Quit
            // 
            this.btnStep5Quit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnStep5Quit.Font = new System.Drawing.Font("Corbel", 12F);
            this.btnStep5Quit.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnStep5Quit.Location = new System.Drawing.Point(663, 6);
            this.btnStep5Quit.Name = "btnStep5Quit";
            this.btnStep5Quit.Size = new System.Drawing.Size(75, 41);
            this.btnStep5Quit.TabIndex = 2;
            this.btnStep5Quit.Text = "&Quit";
            this.btnStep5Quit.UseVisualStyleBackColor = true;
            this.btnStep5Quit.Click += new System.EventHandler(this.btnStep5Quit_Click);
            // 
            // btnLaunchDTO
            // 
            this.btnLaunchDTO.Font = new System.Drawing.Font("Corbel", 12F);
            this.btnLaunchDTO.Image = global::dtop_win_inst.Properties.Resources.favicon_32x32;
            this.btnLaunchDTO.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnLaunchDTO.Location = new System.Drawing.Point(3, 6);
            this.btnLaunchDTO.Name = "btnLaunchDTO";
            this.btnLaunchDTO.Size = new System.Drawing.Size(142, 41);
            this.btnLaunchDTO.TabIndex = 0;
            this.btnLaunchDTO.Text = "Launch &DTO+";
            this.btnLaunchDTO.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnLaunchDTO.UseVisualStyleBackColor = true;
            this.btnLaunchDTO.Click += new System.EventHandler(this.btnLaunchDTO_Click);
            // 
            // lblTitleStep5
            // 
            this.lblTitleStep5.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblTitleStep5.Font = new System.Drawing.Font("Corbel", 14.25F);
            this.lblTitleStep5.ForeColor = System.Drawing.Color.White;
            this.lblTitleStep5.Location = new System.Drawing.Point(5, 5);
            this.lblTitleStep5.Name = "lblTitleStep5";
            this.lblTitleStep5.Size = new System.Drawing.Size(744, 33);
            this.lblTitleStep5.TabIndex = 0;
            this.lblTitleStep5.Text = "Step 5/5 - Checking deployed infrastructure and DTOP Running";
            this.lblTitleStep5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tpgUninstall
            // 
            this.tpgUninstall.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(2)))), ((int)(((byte)(44)))), ((int)(((byte)(95)))));
            this.tpgUninstall.Controls.Add(this.panel4);
            this.tpgUninstall.Controls.Add(this.panel3);
            this.tpgUninstall.Controls.Add(this.lblTitleUninstall);
            this.tpgUninstall.Location = new System.Drawing.Point(4, 22);
            this.tpgUninstall.Name = "tpgUninstall";
            this.tpgUninstall.Padding = new System.Windows.Forms.Padding(3, 3, 3, 3);
            this.tpgUninstall.Size = new System.Drawing.Size(754, 437);
            this.tpgUninstall.TabIndex = 7;
            this.tpgUninstall.Text = "Uninstall";
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.label2);
            this.panel4.Controls.Add(this.lvwUninstall);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(3, 36);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(748, 348);
            this.panel4.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Corbel", 12F);
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(14, 15);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(257, 19);
            this.label2.TabIndex = 0;
            this.label2.Text = "Please select the modules to uninstall";
            // 
            // lvwUninstall
            // 
            this.lvwUninstall.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lvwUninstall.CheckBoxes = true;
            this.lvwUninstall.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colUnistallShort,
            this.colUninstallName});
            this.lvwUninstall.Font = new System.Drawing.Font("Corbel", 9.75F);
            this.lvwUninstall.FullRowSelect = true;
            this.lvwUninstall.GridLines = true;
            this.lvwUninstall.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None;
            this.lvwUninstall.HideSelection = false;
            this.lvwUninstall.Location = new System.Drawing.Point(0, 37);
            this.lvwUninstall.MultiSelect = false;
            this.lvwUninstall.Name = "lvwUninstall";
            this.lvwUninstall.Size = new System.Drawing.Size(746, 303);
            this.lvwUninstall.TabIndex = 1;
            this.lvwUninstall.UseCompatibleStateImageBehavior = false;
            this.lvwUninstall.View = System.Windows.Forms.View.Details;
            // 
            // colUnistallShort
            // 
            this.colUnistallShort.Text = "Short";
            // 
            // colUninstallName
            // 
            this.colUninstallName.Text = "Module";
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.btnUninstallClearAll);
            this.panel3.Controls.Add(this.btnUninstallSelectAll);
            this.panel3.Controls.Add(this.btnUninstallCancel);
            this.panel3.Controls.Add(this.btnUninstallContinue);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel3.Location = new System.Drawing.Point(3, 384);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(748, 50);
            this.panel3.TabIndex = 3;
            // 
            // btnUninstallClearAll
            // 
            this.btnUninstallClearAll.Font = new System.Drawing.Font("Corbel", 12F);
            this.btnUninstallClearAll.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnUninstallClearAll.Location = new System.Drawing.Point(98, 6);
            this.btnUninstallClearAll.Name = "btnUninstallClearAll";
            this.btnUninstallClearAll.Size = new System.Drawing.Size(124, 41);
            this.btnUninstallClearAll.TabIndex = 1;
            this.btnUninstallClearAll.Text = "Clea&r Selection";
            this.btnUninstallClearAll.UseVisualStyleBackColor = true;
            this.btnUninstallClearAll.Click += new System.EventHandler(this.btnUninstallClearAll_Click);
            // 
            // btnUninstallSelectAll
            // 
            this.btnUninstallSelectAll.Font = new System.Drawing.Font("Corbel", 12F);
            this.btnUninstallSelectAll.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnUninstallSelectAll.Location = new System.Drawing.Point(3, 6);
            this.btnUninstallSelectAll.Name = "btnUninstallSelectAll";
            this.btnUninstallSelectAll.Size = new System.Drawing.Size(89, 41);
            this.btnUninstallSelectAll.TabIndex = 0;
            this.btnUninstallSelectAll.Text = "Select &All";
            this.btnUninstallSelectAll.UseVisualStyleBackColor = true;
            this.btnUninstallSelectAll.Click += new System.EventHandler(this.btnUninstallSelectAll_Click);
            // 
            // btnUninstallCancel
            // 
            this.btnUninstallCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnUninstallCancel.Font = new System.Drawing.Font("Corbel", 12F);
            this.btnUninstallCancel.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnUninstallCancel.Location = new System.Drawing.Point(668, 6);
            this.btnUninstallCancel.Name = "btnUninstallCancel";
            this.btnUninstallCancel.Size = new System.Drawing.Size(75, 41);
            this.btnUninstallCancel.TabIndex = 3;
            this.btnUninstallCancel.Text = "Cancel";
            this.btnUninstallCancel.UseVisualStyleBackColor = true;
            this.btnUninstallCancel.Click += new System.EventHandler(this.btnUninstallCancel_Click);
            // 
            // btnUninstallContinue
            // 
            this.btnUninstallContinue.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnUninstallContinue.Font = new System.Drawing.Font("Corbel", 12F);
            this.btnUninstallContinue.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnUninstallContinue.Location = new System.Drawing.Point(580, 6);
            this.btnUninstallContinue.Name = "btnUninstallContinue";
            this.btnUninstallContinue.Size = new System.Drawing.Size(82, 41);
            this.btnUninstallContinue.TabIndex = 2;
            this.btnUninstallContinue.Text = "&Continue";
            this.btnUninstallContinue.UseVisualStyleBackColor = true;
            this.btnUninstallContinue.Click += new System.EventHandler(this.btnUninstallContinue_Click);
            // 
            // lblTitleUninstall
            // 
            this.lblTitleUninstall.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblTitleUninstall.Font = new System.Drawing.Font("Corbel", 14.25F);
            this.lblTitleUninstall.ForeColor = System.Drawing.Color.White;
            this.lblTitleUninstall.Location = new System.Drawing.Point(3, 3);
            this.lblTitleUninstall.Name = "lblTitleUninstall";
            this.lblTitleUninstall.Size = new System.Drawing.Size(748, 33);
            this.lblTitleUninstall.TabIndex = 1;
            this.lblTitleUninstall.Text = "Uninstall";
            this.lblTitleUninstall.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tpgRedeploy
            // 
            this.tpgRedeploy.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(2)))), ((int)(((byte)(44)))), ((int)(((byte)(95)))));
            this.tpgRedeploy.Controls.Add(this.panel5);
            this.tpgRedeploy.Controls.Add(this.panel6);
            this.tpgRedeploy.Controls.Add(this.lblTitleRedeploy);
            this.tpgRedeploy.Location = new System.Drawing.Point(4, 22);
            this.tpgRedeploy.Name = "tpgRedeploy";
            this.tpgRedeploy.Padding = new System.Windows.Forms.Padding(3, 3, 3, 3);
            this.tpgRedeploy.Size = new System.Drawing.Size(754, 437);
            this.tpgRedeploy.TabIndex = 8;
            this.tpgRedeploy.Text = "Redeploy";
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.label3);
            this.panel5.Controls.Add(this.lvwRedeploy);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel5.Location = new System.Drawing.Point(3, 36);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(748, 348);
            this.panel5.TabIndex = 7;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Corbel", 12F);
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(14, 15);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(261, 19);
            this.label3.TabIndex = 0;
            this.label3.Text = "Please select the modules to redeploy";
            // 
            // lvwRedeploy
            // 
            this.lvwRedeploy.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lvwRedeploy.CheckBoxes = true;
            this.lvwRedeploy.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colRedeployShort,
            this.colRedeployName});
            this.lvwRedeploy.Font = new System.Drawing.Font("Corbel", 9.75F);
            this.lvwRedeploy.FullRowSelect = true;
            this.lvwRedeploy.GridLines = true;
            this.lvwRedeploy.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None;
            this.lvwRedeploy.HideSelection = false;
            this.lvwRedeploy.Location = new System.Drawing.Point(0, 37);
            this.lvwRedeploy.MultiSelect = false;
            this.lvwRedeploy.Name = "lvwRedeploy";
            this.lvwRedeploy.Size = new System.Drawing.Size(746, 303);
            this.lvwRedeploy.TabIndex = 1;
            this.lvwRedeploy.UseCompatibleStateImageBehavior = false;
            this.lvwRedeploy.View = System.Windows.Forms.View.Details;
            // 
            // colRedeployShort
            // 
            this.colRedeployShort.Text = "Short";
            // 
            // colRedeployName
            // 
            this.colRedeployName.Text = "Module";
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.btnRedeployClearAll);
            this.panel6.Controls.Add(this.btnRedeploySelectAll);
            this.panel6.Controls.Add(this.btnRedeployCancel);
            this.panel6.Controls.Add(this.btnRedeployContinue);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel6.Location = new System.Drawing.Point(3, 384);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(748, 50);
            this.panel6.TabIndex = 6;
            // 
            // btnRedeployClearAll
            // 
            this.btnRedeployClearAll.Font = new System.Drawing.Font("Corbel", 12F);
            this.btnRedeployClearAll.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnRedeployClearAll.Location = new System.Drawing.Point(98, 6);
            this.btnRedeployClearAll.Name = "btnRedeployClearAll";
            this.btnRedeployClearAll.Size = new System.Drawing.Size(124, 41);
            this.btnRedeployClearAll.TabIndex = 1;
            this.btnRedeployClearAll.Text = "Clea&r Selection";
            this.btnRedeployClearAll.UseVisualStyleBackColor = true;
            this.btnRedeployClearAll.Click += new System.EventHandler(this.btnRedeployClearAll_Click);
            // 
            // btnRedeploySelectAll
            // 
            this.btnRedeploySelectAll.Font = new System.Drawing.Font("Corbel", 12F);
            this.btnRedeploySelectAll.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnRedeploySelectAll.Location = new System.Drawing.Point(3, 6);
            this.btnRedeploySelectAll.Name = "btnRedeploySelectAll";
            this.btnRedeploySelectAll.Size = new System.Drawing.Size(89, 41);
            this.btnRedeploySelectAll.TabIndex = 0;
            this.btnRedeploySelectAll.Text = "Select &All";
            this.btnRedeploySelectAll.UseVisualStyleBackColor = true;
            this.btnRedeploySelectAll.Click += new System.EventHandler(this.btnRedeploySelectAll_Click);
            // 
            // btnRedeployCancel
            // 
            this.btnRedeployCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRedeployCancel.Font = new System.Drawing.Font("Corbel", 12F);
            this.btnRedeployCancel.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnRedeployCancel.Location = new System.Drawing.Point(668, 6);
            this.btnRedeployCancel.Name = "btnRedeployCancel";
            this.btnRedeployCancel.Size = new System.Drawing.Size(75, 41);
            this.btnRedeployCancel.TabIndex = 3;
            this.btnRedeployCancel.Text = "Cancel";
            this.btnRedeployCancel.UseVisualStyleBackColor = true;
            this.btnRedeployCancel.Click += new System.EventHandler(this.btnRedeployCancel_Click);
            // 
            // btnRedeployContinue
            // 
            this.btnRedeployContinue.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRedeployContinue.Font = new System.Drawing.Font("Corbel", 12F);
            this.btnRedeployContinue.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnRedeployContinue.Location = new System.Drawing.Point(580, 6);
            this.btnRedeployContinue.Name = "btnRedeployContinue";
            this.btnRedeployContinue.Size = new System.Drawing.Size(82, 41);
            this.btnRedeployContinue.TabIndex = 2;
            this.btnRedeployContinue.Text = "&Continue";
            this.btnRedeployContinue.UseVisualStyleBackColor = true;
            this.btnRedeployContinue.Click += new System.EventHandler(this.btnRedeployContinue_Click);
            // 
            // lblTitleRedeploy
            // 
            this.lblTitleRedeploy.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblTitleRedeploy.Font = new System.Drawing.Font("Corbel", 14.25F);
            this.lblTitleRedeploy.ForeColor = System.Drawing.Color.White;
            this.lblTitleRedeploy.Location = new System.Drawing.Point(3, 3);
            this.lblTitleRedeploy.Name = "lblTitleRedeploy";
            this.lblTitleRedeploy.Size = new System.Drawing.Size(748, 33);
            this.lblTitleRedeploy.TabIndex = 5;
            this.lblTitleRedeploy.Text = "Redeploy modules";
            this.lblTitleRedeploy.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnClearLog
            // 
            this.btnClearLog.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClearLog.Font = new System.Drawing.Font("Corbel", 12F);
            this.btnClearLog.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnClearLog.Location = new System.Drawing.Point(487, 6);
            this.btnClearLog.Name = "btnClearLog";
            this.btnClearLog.Size = new System.Drawing.Size(84, 41);
            this.btnClearLog.TabIndex = 4;
            this.btnClearLog.Text = "Clear log";
            this.btnClearLog.UseVisualStyleBackColor = true;
            this.btnClearLog.Click += new System.EventHandler(this.btnClearLog_Click);
            // 
            // FrmInstall
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(2)))), ((int)(((byte)(44)))), ((int)(((byte)(95)))));
            this.ClientSize = new System.Drawing.Size(762, 518);
            this.ControlBox = false;
            this.Controls.Add(this.tclWizard);
            this.Controls.Add(this.pnlHeader);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(777, 532);
            this.Name = "FrmInstall";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Title";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FrmInstall_FormClosed);
            this.Load += new System.EventHandler(this.FrmInstall_Load);
            ((System.ComponentModel.ISupportInitialize)(this.errDockerCheck)).EndInit();
            this.tclWizard.ResumeLayout(false);
            this.tpgConfiguration.ResumeLayout(false);
            this.tpgConfiguration.PerformLayout();
            this.gbxDockerInfo.ResumeLayout(false);
            this.pnlDockerInfo.ResumeLayout(false);
            this.pnlDockerInfo.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.tpgLicense.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.pnlLicenseButtons.ResumeLayout(false);
            this.pnlLicenseButtons.PerformLayout();
            this.tpgStep1.ResumeLayout(false);
            this.pnlStep1Buttons.ResumeLayout(false);
            this.tpgStep2.ResumeLayout(false);
            this.pnlStep2.ResumeLayout(false);
            this.pnlStep2.PerformLayout();
            this.pnlStep2Buttons.ResumeLayout(false);
            this.tpgStep3.ResumeLayout(false);
            this.pnlStep3.ResumeLayout(false);
            this.pnlStep3Buttons.ResumeLayout(false);
            this.tpgStep4.ResumeLayout(false);
            this.pnlStep4Buttons.ResumeLayout(false);
            this.tpgStep5.ResumeLayout(false);
            this.pnlStep5Buttons.ResumeLayout(false);
            this.tpgUninstall.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.tpgRedeploy.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private WizardTabControl tclWizard;
        private System.Windows.Forms.TabPage tpgLicense;
        private System.Windows.Forms.TabPage tpgStep1;
        private System.Windows.Forms.Label lblTitleLicense;
        private System.Windows.Forms.TextBox tbxLicense;
        private System.Windows.Forms.Label lblTitleStep1;
        private System.Windows.Forms.TabPage tpgStep2;
        private System.Windows.Forms.Label lblTitleStep2;
        private System.Windows.Forms.TabPage tpgStep3;
        private System.Windows.Forms.Label lblTitleStep3;
        private System.Windows.Forms.TabPage tpgStep4;
        private System.Windows.Forms.Label lblTitleStep4;
        private System.Windows.Forms.TabPage tpgStep5;
        private System.Windows.Forms.Label lblTitleStep5;
        private System.Windows.Forms.Panel pnlLicenseButtons;
        private System.Windows.Forms.Button btnLicenseOK;
        private System.Windows.Forms.Panel pnlStep1Buttons;
        private System.Windows.Forms.Panel pnlStep2Buttons;
        private System.Windows.Forms.Panel pnlStep3Buttons;
        private System.Windows.Forms.Panel pnlStep4Buttons;
        private System.Windows.Forms.Panel pnlStep5Buttons;
        private System.Windows.Forms.Button btnStep1Reject;
        private System.Windows.Forms.Button btnStep1Accept;
        private System.Windows.Forms.Label lblStep1;
        private System.Windows.Forms.Panel pnlStep2;
        private System.Windows.Forms.Panel pnlStep3;
        private System.Windows.Forms.Label lblStep2Select;
        private System.Windows.Forms.ListView lvwSelectModules;
        private System.Windows.Forms.ColumnHeader colShort;
        private System.Windows.Forms.ColumnHeader colModule;
        private System.Windows.Forms.Button btnStep2Cancel;
        private System.Windows.Forms.Button btnStep2OK;
        private System.Windows.Forms.Button btnSelectNone;
        private System.Windows.Forms.Button btnSelectAll;
        private System.Windows.Forms.Button btnStep3Cancel;
        private System.Windows.Forms.Button btnStep3Continue;
        private System.Windows.Forms.Panel pnlHeader;
        private System.Windows.Forms.ListView lvwPulledImages;
        private System.Windows.Forms.ColumnHeader colImageCreate;
        private System.Windows.Forms.ColumnHeader colImageRepository;
        private System.Windows.Forms.ColumnHeader colImageTag;
        private System.Windows.Forms.ColumnHeader colImageSize;
        private System.Windows.Forms.ListView lvwServices;
        private System.Windows.Forms.ColumnHeader colServiceImage;
        private System.Windows.Forms.ColumnHeader colServiceName;
        private System.Windows.Forms.ColumnHeader colServiceReplicas;
        private System.Windows.Forms.Label lblStep5End;
        private System.Windows.Forms.Button btnOpenPortainer;
        private System.Windows.Forms.Button btnStep5Quit;
        private System.Windows.Forms.Button btnLaunchDTO;
        private System.Windows.Forms.Button btnStep4Cancel;
        private System.Windows.Forms.Button btnStep4Continue;
        private System.Windows.Forms.TabPage tpgConfiguration;
        private System.Windows.Forms.Label lblTitleConfiguration;
        private System.Windows.Forms.TextBox tbxDomain;
        private System.Windows.Forms.TextBox tbxData;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnConfigQuit;
        private System.Windows.Forms.Button btnConfigContinue;
        private System.Windows.Forms.Label lblDtopDomain;
        private System.Windows.Forms.Label lblDtopData;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblInstallationType;
        private System.Windows.Forms.CheckBox ckxDryRun;
        private System.Windows.Forms.Button btnRedeploy;
        private System.Windows.Forms.Button btnUninstall;
        private System.Windows.Forms.TabPage tpgUninstall;
        private System.Windows.Forms.TabPage tpgRedeploy;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ListView lvwUninstall;
        private System.Windows.Forms.ColumnHeader colUnistallShort;
        private System.Windows.Forms.ColumnHeader colUninstallName;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button btnUninstallCancel;
        private System.Windows.Forms.Button btnUninstallContinue;
        private System.Windows.Forms.Label lblTitleUninstall;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ListView lvwRedeploy;
        private System.Windows.Forms.ColumnHeader colRedeployShort;
        private System.Windows.Forms.ColumnHeader colRedeployName;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Button btnRedeployClearAll;
        private System.Windows.Forms.Button btnRedeploySelectAll;
        private System.Windows.Forms.Button btnRedeployCancel;
        private System.Windows.Forms.Button btnRedeployContinue;
        private System.Windows.Forms.Label lblTitleRedeploy;
        private System.Windows.Forms.Button btnUninstallClearAll;
        private System.Windows.Forms.Button btnUninstallSelectAll;
        private System.Windows.Forms.GroupBox gbxDockerInfo;
        private System.Windows.Forms.Label lblDockerStatus;
        private System.Windows.Forms.Panel pnlDockerInfo;
        private System.Windows.Forms.TextBox tbxDockerProc;
        private System.Windows.Forms.TextBox tbxDockerTotalMem;
        private System.Windows.Forms.Label lblCPUs;
        private System.Windows.Forms.Label lblMemory;
        private System.Windows.Forms.ErrorProvider errDockerCheck;
        private System.Windows.Forms.ColumnHeader colModuleVersion;
        private System.Windows.Forms.ColumnHeader colModuleStatus;
        private System.Windows.Forms.Button btnCheckUpdates;
        private System.Windows.Forms.Label lblDiskSpace;
        private System.Windows.Forms.TextBox tbxDiskSpace;
        private System.Windows.Forms.Button btnClearLog;
    }
}

