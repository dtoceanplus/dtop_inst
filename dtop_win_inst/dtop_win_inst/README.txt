=================================
This file is part of the DTOceanPlus application.
hosted at http://gitlab.com/dtoceanplus

DTOceanPlus is distributed under the GNU AFFERO GENERAL PUBLIC LICENSE.
See LICENSE file for more details.

DTOceanPlus Win Install is the installation program to install, update, uninstall the application.
DTOceanPlus Monitor is a tool to monitor the different Docker services composing the DTOceanPlus application.

-----------------

SharpZipLib library is released under the MIT License.
https://github.com/icsharpcode/SharpZipLib

DTOceanPlus icons are Copyright

Icons are part of the FatCow icons licensed under Creative Commons Attribution 3.0 License.
http://www.fatcow.com/free-icons
