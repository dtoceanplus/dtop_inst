// This is DTOceanPlus Windows Installation.
// This program can be used to install, update or uninstall the DTOceanPlus suite of tools.
// Copyright (C) 2021 OPEN CASCADE
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
// License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dtop_win_inst
{
    public class DtopModule
    {
        public string Name { get; private set; }
        public string ShortName { get; private set; }

        public string Tag { get; set; }
        public string Registry { get; set; }

        public DateTime CreationDate { get; set; }
        public string ImageSize { get; set; }

        public bool Succeed { get; set; }

        public DtopModule(string name, string shortName)
        {
            Name = name;
            ShortName = shortName;
        }

        public string BackEndUrl
        {
            get
            {
                return String.Format("{0}/{1}_backend:{2}", Registry, ShortName.ToLowerInvariant(), Tag);
            }
        }
        public string FrontEndUrl
        {
            get
            {
                return String.Format("{0}/{1}_frontend:{2}", Registry, ShortName.ToLowerInvariant(), Tag);
            }
        }
    }
}
