// This is DTOceanPlus Windows Installation.
// This program can be used to install, update or uninstall the DTOceanPlus suite of tools.
// Copyright (C) 2021 OPEN CASCADE
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
// License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.
﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace dtop_win_inst
{
    public partial class FrmUninstall: Form
    {
        private List<DtopModule> m_toUninstall = new List<DtopModule>();
        private bool m_cleanAll = false;

        public FrmUninstall(List<DtopModule> modules, bool all)
        {
            InitializeComponent();
            
            m_toUninstall.AddRange(modules);
            m_cleanAll = all;
        }

        public void StartUninstall()
        {
            this.Cursor = Cursors.WaitCursor;
            backgroundWorker1.RunWorkerAsync();
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            int progress = 0;
            int nbStep = m_toUninstall.Count + 2;
            if (m_cleanAll) nbStep += 3;
            int step = 100 / nbStep;

            foreach (var module in m_toUninstall)
            {
                Logger.Write("Removing {0}... ", module.Name);
                BackupData(module);
                backgroundWorker1.ReportProgress(progress, String.Format("Removing {0}...", module.Name));
                if (DockerCommand.RemoveStack(module.ShortName.ToLower(), out string err)) Logger.WriteLine("Done");
                else Logger.WriteError(err);                
                progress += step;
            }

            if (m_cleanAll)
            {
                backgroundWorker1.ReportProgress(progress, "Basic Stack Undeploy...");
                BasicStackUndeploy("portainer");
                BasicStackUndeploy("tra");
                progress += step;

                backgroundWorker1.ReportProgress(progress, "Removing Traefik Network...");
                RemoveNetwork("traefik-public");
                progress += step;

                backgroundWorker1.ReportProgress(progress, "Leaving Swarm...");
                LeaveSwarmMode();
                progress += step;
            }

            if (m_toUninstall.Any(m => m.ShortName == "MM"))
            {
                backgroundWorker1.ReportProgress(progress, "Removing Volume mm_mysql_data...");
                RemoveVolume("mm_mysql_data");
                progress += step;
            }

            if (m_toUninstall.Any(m => m.ShortName == "CM"))
            {
                backgroundWorker1.ReportProgress(progress, "Remove Voluming cm_pg_data...");
                RemoveVolume("cm_pg_data");
                progress += step;
            }

            backgroundWorker1.ReportProgress(100, "Finished.");
        }

        private delegate void SetProgressDelegate(int p, string details);
        private void UpdateProgress(int p, string details)
        {
            if (progressBar1.InvokeRequired)
            {
                progressBar1.BeginInvoke(new SetProgressDelegate(UpdateProgress), p, details);
            }
            else
            {
                progressBar1.Value = p <= 100 ? p : 100;
                lblDetails.Text = details;
            }
        }

        private void backgroundWorker1_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            UpdateProgress(e.ProgressPercentage, e.UserState != null ? e.UserState.ToString() : "");
        }

        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (m_cleanAll)
            {
                var dr = MessageBox.Show("Do you want to remove all unused data?",
                    Properties.Resources.AppTitle, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (dr == DialogResult.Yes)
                {
                    try
                    {
                        this.Cursor = Cursors.WaitCursor;
                        Logger.WriteLine("-- Docker Prune all:");
                        Logger.WriteLine(DockerCommand.PruneAll());
                        Logger.WriteLine("--");
                    }
                    finally
                    {
                        this.Cursor = Cursors.Default;
                    }
                }
            }
            this.Close();
        }

        private void BackupData(DtopModule module)
        {
            if (module.ShortName == "MM")
            {
                BackupModule("mm_mm_database", "/var/lib/mysql", Path.Combine(InstallConfig.Instance().Data, "mm", "db"));
            }
            else if (module.ShortName == "CM")
            {
                BackupModule("cm_cm_backend", "/var/lib/postgresql", Path.Combine(InstallConfig.Instance().Data, "cm", "db"));
            }
        }

        private void BackupModule(string container, string source, string destination)
        {
            string id = DockerCommand.GetContainerId(container);
            if (String.IsNullOrEmpty(id))
            {
                Logger.WriteError("  Container not found: {0}", container);
            }
            else
            {
                Logger.Write("  Coping database from docker volume: {0} --> {1} ... ", source, destination);
                if (DockerCommand.Copy(String.Format("{0}:{1}", id, source), destination, out string err)) Logger.WriteLine("Done");
                else Logger.WriteError(err);
            }
        }

        private void BasicStackUndeploy(string stack)
        {
            if (DockerCommand.StackPs(stack))
            {
                Logger.Write("Removing {0}... ", stack);
                if (DockerCommand.RemoveStack(stack, out string err)) Logger.WriteLine("Done");
                else Logger.WriteError(err);
            }
            else
            {
                Logger.WriteLine("The service {0} does not exist", stack);
            }
        }

        private void RemoveNetwork(string network)
        {
            if (DockerCommand.CheckNetwork(network))
            {
                Logger.Write("Removing {0}... ", network);
                if (DockerCommand.RemoveNetwork(network, out string err)) Logger.WriteLine("Done");
                else Logger.WriteLine(err);
            }
            else
            {
                Logger.WriteLine("Network {0} does not exist", network);
            }
        }

        private void LeaveSwarmMode()
        {
            if (DockerCommand.IsSwarMode())
            {
                Logger.Write("Leaving Swarm mode...");
                if (DockerCommand.LeaveSwarm(true, out string err)) Logger.WriteLine("Done");
                else Logger.WriteError(err);
            }
            else
            {
                Logger.WriteLine("The node is in standalone mode");
            }
        }

        private void RemoveVolume(string volume)
        {
            if (DockerCommand.InspectVolume(volume))
            {
                Logger.Write("Removing Volume: {0}... ", volume);
                if (DockerCommand.RemoveVolume(volume, out string err)) Logger.WriteLine("Done");
                else Logger.WriteError(err);
            }
            else
            {
                Logger.WriteLine("Volume {0} does not exist", volume);
            }
        }
    }
}
