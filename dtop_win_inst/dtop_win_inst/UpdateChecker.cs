// This is DTOceanPlus Windows Installation.
// This program can be used to install, update or uninstall the DTOceanPlus suite of tools.
// Copyright (C) 2021 OPEN CASCADE
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
// License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Net;

namespace dtop_win_inst
{
    /// <summary>
    /// Utilitary class to check for updates.
    /// </summary>
    public static class UpdateChecker
    {
        /// <summary>
        /// Gets new version of dtop_inst.env file.
        /// </summary>
        /// <param name="path">Path where to download the file from GitLab.</param>
        /// <returns>Path to the new env file.</returns>
        private static string DownloadDtopEnv(string path, string dataPath)
        {
            string newEnvFile = "";
            
            // Download file from GitLab
            bool success = false;
            using (var client = new WebClient())
            {
                client.Headers["Private-Token"] = Properties.Settings.Default.UpdateToken;
                client.DownloadFile(Properties.Settings.Default.UpdateUrl, path);
                success = true;
            }

            if (success)
            {
                // get file content from JSON file returned by GitLab.
                string envFile = "";
                using (StreamReader sr = new StreamReader(path))
                {
                    string full = sr.ReadToEnd();
                    int index = full.IndexOf("\"content\":") + 11;
                    string content = full.Substring(index, full.Length - index - 2);
                    byte[] data = Convert.FromBase64String(content);
                    envFile = Encoding.UTF8.GetString(data);
                }

                // create the new env file
                if (!String.IsNullOrEmpty(envFile))
                {
                    if (Directory.Exists(dataPath))
                    {
                        // if install directory exists create the file here
                        newEnvFile = Path.Combine(dataPath, "dtop_inst.env.update");
                    }
                    else
                    {
                        // if install path not set reuse tmp file
                        newEnvFile = path;
                    }
                    using (StreamWriter sw = new StreamWriter(newEnvFile))
                    {
                        sw.Write(envFile);
                    }
                }
            }

           return newEnvFile;
        }

        /// <summary>
        /// Parse env file to get versions of images.
        /// </summary>
        /// <param name="path">Path of the env file to parse.</param>
        /// <returns>Dictionary with key = module short name in lower case, value = image's version.</returns>
        private static Dictionary<string, string> ParseEnvFile(string path)
        {
            Dictionary<string, string> versions = new Dictionary<string, string>();

            string envFile = "";
            using (StreamReader sr = new StreamReader(path))
            {
                envFile = sr.ReadToEnd();
            }

            var lines = envFile.Split('\n');
                
            foreach (var line in lines)
            {
                // skip emty lines & comments
                if (String.IsNullOrWhiteSpace(line) || line.TrimStart().StartsWith("#")) continue;

                if (line.Contains("IMAGE_TAG"))
                {
                    var elts = line.Trim().Split('=');
                    string module = elts[0].Split('_')[0];
                    versions[module] = elts[1];
                }
            }

            return versions;
        }

        /// <summary>
        /// Checks for update of images.
        /// </summary>
        /// <returns>Dictionary with key = module short name in lower case, value = image's available version.</returns>
        public static Dictionary<string, string> CheckUpdates(string dir)
        {
            // download file
            string env = Path.GetTempFileName();
            string newEnv = DownloadDtopEnv(env, dir);

            if (String.IsNullOrEmpty(newEnv)) return null; // throw exc?

            // parse file
            var res = ParseEnvFile(newEnv);
            if (File.Exists(env)) File.Delete(env);
            return res;
        }
    }
}
