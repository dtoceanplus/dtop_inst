// This is DTOceanPlus Windows Installation.
// This program can be used to install, update or uninstall the DTOceanPlus suite of tools.
// Copyright (C) 2021 OPEN CASCADE
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
// License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.
﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace dtop_win_inst
{
    public partial class WizardTabControl : TabControl
    {
        private bool m_simpleMode; // false
        private bool m_simpleModeInDesign; // false

        [DefaultValue(false)]
        public bool SimpleMode
        {
            get { return m_simpleMode; }
            set
            {
                m_simpleMode = value;
                RecreateHandle();
            }
        }
        [DefaultValue(false)]
        public bool SimpleModeInDesign
        {
            get { return m_simpleModeInDesign; }
            set
            {
                m_simpleModeInDesign = value;
                RecreateHandle();
            }
        }

        public override Rectangle DisplayRectangle
        {
            get
            {
                if (m_simpleMode && (!DesignMode || m_simpleModeInDesign))
                {
                    return new Rectangle(0, 0, Width, Height);
                }
                else
                {
                    return base.DisplayRectangle;
                }
            }
        }
    }
}
