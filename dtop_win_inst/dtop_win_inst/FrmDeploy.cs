// This is DTOceanPlus Windows Installation.
// This program can be used to install, update or uninstall the DTOceanPlus suite of tools.
// Copyright (C) 2021 OPEN CASCADE
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
// License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.
﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace dtop_win_inst
{
    public partial class FrmDeploy : Form
    {
        private List<DtopModule> m_toInstall = new List<DtopModule>();

        public FrmDeploy(List<DtopModule> modules)
        {
            InitializeComponent();
            
            m_toInstall.AddRange(modules);
        }

        public void StartDeployment()
        {
            backgroundWorker1.RunWorkerAsync();
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            int progress = 0;

            backgroundWorker1.ReportProgress(progress, "Configure Swarm...");
            CheckSwarmMode();
            progress += 5;
            
            backgroundWorker1.ReportProgress(progress, "Configure Network...");
            CheckTraefikNetwork();
            progress += 5;
            
            backgroundWorker1.ReportProgress(progress, "Basic Deploy...");
            BasicStackDeploy();
            progress += 5;
            
            int step = (100 - progress) / m_toInstall.Count;
            foreach (var module in m_toInstall)
            {
                if (!module.Succeed) continue;
                backgroundWorker1.ReportProgress(progress, String.Format("Deploying {0}...", module.Name));
                Logger.WriteLine("Deploying MODULE = {0}", module.ShortName);
                DataPrepare(module);
                DeployModule(module);
                progress += step;
            }

            backgroundWorker1.ReportProgress(100, "Finished.");
        }

        private delegate void SetProgressDelegate(int p, string details);
        private void UpdateProgress(int p, string details)
        {
            if (progressBar1.InvokeRequired)
            {
                progressBar1.BeginInvoke(new SetProgressDelegate(UpdateProgress), p, details);
            }
            else
            {
                progressBar1.Value = p <= 100 ? p : 100;
                lblDetails.Text = details;
            }
        }

        private void backgroundWorker1_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            UpdateProgress(e.ProgressPercentage, e.UserState != null ? e.UserState.ToString() : "");
        }

        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            this.Close();
        }

        private bool CheckSwarmMode()
        {
            bool res = true;
            if (DockerCommand.IsSwarMode())
            {
                Logger.WriteLine("The node is already in a swarm mode");
            }
            else
            {
                Logger.Write("The node is in standalone mode. Initializing swarm mode... ");
                if (DockerCommand.InitSwarm(out string err)) Logger.WriteLine("Done");
                else
                {
                    res = false;
                    Logger.WriteError("Swarm init failed: {0}", err);
                }
            }
            return res;
        }
        private bool CheckTraefikNetwork()
        {
            bool res = true;
            if (DockerCommand.CheckNetwork("traefik-public"))
            {
                Logger.WriteLine("Docker network \"traefik-public\" already exists");
            }
            else
            {
                Logger.Write("Creating Docker network \"traefik-public\"... ");
                if (DockerCommand.CreateNetwork("traefik-public", out string err)) Logger.WriteLine("Done");
                else
                {
                    res = false;
                    Logger.WriteError("Network not created: {0}", err);
                }
            }
            return res;
        }
        private void BasicStackDeploy()
        {
            var env = InstallConfig.Instance().Environment();

            if (DockerCommand.StackPs("tra"))
            {
                Logger.WriteLine("The service \"traefik\" already exists");
            }
            else
            {
                Logger.Write("Deploying serivce \"traefik\"... ");
                if (DockerCommand.DeployStack("tra", Path.Combine(InstallConfig.Instance().InstallPath, "stacks", "tra_stack.yml"), env, out string err))
                    Logger.WriteLine("Done");
                else
                    Logger.WriteError(err);
            }

            string portainerDir = Path.Combine(InstallConfig.Instance().Data, "portainer");
            if (!Directory.Exists(portainerDir)) Directory.CreateDirectory(portainerDir);
            if (DockerCommand.StackPs("portainer"))
            {
                Logger.WriteLine("The service \"portainer\" already exists");
            }
            else
            {
                Logger.Write("Deploying serivce \"portainer\"... ");
                if (DockerCommand.DeployStack("portainer", Path.Combine(InstallConfig.Instance().InstallPath, "stacks", "portainer_stack.yml"), env, out string err))
                    Logger.WriteLine("Done");
                else
                    Logger.WriteError(err);
            }
        }

        private void DataPrepare(DtopModule module)
        {
            string code = module.ShortName.ToLower();

            Utils.CopyDir(Path.Combine(InstallConfig.Instance().InstallPath, "init_data", code, "dumps"), Path.Combine(InstallConfig.Instance().Data, code, "dumps"));

            string storage = "db";
            if (code == "sc" || code == "sk" || code == "esa") storage = "storage";
            Directory.CreateDirectory(Path.Combine(InstallConfig.Instance().Data, code, storage));

            if (code == "sc")
            {
                Directory.CreateDirectory(Path.Combine(InstallConfig.Instance().Data, code, "databases"));
                if (File.Exists(Path.Combine(InstallConfig.Instance().Data, code, "dabases", "SiteCharacterisation_Main-Database", "readme.md")))
                {
                    Logger.WriteLine("The basic data files of DTOP SC module seem to be already installed into ${DTOP_DATA}/sc/databases and can be used");
                }
                else if (InstallConfig.Instance().SiteDatabaseArchiveDefined)
                {
                    string tgz = Path.Combine(InstallConfig.Instance().InstallPath, InstallConfig.Instance().SiteDatabaseArchive);
                    if (!File.Exists(tgz))
                    {
                        Logger.WriteLine("It seems that the archive '{0}' of the basic data files for DTOP SC module was not downloaded in advance.",
                            InstallConfig.Instance().SiteDatabaseArchive);
                        Logger.WriteLine("It was not found in {0} and therefore cannot be installed into {1}/sc/databases",
                            InstallConfig.Instance().InstallPath, Path.Combine(InstallConfig.Instance().Data));
                    }
                    else
                    {
                        string dest = Path.Combine(InstallConfig.Instance().Data, "sc");
                        Utils.ExtractTgz(tgz, dest);
                    }
                }
            }
            else if (code == "mm")
            {
                Utils.CopyDir(Path.Combine(InstallConfig.Instance().InstallPath, "init_data", code, "scripts"), Path.Combine(InstallConfig.Instance().Data, code, "scripts"));
                CreateVolume("mm_mysql_data");
            }
            else if (code == "cm")
            {
                Utils.CopyDir(Path.Combine(InstallConfig.Instance().InstallPath, "init_data", code, "scripts"), Path.Combine(InstallConfig.Instance().Data, code, "scripts"));
                Directory.CreateDirectory(Path.Combine(InstallConfig.Instance().Data, code, "db"));
                CreateVolume("cm_pg_data");
            }
        }

        private void CreateVolume(string volume)
        {
            if (DockerCommand.InspectVolume(volume))
            {
                Logger.WriteLine("Docker volume \"{0}\" already exists.", volume);
            }
            else
            {
                Logger.Write("Creating Docker volume \"{0}\" for database data storage... ", volume);
                if (DockerCommand.CreateVolume(volume)) Logger.WriteLine("Done");
                else
                {
                    Logger.WriteError("Volume not create: {0}", volume);
                }
            }
        }

        private void DeployModule(DtopModule module)
        {
            string code = module.ShortName.ToLower();
            string stackFile = Path.Combine(InstallConfig.Instance().InstallPath, "stacks", code + "_stack.yml");
            if (Properties.Settings.Default.AuthRequired)
            {
                Logger.Write("  Login to {0}... ", module.Registry);
                if (DockerCommand.LoginRegistry(module.Registry, out string err)) Logger.WriteLine("Done");
                else Logger.WriteError(err);
            }

            Logger.Write("  Deploying... ");
            if (DockerCommand.DeployStack(code, stackFile, InstallConfig.Instance().Environment(module.ShortName), out string derr)) Logger.WriteLine("Done");
            else Logger.WriteError(derr);

            if (Properties.Settings.Default.AuthRequired)
            {
                Logger.Write("  Logout from {0}... ", module.Registry);
                if (DockerCommand.LogoutRegistry(module.Registry, out string err)) Logger.WriteLine("Done");
                else Logger.WriteError(err );
            }
        }

    }
}
