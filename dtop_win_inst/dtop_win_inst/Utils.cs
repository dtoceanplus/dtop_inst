// This is DTOceanPlus Windows Installation.
// This program can be used to install, update or uninstall the DTOceanPlus suite of tools.
// Copyright (C) 2021 OPEN CASCADE
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
// License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.
﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ICSharpCode.SharpZipLib.GZip;
using ICSharpCode.SharpZipLib.Tar;

namespace dtop_win_inst
{
    public static class Utils
    {
        public static string ReadLicense(string path)
        {
            string license = "";
            if (File.Exists(path))
            {
                using (StreamReader sr = new StreamReader(path))
                {
                    license = sr.ReadToEnd();
                }
            }
            else
            {
                license = String.Format("License File Not Found: {0}", path);
            }
            return license;
        }

        public static void OpenUrl(string url)
        {
            if (File.Exists(Properties.Settings.Default.BrowserExe))
            {
                Process.Start(Properties.Settings.Default.BrowserExe, url);
            }
            else
            {
                Process.Start(url);
            }
        }

        public static void CopyDir(string src, string dest)
        {
            if (!Directory.Exists(src)) return;
            if (!Directory.Exists(dest)) Directory.CreateDirectory(dest);
            foreach (string file in Directory.GetFiles(src))
            {
                string destFile = Path.Combine(dest, Path.GetFileName(file));
                if (!File.Exists(destFile)) File.Copy(file, destFile, false);
            }
        }

        public static void ExtractTgz(string archive, string destFolder)
        {
            using (Stream inStream = File.OpenRead(archive))
            using (Stream gzipStream = new GZipInputStream(inStream))
            {
                TarArchive tarArchive = TarArchive.CreateInputTarArchive(gzipStream);
                tarArchive.ExtractContents(destFolder);
                tarArchive.Close();
            }
        }

        public static string GetStrSize(long size)
        {
            string[] units = new string[] { "B", "KB", "MB", "GB", "TB" };

            double sz = size;
            int nb = 0;
            while (sz > 1000 && nb < units.Length)
            {
                sz = sz / 1000;
                nb++;
            }

            return String.Format("{0:F2} {1}", sz, units[nb]);
        }

        public static double GetSizeGo(long size)
        {
            return size / Math.Pow(1000, 3);
        }

        public static long GetDiskSpace(string drive)
        {
            DriveInfo di = new DriveInfo(drive);
            return di.AvailableFreeSpace;
        }
    }
}
