// This is DTOceanPlus Windows Installation.
// This program can be used to install, update or uninstall the DTOceanPlus suite of tools.
// Copyright (C) 2021 OPEN CASCADE
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
// License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.
﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace dtop_win_inst
{
    public partial class FrmInstall : Form
    {
        private List<DtopModule> m_toInstall = new List<DtopModule>();
        private Dictionary<string, string> m_modulesNames = new Dictionary<string, string>();

        public FrmInstall()
        {
            InitializeComponent();

            this.Text = String.Format("{0} - v{1}", Properties.Resources.AppTitle, Application.ProductVersion);
            tbxLicense.Text = Utils.ReadLicense(Properties.Settings.Default.LicenseFile);
            lblStep1.Text = Properties.Resources.ConfirmLicense;
            lblStep5End.Text = Properties.Resources.EndMessage;

#if DEBUG
            ckxDryRun.Visible = true;
            this.Text += " (DEBUG)";
#endif
        }
        private void FrmInstall_Load(object sender, EventArgs e)
        {
            StartLog(true);
            
            // get list of modules from settings
            foreach (var module in Properties.Settings.Default.Modules)
            {
                var elts = module.Split('=');
                if (!m_modulesNames.ContainsKey(elts[0]))
                    m_modulesNames.Add(elts[0], elts[1]);
            }

            LoadConfig();

            // Define DTOP_DATA if needed
            InstallConfig config = InstallConfig.Instance();
            Logger.WriteLine("DTOP_DATA value defined in 'dtop_inst.env' is equal to '{0}'", config.Data);
            if (Directory.Exists(config.Data))
            {
                Logger.WriteLine("The directory '{0}' exists.'", config.Data);
            }
            else
            {
                Directory.CreateDirectory(config.Data);
                Logger.WriteLine("The directory '{0}' has been created.'", config.Data);
            }
            Logger.WriteLine();

            CheckDockerConfig();
        }

        private void FrmInstall_FormClosed(object sender, FormClosedEventArgs e)
        {
            Logger.WriteLine();
            Logger.WriteLine("Installation ended {0}", DateTime.Now.ToString());
            Logger.WriteLine("======================");
            Logger.WriteLine();
        }

        private void LoadConfig()
        {
            InstallConfig.ParseEnvFile(Properties.Settings.Default.EnvFile, m_modulesNames);

            InstallConfig config = InstallConfig.Instance();
            config.InstallPath = Path.GetDirectoryName(Application.ExecutablePath);
            if (!config.DataDefined) config.Data = Path.Combine(config.InstallPath, "_volume", "data");

            // set config
            tbxData.Text = config.Data;
            tbxDomain.Text = config.Domain;
            lblInstallationType.Text = config.IsLocalDomain ? "Local Installation" : "Server Installation";
        }

        private void CheckDockerConfig()
        {
            if (DockerCommand.IsDockerRunning())
            {
                lblDockerStatus.Visible = false;
                lblDockerStatus.Dock = DockStyle.None;
                pnlDockerInfo.Visible = true;

                DockerInfo info = DockerCommand.GetInfo();
                tbxDockerProc.Text = info.NbProc.ToString();
                tbxDockerTotalMem.Text = info.StrTotalMemory;
                long driveAvailableSpace = Utils.GetDiskSpace(InstallConfig.Instance().InstallPath);
                tbxDiskSpace.Text = Utils.GetStrSize(driveAvailableSpace);

                if (info.NbProc < Properties.Settings.Default.DockerRecProc)
                    errDockerCheck.SetError(tbxDockerProc, String.Format("{0} CPUs recommended", Properties.Settings.Default.DockerRecProc));
                if (Utils.GetSizeGo(info.TotalMemory) < Properties.Settings.Default.DockerRecMem)
                    errDockerCheck.SetError(tbxDockerTotalMem, String.Format("{0} GB memory recommended", Properties.Settings.Default.DockerRecMem));
                if (Utils.GetSizeGo(driveAvailableSpace) < Properties.Settings.Default.DiskRecSpace)
                    errDockerCheck.SetError(tbxDiskSpace, String.Format("{0} GiB available disk space recommended", Properties.Settings.Default.DiskRecSpace));
            }
            else
            {
                pnlDockerInfo.Visible = false;
                lblDockerStatus.Dock = DockStyle.Fill;
                lblDockerStatus.Visible = true;
                btnUninstall.Enabled = false;
                btnRedeploy.Enabled = false;
                btnConfigContinue.Enabled = false;
            }
        }

        private void QuitWizard()
        {
            if (AskQuestion("Are you sure you want to quit, the DTOceanPlus installation?") == DialogResult.Yes)
            {
                this.Close();
            }
        }

        #region Config tab
        private void btnConfigContinue_Click(object sender, EventArgs e)
        {
            tclWizard.SelectedTab = tpgLicense;
            if (ckxDryRun.Checked) InstallConfig.Instance().DryRun = true;
        }

        private void btnConfigQuit_Click(object sender, EventArgs e)
        {
            QuitWizard();
        }

        private void btnUninstall_Click(object sender, EventArgs e)
        {
            ListInstalledModules(lvwUninstall);
            tclWizard.SelectedTab = tpgUninstall;
        }

        private void btnRedeploy_Click(object sender, EventArgs e)
        {
            ListInstalledModules(lvwRedeploy);
            tclWizard.SelectedTab = tpgRedeploy;
        }
        #endregion

        #region Tab License
        private void btnLicenseOK_Click(object sender, EventArgs e)
        {
            tclWizard.SelectedTab = tpgStep1;
        }
        #endregion

        #region Step 1
        private void btnStep1Accept_Click(object sender, EventArgs e)
        {
            tclWizard.SelectedTab = tpgStep2;
            ConfigureModules();
        }

        private void btnStep1Reject_Click(object sender, EventArgs e)
        {
            QuitWizard();
        }
        #endregion

        #region Step 2
        private void ConfigureModules()
        {
            // get installed modules
            var installed = DockerCommand.InstalledModules();

            lvwSelectModules.Items.Clear();
            foreach (var module in InstallConfig.Instance().Modules.OrderBy(m => m.Name))
            {
                string status = "Not installed";
                bool update = true;
                if (installed.ContainsKey(module.ShortName.ToLower()))
                {
                    if (module.Tag == installed[module.ShortName.ToLower()])
                    {
                        status = "Installed";
                        update = false;
                    }
                    else
                    {
                        status = "New version";
                    }
                }
                ListViewItem item = new ListViewItem(new string[] { module.ShortName, module.Name, module.Tag, status });
                if (update)
                {
                    item.ForeColor = Color.DarkRed;
                    item.Checked = true;
                }
                lvwSelectModules.Items.Add(item);
            }
            lvwSelectModules.AutoResizeColumns(ColumnHeaderAutoResizeStyle.ColumnContent);
        }
        private void btnSelectAll_Click(object sender, EventArgs e)
        {
            SelectListViewItems(lvwSelectModules, true);
        }

        private void btnSelectNone_Click(object sender, EventArgs e)
        {
            SelectListViewItems(lvwSelectModules, false);
        }

        private void btnStep2OK_Click(object sender, EventArgs e)
        {
            if (lvwSelectModules.CheckedItems.Count == 0)
            {
                ShowMessage("Please select at least one module.");
            }
            else
            {
                btnStep3Continue.Enabled = false;
                btnStep3Cancel.Enabled = false;
                lvwPulledImages.Visible = false;
                tclWizard.SelectedTab = tpgStep3;
                if (PullImages())
                {
                    ShowPulledImages();
                }
                else
                {
                    ShowMessage("Pulling was cancelled.");
                    tclWizard.SelectedTab = tpgStep2;
                }
            }
        }

        private void btnStep2Cancel_Click(object sender, EventArgs e)
        {
            QuitWizard();
        }
        #endregion

        #region Step 3

        private bool PullImages()
        {
            m_toInstall.Clear();
            foreach (ListViewItem item in lvwSelectModules.CheckedItems)
            {
                string code = item.SubItems[0].Text;
                DtopModule module = InstallConfig.Instance().Modules.FirstOrDefault(m => m.ShortName == code);
                if (module == null) continue;
                m_toInstall.Add(module);
            }

            bool cancelled = false;
            foreach (DtopModule module in m_toInstall)
            {
                FrmPullImage frm = new FrmPullImage(module);
                frm.PullImage();
                var res = frm.ShowDialog();
                cancelled = res == DialogResult.Cancel;
                if (cancelled) break;
            }
            return !cancelled;
        }
        private void ShowPulledImages()
        {
            var images = DockerCommand.ListImages();
            lvwPulledImages.Items.Clear();
            foreach (var image in images)
            {
                lvwPulledImages.Items.Add(new ListViewItem(image));
            }
            lvwPulledImages.AutoResizeColumns(ColumnHeaderAutoResizeStyle.ColumnContent);

            lvwPulledImages.Visible = true;
            btnStep3Continue.Enabled = true;
            btnStep3Cancel.Enabled = true;

            var failed = m_toInstall.Where(m => !m.Succeed);
            if (failed.Count() > 0)
            {
                StringBuilder msg = new StringBuilder();
                msg.AppendLine("The following modules are not accessible:");
                foreach (var m in failed) msg.AppendFormat("{0} - {1}", m.ShortName, m.Name).AppendLine();

                ShowWarning(msg.ToString());
            }
        }

        private void btnStep3Continue_Click(object sender, EventArgs e)
        {
            btnStep4Cancel.Enabled = false;
            btnStep4Continue.Enabled = false;
            lvwServices.Visible = false;
            tclWizard.SelectedTab = tpgStep4;

            FrmDeploy frm = new FrmDeploy(m_toInstall);
            frm.StartDeployment();
            frm.ShowDialog();

            try
            {
                this.Cursor = Cursors.WaitCursor;
                var services = DockerCommand.ListServices();
                lvwServices.Items.Clear();
                foreach (var service in services)
                {
                    lvwServices.Items.Add(new ListViewItem(service));
                }
                lvwServices.AutoResizeColumns(ColumnHeaderAutoResizeStyle.ColumnContent);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }

            lvwServices.Visible = true;
            btnStep4Cancel.Enabled = true;
            btnStep4Continue.Enabled = true;
        }

        private void btnStep3Cancel_Click(object sender, EventArgs e)
        {
            QuitWizard();
        }

        #endregion

        #region Step 4
        private void btnStep4Continue_Click(object sender, EventArgs e)
        {
            tclWizard.SelectedTab = tpgStep5;
            CleanUnsedContainers();
        }

        private void CleanUnsedContainers()
        {
            Logger.WriteLine();
            Logger.WriteLine("Cleaning / removing unused docker containers ... ");
            Logger.Write(DockerCommand.CleanupUnusedContainers());
        }

        private void btnStep4Cancel_Click(object sender, EventArgs e)
        {
            QuitWizard();
        }
        #endregion

        #region Step 5
        private void btnStep5Quit_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void btnOpenPortainer_Click(object sender, EventArgs e)
        {
            Utils.OpenUrl(Properties.Settings.Default.UrlPortainer);
        }
        private void btnLaunchDTO_Click(object sender, EventArgs e)
        {
            Utils.OpenUrl(Properties.Settings.Default.UrlMain);
        }

        #endregion

        #region Uninstall

        private void btnUninstallSelectAll_Click(object sender, EventArgs e)
        {
            SelectListViewItems(lvwUninstall, true);
        }

        private void btnUninstallClearAll_Click(object sender, EventArgs e)
        {
            SelectListViewItems(lvwUninstall, false);
        }

        private void btnUninstallContinue_Click(object sender, EventArgs e)
        {
            List<DtopModule> toRemove = new List<DtopModule>();
            foreach (ListViewItem item in lvwUninstall.CheckedItems)
            {
                string code = item.SubItems[0].Text;
                DtopModule module = InstallConfig.Instance().Modules.FirstOrDefault(m => m.ShortName == code);
                if (module == null) continue;
                toRemove.Add(module);
            }
            bool all = toRemove.Count == lvwUninstall.Items.Count;

            if (toRemove.Count == 0)
            {
                ShowMessage("Please select at least one module.");
            }
            else
            {
                var res = AskQuestion("Are you sure you want to uninstall the selected modules?");
                if (res == DialogResult.Yes)
                {
                    FrmUninstall frm = new FrmUninstall(toRemove, all);
                    Logger.WriteLine("Uninstall Start: {0}", DateTime.Now);
                    frm.StartUninstall();
                    frm.ShowDialog();

                    ShowMessage("Uninstall complete");
                    Logger.WriteLine("Uninstall complete: {0}", DateTime.Now);

                    // back to config tab
                    tclWizard.SelectedTab = tpgConfiguration;
                    InstallConfig.Reset();
                    LoadConfig();
                }
            }
        }
        private void btnUninstallCancel_Click(object sender, EventArgs e)
        {
            QuitWizard();
        }
        #endregion

        #region Redeploy
        private void btnRedeploySelectAll_Click(object sender, EventArgs e)
        {
            SelectListViewItems(lvwRedeploy, true);
        }

        private void btnRedeployClearAll_Click(object sender, EventArgs e)
        {
            SelectListViewItems(lvwRedeploy, false);
        }

        private void btnRedeployContinue_Click(object sender, EventArgs e)
        {
            List<DtopModule> toRedeploy = new List<DtopModule>();
            foreach (ListViewItem item in lvwRedeploy.CheckedItems)
            {
                string code = item.SubItems[0].Text;
                DtopModule module = InstallConfig.Instance().Modules.FirstOrDefault(m => m.ShortName == code);
                if (module == null) continue;
                module.Succeed = true; // needed to redeploy
                toRedeploy.Add(module);
            }

            if (toRedeploy.Count == 0)
            {
                ShowMessage("Please select at least one module.");
            }
            else
            {
                FrmDeploy frm = new FrmDeploy(toRedeploy);
                Logger.WriteLine("Redeploy Start: {0}", DateTime.Now);
                frm.StartDeployment();
                frm.ShowDialog();

                ShowMessage("Redeploy complete");
                Logger.WriteLine("Redeploy complete: {0}", DateTime.Now);
                // back to config tab
                tclWizard.SelectedTab = tpgConfiguration;
            }
        }

        private void btnRedeployCancel_Click(object sender, EventArgs e)
        {
            QuitWizard();
        }
        #endregion

        /// <summary>
        /// Checks/unchecks all items of a ListView.
        /// </summary>
        /// <param name="lvw">The ListView.</param>
        /// <param name="select">True to check, False to uncheck.</param>
        private static void SelectListViewItems(ListView lvw, bool select)
        {
            foreach (ListViewItem item in lvw.Items)
            {
                item.Checked = select;
            }
        }

        private void ListInstalledModules(ListView lvw)
        {
            lvw.Items.Clear();
            var installed = DockerCommand.InstalledModules();
            foreach (var im in installed.OrderBy(kv => kv.Key))
            {
                string id = im.Key;
                var module = InstallConfig.Instance().Modules.FirstOrDefault(m => m.ShortName == id.ToUpper());
                if (module == null) continue;

                ListViewItem item = new ListViewItem(new string[] { module.ShortName, module.Name });
                lvw.Items.Add(item);
            }
            lvw.AutoResizeColumns(ColumnHeaderAutoResizeStyle.ColumnContent);
        }

        private void btnCheckUpdates_Click(object sender, EventArgs e)
        {
            // get new version of env files
            var nv = UpdateChecker.CheckUpdates(InstallConfig.Instance().InstallPath);
            if (nv == null)
            {
                ShowMessage("Unable to check for updates");
                return;
            }

            // compare version with current
            List<DtopModule> toUpdate = new List<DtopModule>();
            foreach (var module in InstallConfig.Instance().Modules)
            {
                if (String.IsNullOrEmpty(module.Tag)) continue;
                if (nv.ContainsKey(module.ShortName) && nv[module.ShortName] != module.Tag)
                {
                    toUpdate.Add(module);
                }
            }

            if (toUpdate.Count == 0)
            {
                ShowMessage("No updates available");
                return;
            }

            // updates available
            StringBuilder bld = new StringBuilder();
            bld.AppendLine("Available updates for:");
            foreach (var module in toUpdate)
            {
                bld.AppendLine(String.Format("  {0}: {1} => {2}", module.ShortName.ToUpper(),
                    module.Tag, nv[module.ShortName]));
            }

            if (Directory.Exists(InstallConfig.Instance().InstallPath))
            {
                string currentEnv = Path.Combine(InstallConfig.Instance().InstallPath, "dtop_inst.env");
                string newEnv = Path.Combine(InstallConfig.Instance().InstallPath, "dtop_inst.env.update");

                if (File.Exists(newEnv))
                {
                    bld.AppendLine().AppendLine("Do you want to update your env file?");
                    this.TopMost = false;
                    if (AskQuestion(bld.ToString()) == DialogResult.Yes)
                    {
                        File.Copy(newEnv, currentEnv, true);
                        InstallConfig.Reset();
                        LoadConfig();
                    }
                }
            }
        }

        private void btnClearLog_Click(object sender, EventArgs e)
        {
            Logger.Stop();
            StartLog(false);
        }
        private void StartLog(bool append)
        {
            Logger.Start(Properties.Settings.Default.LogFile, append);
            Logger.WriteLine("======================");
            Logger.WriteLine("Installation started {0}", DateTime.Now.ToString());
        }


        #region MessageBox utils
        private static DialogResult ShowMessage(string message)
        {
            return MessageBox.Show(message, Properties.Resources.AppTitle);
        }
        private static DialogResult ShowWarning(string message)
        {
            return MessageBox.Show(message, Properties.Resources.AppTitle, MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }
        private static DialogResult AskQuestion(string message)
        {
            return MessageBox.Show(message, Properties.Resources.AppTitle, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
        }
        #endregion
    }
}
