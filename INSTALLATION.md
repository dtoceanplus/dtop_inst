# DTOP Modules' Installation Procedure

DTOP Installation documentation includes :

- [DTOP (DTOP) Project Installation Summary > > . . . ](./README.md)
- [DTOP Project Installation Prerequisite > > . . . ](./PREREQUISITES.md)
- [DTOP Modules' Installation Procedure > > . . . ](./INSTALLATION.md)
- [DTOP Modules' Re-Installation and Un-Installation Procedure > > . . . ](./RE_UN_INSTALLATION.md)

This document - `DTOP Modules' Installation Procedure`.

After performing the mandatory prerequisite actions and settings described in **[DTOP Project Installation Prerequisite](./PREREQUISITES.md)**, DTOP Project Installation Procedure can be performed as described here.

This document presents the necessary details and illustrations of 5 Steps of Installation Process.

- Step 1/5 - DTOceanPlus (DTOP) Project License Acceptance
- Step 2/5 - Selection of DTOP modules to be installed and deployed
- Step 3/5 - DTOP Modules' Docker Images Downloading / Pulling
- Step 4/5 - DTOP Modules' Docker Stacks Deployment
- Step 5/5 - Checking deployed infrastructure and DTOP Running
- DTOP Installation Process Console Output.

For post control of the results of Installation Process Steps please see Console Output - the example is below.

## Running the DTOP installation

### Enter into DTOP installation directory

For Windows 10 run `dtop_inst.bat` to open **bash terminal** after creation of `Cygwin` environment by `cygwin_inst.bat`.

For Debian and iMac in the **bash terminal** change directory to the DTOP installation one, for example
```
cd ./dtop_inst_1.0.0
```

**NOTE**:
- installation directory name should not contain spaces and special characters.
- Installation process of all modules from the scratch can take some time - please be patient.

### Check / Set the scripts' permissions

Check and make sure that the scripts are executable :

```
chmod u+x ./<script name>.sh
# or
chmod u+x *.sh
```

### Run DTOP installation script :
```
./dtop_inst.sh
```

The script provides installation in the next five steps :
- reading and acceptance of DTOP license
- selection of the module(s)
- downloading (pulling) docker production images of the module(s)
- deployment and running docker containers of the module(s)
- checking (re)deployed infrastructure

**NOTE**: in Installation Procedure UI please use a Keyboard only including TAB, ENTER, SPACE, BACKSPACE, LEFT/RIGHT, UP/DOWN and PGUP/PGDN keys.

## Step 1/5 - DTOceanPlus (DTOP) Project License Acceptance

On this Step Installation an User should learn and accept DTOceanPlus (DTOP) Project License.

![](./images/1_1.png)
![](./images/1_2.png)

## Step 2/5 - Selection of DTOP modules to be installed and deployed

On this Step an User selects DTOP modules to be installed.

By default option `all` is selected that proposes to install all DTOP modules.

You can select any set of DTOP modules to be installed and deployed.

![](./images/2_1.png)

Additionally the Step creates the file \"latest_selected_modules.txt\" with info about the selected DTOP Modules.

The file "latest_selected_modules.txt" created during the previous installation Step 1/5.

The file "latest_selected_modules.txt" can be edited manually.

## Step 3/5 - DTOP Modules' Docker Images Downloading / Pulling

On this Step the Procedure pulls DTOP Modules' Docker Production Images for the selected modules from the Module Gitlab registries.

The Progress bar below displays the installation progress for each DTOP module selected - pulling DTOP Modules' Docker Production Images.

![](./images/3_1.png)

The last page of this Step presents the list of Modules' Docker Images pulled from all GilLab DTOP registries.

Additionally the Step creates the file \"latest_modules_images.txt\" with info about the DTOP Modules' Docker Images pulled from the Module Gitlab registries.

![](./images/3_2.png)

## Step 4/5 - DTOP Modules' Docker Stacks Deployment

This Step of the Procedure :
- checks / creates DTOP Services infrastructure - Docker network and SWARM mode
- deploys Basic Docker Service Stacks
- deploys the correspondent DTOP Modules' Docker Stacks
to a server / a local workstation

Some additional related info :

The Docker Services files location is `./stacks/`

Basic Docker Service Stacks files are `tra_stack.yml` for Traefik and `portainer_stack.yml` for Portainer.

DTOP Modules' Docker Stacks files naming - `<module nickname>_stack.yml`, for example `ed_stack.yml`

Progress bar of deploying Modules' Docker Stacks for every previously selected and pulled module is sequentially displayed.

![](./images/4_1.png)

The last page of this Step presents the list of Deployed Docker Modules' Stacks.

![](./images/4_2.png)

Additionally the Step creates the file \"latest_module_stacks.txt\" with info about the deployed DTOP Modules' Docker Stacks.

## Step 5/5 - Checking deployed infrastructure and DTOP Running

On this Step an User can check the deployed DTOP Stacks, Services and Containers docker infrastructure using Portainer web service and GUI.

Portainer Community Edition <https://www.portainer.io/> service is deployed and run as a part of Installation.

You can open and use the Portainer service GUI in the browser using the URLs :
- <http://portainer.dtop.localhost> - for DTOP installation on the local workstation
- http://portainer.\<dtop.company domain name\> - for DTOP installation on a server in intranet environment

![](./images/5_1.png)

### Portainer Web UI

Portainer Web UI can be used to control and manage DTOP deployed docker infrastructure, modules services environment, monitor application performance, detect possible problems, simplify processes and streamline operations.

During initial connect Portainer proposes to set username and password for Portainer administrator.

Then Select the button `Docker - Manage the local Docker Environment`.

**NOTE**:  in the case of unexpected Portainer behavior caused by its internal issues you need to logout and login to Portainer.

![](./images/port_dashboard.png)

![](./images/port_stacks.png)

![](./images/port_containers.png)

Portainer `admin` account provides full functionality for all DTOP docker stacks, images, services, containers actions.

You can use the `admin` account mainly for viewing and inspection.

For re-deployment and un-deployment DTOP Module Stacks, the installation `dtop_inst.sh` or redeployment `dtop_redeploy.sh` scripts
can be used as well as Portainer.

### DTOP Modules' Running in browser

Ok, if there are no any unexpected issues during the previous steps then DTOP Modules docker infrastructure
is properly installed and deployed.

![](./images/inst_step_7.png)

Now you can run the necessary DTOP modules in the browser using the correspondent URLs :

- http://\<module nickname\>.dtop.localhost - for DTOP installation on the local workstation
- http://\<module nickname\>.\<dtop.company domain name\> - for DTOP installation on a server in intranet environment

The list of NICKNAMES of DTOP modules ( \<module nickname\> ) :
```
"sc" "mc" "ec" "et" "ed" "sk" "lmo" "spey" "rams" "slc" "esa" "si" "sg" "cm" "mm"
```

As example, let's open Main Module (mm) using admin credentials - `name: admin@dtop.com` and `pass: adminadmin` :
```
- http://mm.dtop.localhost - for DTOP installation on the local workstation
- http://mm.<dtop.company domain name> - for DTOP installation on a server in intranet environment
```

![](./images/mm_login_page.png)

When DTOP Project Installation is successfully finished, please follow the provided Modules' Documentation for their further designed usage !

## (re)Deployment of DTOP module(s)

### Run DTOP (re)Deployment script :
```
./dtop_redeploy.sh
```

The script provides (re)deployment of the necessary DTOP module(s) in three steps :
- selection of the module(s)
- (re)deployment
- checking (re)deployed infrastructure

## DTOP Installation Process Console Output.

To control the results of Installation Process Steps console output can be used.

The example of Installation console output is below.

```
$ ./dtop_inst.sh

INST_ROOT == /cygdrive/d/dev/dtop/dtop_inst_windows_1.0.0

The detault value DTOP_DATA is set to '/cygdrive/d/dev/dtop/dtop_inst_1.0.0_windows/_volume/data'
The directory '/cygdrive/d/dev/dtop/dtop_inst_1.0.0_windows/_volume/data' for sharing and keeping modules' databases and data has been created.

---------------------------------------------
 DTOceanPlus (DTOP) installation log start :
 DTOP installation is running on Windows

 OK - it looks like the internet is available now
 Installation directory - /cygdrive/d/dev/dtop/dtop_inst_1.0.0_windows

 Step 1/5 - DTOceanPlus (DTOP) Project License Acceptance

 DTOP License is Accepted

 Step 2/5 - Selection of DTOP modules
 The selected modules nicknames: "all"
 ALL is selected !
 The modules nicknames taking into account that "ALL" is selected : "sc" "mc" "ec" "et" "ed" "sk" "lmo" "spey" "rams" "slc" "esa" "si" "sg" "cm" "mm"
 MODULES_NICKNAMES = "sc" "mc" "ec" "et" "ed" "sk" "lmo" "spey" "rams" "slc" "esa" "si" "sg" "cm" "mm"
 You chose Ok

 Step 3/5 - DTOP Modules' Docker Images Downloading

 MODULE = SC
 Module BE GitLab production Image = registry.gitlab.com/fem-dtocean/dtop-site/sc_backend:v1.4
 Module FE GitLab production Image = registry.gitlab.com/fem-dtocean/dtop-site/sc_frontend:v1.4
 The module sc is accessible

 MODULE = MC
 Module BE GitLab production Image = registry.gitlab.com/aau-c/dtoceanplus/dtop_machine/mc_backend:v1.0.1
 Module FE GitLab production Image = registry.gitlab.com/aau-c/dtoceanplus/dtop_machine/mc_frontend:v1.0.1
 The module mc is accessible

...

 MODULE = MM
 Module BE GitLab production Image = registry.gitlab.com/opencascade/dtocean/dtop_mm/mm_backend:1.0.0
 Module FE GitLab production Image = registry.gitlab.com/opencascade/dtocean/dtop_mm/mm_frontend:1.0.0
 The module mm is accessible


CREATED AT                      REPOSITORY                                                                               TAG       SIZE

2021-07-28 19:51:52 +0300 MSK   registry.gitlab.com/dtoceanplus/dtop_mm/mm_frontend                              1.0.0     33.9MB
2021-07-28 19:23:06 +0300 MSK   registry.gitlab.com/dtoceanplus/dtop_sc/sc_backend                                     v1.4      1GB
2021-07-28 19:09:54 +0300 MSK   registry.gitlab.com/dtoceanplus/dtop_sk/sk_frontend                             v1.5      46.8MB
2021-07-28 17:27:13 +0300 MSK   registry.gitlab.com/dtoceanplus/dtop_sk/sk_backend                              v1.5      702MB
2021-07-28 13:56:23 +0300 MSK   registry.gitlab.com/opencascade/dtocean/sandbox/dtop_catalog_from_to_excel/cm_frontend   1.0.0     28MB
2021-07-28 13:54:45 +0300 MSK   registry.gitlab.com/opencascade/dtocean/sandbox/dtop_catalog_from_to_excel/cm_backend    1.0.0     690MB
2021-07-27 14:03:32 +0300 MSK   registry.gitlab.com/uedin/dtoceanplus/dtop-energydeliv/ed_frontend                       0.0.7     29.2MB
2021-07-27 14:00:52 +0300 MSK   registry.gitlab.com/uedin/dtoceanplus/dtop-energydeliv/ed_backend                        0.0.7     1.6GB
2021-07-27 13:00:31 +0300 MSK   registry.gitlab.com/fem-dtocean/dtop-site/sc_frontend                                    v1.4      24.9MB
2021-07-26 11:57:45 +0300 MSK   registry.gitlab.com/opencascade/dtocean/dtop_mm/mm_backend                               1.0.0     410MB
2021-07-26 02:00:31 +0300 MSK   registry.gitlab.com/aau-c/dtoceanplus/dtop_machine/mc_backend                            v1.0.1    583MB
2021-07-26 00:30:49 +0300 MSK   registry.gitlab.com/aau-c/dtoceanplus/dtop_machine/mc_frontend                           v1.0.1    29.5MB
2021-07-23 11:54:29 +0300 MSK   registry.gitlab.com/aau-c/dtoceanplus/energycapture/ec_frontend                          v1.1.1    29.8MB
2021-07-23 11:53:12 +0300 MSK   registry.gitlab.com/aau-c/dtoceanplus/energycapture/ec_backend                           v1.1.1    514MB
2021-07-21 22:26:11 +0300 MSK   registry.gitlab.com/aau-c/dtoceanplus/rams/rams_backend                                  0.9.0     512MB
2021-07-17 07:17:21 +0300 MSK   registry.gitlab.com/energysystemscatapult/dtoceanplus/dtop_structinn/si_frontend         v1.0.1    28.6MB
2021-07-17 07:08:23 +0300 MSK   registry.gitlab.com/energysystemscatapult/dtoceanplus/dtop_structinn/si_backend          v1.0.1    177MB
2021-07-12 13:41:00 +0300 MSK   registry.gitlab.com/tecnalia_ore/dtoceanplus/dtop_energytransf/et_backend                v1.5      632MB
2021-07-12 12:51:35 +0300 MSK   registry.gitlab.com/tecnalia_ore/dtoceanplus/dtop_energytransf/et_frontend               v1.5      24.9MB
2021-07-09 14:22:41 +0300 MSK   portainer/portainer-ce                                                                   <none>    210MB
2021-07-09 12:38:27 +0300 MSK   registry.gitlab.com/wave-energy-scotland/dtoceanplus/dtop-stagegate/sg_frontend          1.0.0     34.3MB
2021-07-09 12:36:13 +0300 MSK   registry.gitlab.com/wave-energy-scotland/dtoceanplus/dtop-stagegate/sg_backend           1.0.0     322MB
2021-07-07 11:12:06 +0300 MSK   registry.gitlab.com/fem-dtocean/dtop-esa/esa_frontend                                    v1.2      48.2MB
2021-07-07 11:11:01 +0300 MSK   registry.gitlab.com/fem-dtocean/dtop-esa/esa_backend                                     v1.2      557MB
2021-07-06 09:45:39 +0300 MSK   registry.gitlab.com/wavec/dtoceanplus/dtop_lmo/lmo_frontend                              v1.3      25.2MB
2021-07-06 09:45:38 +0300 MSK   registry.gitlab.com/wavec/dtoceanplus/dtop_lmo/lmo_backend                               v1.3      735MB
2021-07-04 11:32:56 +0300 MSK   registry.gitlab.com/aau-c/dtoceanplus/rams/rams_frontend                                 0.9.0     28.2MB
2021-07-02 17:08:12 +0300 MSK   registry.gitlab.com/fem-dtocean/dtop-stationkeep/sk_frontend                             v1.4      46.8MB
2021-07-02 14:44:57 +0300 MSK   registry.gitlab.com/fem-dtocean/dtop-stationkeep/sk_backend                              v1.4      679MB
2021-07-02 14:27:39 +0300 MSK   registry.gitlab.com/opencascade/dtocean/sandbox/dtop_catalog_from_to_excel/cm_frontend   0.9.1     28MB
2021-07-02 14:25:17 +0300 MSK   registry.gitlab.com/opencascade/dtocean/sandbox/dtop_catalog_from_to_excel/cm_backend    0.9.1     680MB
2021-07-02 14:24:07 +0300 MSK   registry.gitlab.com/opencascade/dtocean/dtop_mm/mm_frontend                              0.9.1     32.7MB
2021-07-02 14:21:45 +0300 MSK   registry.gitlab.com/opencascade/dtocean/dtop_mm/mm_backend                               0.9.1     410MB
2021-07-02 14:18:11 +0300 MSK   registry.gitlab.com/aau-c/dtoceanplus/energycapture/ec_frontend                          v1.0.3    29.8MB
2021-07-02 14:16:39 +0300 MSK   registry.gitlab.com/aau-c/dtoceanplus/energycapture/ec_backend                           v1.0.3    514MB
2021-07-02 13:50:24 +0300 MSK   registry.gitlab.com/fem-dtocean/dtop-site/sc_backend                                     v1.3      1.59GB
2021-07-02 13:49:27 +0300 MSK   registry.gitlab.com/fem-dtocean/dtop-site/sc_frontend                                    v1.3      24.9MB
2021-07-02 10:43:44 +0300 MSK   registry.gitlab.com/tecnalia_ore/dtoceanplus/dtop_energytransf/et_frontend               v1.4      24.9MB
2021-07-02 10:43:40 +0300 MSK   registry.gitlab.com/tecnalia_ore/dtoceanplus/dtop_energytransf/et_backend                v1.4      632MB
2021-07-01 17:30:23 +0300 MSK   registry.gitlab.com/tecnalia_ore/dtoceanplus/dtop_spey/spey_frontend                     v1.6      30.9MB
2021-07-01 17:28:53 +0300 MSK   registry.gitlab.com/tecnalia_ore/dtoceanplus/dtop_spey/spey_backend                      v1.6      394MB
2021-06-30 22:53:40 +0300 MSK   registry.gitlab.com/uedin/dtoceanplus/dtop-energydeliv/ed_frontend                       0.0.5     29.2MB
2021-06-30 22:51:45 +0300 MSK   registry.gitlab.com/uedin/dtoceanplus/dtop-energydeliv/ed_backend                        0.0.5     1.59GB
2021-06-30 11:02:15 +0300 MSK   registry.gitlab.com/wave-energy-scotland/dtoceanplus/dtop-stagegate/sg_backend           0.11.0    322MB
2021-06-29 16:04:08 +0300 MSK   registry.gitlab.com/wave-energy-scotland/dtoceanplus/dtop-stagegate/sg_frontend          0.11.0    34.2MB
2021-06-28 12:06:49 +0300 MSK   registry.gitlab.com/aau-c/dtoceanplus/dtop_machine/mc_frontend                           v1.0.0    29.5MB
2021-06-28 12:05:46 +0300 MSK   registry.gitlab.com/aau-c/dtoceanplus/dtop_machine/mc_backend                            v1.0.0    583MB
2021-06-24 15:16:58 +0300 MSK   portainer/portainer-ce                                                                   <none>    210MB
2021-06-23 10:11:48 +0300 MSK   mysql                                                                                    <none>    556MB
2021-06-18 21:32:19 +0300 MSK   registry.gitlab.com/wavec/dtoceanplus/dtop_slc/slc_frontend                              0.0.3     27.4MB
2021-06-18 21:31:03 +0300 MSK   registry.gitlab.com/wavec/dtoceanplus/dtop_slc/slc_backend                               0.0.3     306MB
2021-06-17 19:38:42 +0300 MSK   registry.gitlab.com/energysystemscatapult/dtoceanplus/dtop_structinn/si_frontend         v0.3.1    27.7MB
2021-06-15 02:50:39 +0300 MSK   registry.gitlab.com/energysystemscatapult/dtoceanplus/dtop_structinn/si_backend          v0.3.1    177MB
2020-09-09 03:18:16 +0300 MSK   traefik                                                                                  <none>    79.4MB
2020-01-18 07:17:46 +0300 MSK   redis                                                                                    <none>    29.8MB

 You chose Continue

 Step 4/5 - DTOP Modules' Docker Stacks Deployment

 The node is a in standalone mode
Swarm initialized: current node (1tgmgrj5988arq1hattaei2wh) is now a manager.

To add a worker to this swarm, run the following command:

    docker swarm join --token SWMTKN-1-3cjqu1uhay3lwtk6h44zxng5rq7pzsm6907ah0wc1eqy2urumg-evcxii0dmawcvfksmmxryx7ca 192.168.65.3:2377

To add a manager to this swarm, run 'docker swarm join-token manager' and follow the instructions.


 Creating Docker network - "traefik-public"
lcr8w80lp11h9jn3um6s4o2nv

Creating service tra_tra

Creating service portainer_portainer

 Deploying MODULE = SC
 The DTOP SC module data files archive 'dtop_inst_sc_databases_v1.4.tar.gz' is found in /cygdrive/d/dev/dtop/dtop_inst_1.0.0_windows
 Installing the archive into /cygdrive/d/dev/dtop/dtop_inst_1.0.0_windows/_volume/data/sc/databases ...

 Deploying MODULE = MC

...

 Deploying MODULE = CM

 Docker volume "cm_pg_data" - is creating for database data storage ...
cm_pg_data

 Deploying MODULE = MM

 Docker volume "mm_mysql_data" - is creating for database data storage ...
mm_mysql_data

IMAGE                                                                                          NAME                  REPLICAS

registry.gitlab.com/opencascade/dtocean/sandbox/dtop_catalog_from_to_excel/cm_backend:1.0.0    cm_cm_backend         1/1
registry.gitlab.com/opencascade/dtocean/sandbox/dtop_catalog_from_to_excel/cm_frontend:1.0.0   cm_cm_frontend        1/1
registry.gitlab.com/aau-c/dtoceanplus/energycapture/ec_backend:v1.1.1                          ec_ec_backend         1/1
registry.gitlab.com/aau-c/dtoceanplus/energycapture/ec_frontend:v1.1.1                         ec_ec_frontend        1/1
redis:5.0.7-alpine                                                                             ec_ec_redis           1/1
registry.gitlab.com/aau-c/dtoceanplus/energycapture/ec_backend:v1.1.1                          ec_ec_worker          1/1
registry.gitlab.com/uedin/dtoceanplus/dtop-energydeliv/ed_backend:0.0.7                        ed_ed_backend         1/1
registry.gitlab.com/uedin/dtoceanplus/dtop-energydeliv/ed_frontend:0.0.7                       ed_ed_frontend        1/1
registry.gitlab.com/fem-dtocean/dtop-esa/esa_backend:v1.2                                      esa_esa_backend       1/1
registry.gitlab.com/fem-dtocean/dtop-esa/esa_frontend:v1.2                                     esa_esa_frontend      1/1
registry.gitlab.com/tecnalia_ore/dtoceanplus/dtop_energytransf/et_backend:v1.5                 et_et_backend         1/1
registry.gitlab.com/tecnalia_ore/dtoceanplus/dtop_energytransf/et_frontend:v1.5                et_et_frontend        1/1
registry.gitlab.com/wavec/dtoceanplus/dtop_lmo/lmo_backend:v1.3                                lmo_lmo_backend       1/1
registry.gitlab.com/wavec/dtoceanplus/dtop_lmo/lmo_frontend:v1.3                               lmo_lmo_frontend      1/1
registry.gitlab.com/aau-c/dtoceanplus/dtop_machine/mc_backend:v1.0.1                           mc_mc_backend         1/1
registry.gitlab.com/aau-c/dtoceanplus/dtop_machine/mc_frontend:v1.0.1                          mc_mc_frontend        1/1
redis:5.0.7-alpine                                                                             mc_mc_redis           1/1
registry.gitlab.com/aau-c/dtoceanplus/dtop_machine/mc_backend:v1.0.1                           mc_mc_worker          1/1
registry.gitlab.com/opencascade/dtocean/dtop_mm/mm_backend:1.0.0                               mm_mm_backend         1/1
mysql:8.0                                                                                      mm_mm_database        1/1
registry.gitlab.com/opencascade/dtocean/dtop_mm/mm_digitalrep:1.0.0                            mm_mm_digitalrep      1/1
registry.gitlab.com/opencascade/dtocean/dtop_mm/mm_frontend:1.0.0                              mm_mm_frontend        1/1
portainer/portainer-ce:latest                                                                  portainer_portainer   1/1
registry.gitlab.com/aau-c/dtoceanplus/rams/rams_backend:0.9.0                                  rams_rams_backend     1/1
registry.gitlab.com/aau-c/dtoceanplus/rams/rams_frontend:0.9.0                                 rams_rams_frontend    1/1
redis:5.0.7-alpine                                                                             rams_redis            1/1
registry.gitlab.com/aau-c/dtoceanplus/rams/rams_backend:0.9.0                                  rams_worker           1/1
registry.gitlab.com/fem-dtocean/dtop-site/sc_backend:v1.4                                      sc_sc_backend         1/1
registry.gitlab.com/fem-dtocean/dtop-site/sc_frontend:v1.4                                     sc_sc_frontend        1/1
registry.gitlab.com/wave-energy-scotland/dtoceanplus/dtop-stagegate/sg_backend:1.0.0           sg_sg_backend         1/1
registry.gitlab.com/wave-energy-scotland/dtoceanplus/dtop-stagegate/sg_frontend:1.0.0          sg_sg_frontend        1/1
registry.gitlab.com/energysystemscatapult/dtoceanplus/dtop_structinn/si_backend:v1.0.1         si_si_backend         1/1
registry.gitlab.com/energysystemscatapult/dtoceanplus/dtop_structinn/si_frontend:v1.0.1        si_si_frontend        1/1
registry.gitlab.com/fem-dtocean/dtop-stationkeep/sk_backend:v1.5                               sk_sk_backend         1/1
registry.gitlab.com/fem-dtocean/dtop-stationkeep/sk_frontend:v1.5                              sk_sk_frontend        1/1
registry.gitlab.com/wavec/dtoceanplus/dtop_slc/slc_backend:0.0.3                               slc_slc_backend       1/1
registry.gitlab.com/wavec/dtoceanplus/dtop_slc/slc_frontend:0.0.3                              slc_slc_frontend      1/1
registry.gitlab.com/tecnalia_ore/dtoceanplus/dtop_spey/spey_backend:v1.6                       spey_spey_backend     1/1
registry.gitlab.com/tecnalia_ore/dtoceanplus/dtop_spey/spey_frontend:v1.6                      spey_spey_frontend    1/1
traefik:v2.2                                                                                   tra_tra               1/1

 You chose Continue

 Step 5/5 - Checking deployed infrastructure and DTOP Running
 You chose Finish

Cleaning / removing unused docker containers ...
ae6d6e583900
767481409ab3
...
ee16157f255d
---------------------------------------------
 DTOceanPlus (DTOP) installation log finish.
---------------------------------------------
 Installation process took xxx seconds

```
