# DTOceanPlus (DTOP) Project Installation Summary

## Overview

DTOP Project is composed of 15 functional interacting Modules designed and provided by correspondent Proxy, Docker Stacks, Services and production Images.

DTOP Project uses Docker Engine - an open source containerization technology for building and containerizing the DTOP modules - [Docker Engine overview](https://docs.docker.com/engine/).

Docker production images are available in `Container Registry` of correspondent private GitLab repositories of DTOP modules.

For DTOP Modules Services (microservices) orchestration Docker in SWARM mode is used.

[Traefik proxy Edge Router](https://traefik.io) interacts with Docker registry/orchestrator API and generates the properly resolved routes to DTOP Module Services (microservices).

These routes allow access to DTOP Modules' Frontends and Backends as interacting Web applications for final DTOP project users.

------------------------

This document - `DTOP (DTOP) Project Installation Summary` presents overview of DTOP Project Installation components.

DTOP Installation documentation includes :

- [DTOP (DTOP) Project Installation Summary > > . . . ](./README.md)
- [DTOP Project Installation Prerequisite > > . . . ](./PREREQUISITES.md)
- [DTOP Modules' Installation Procedure > > . . . ](./INSTALLATION.md)
- [DTOP Modules' Re-Installation and Un-Installation Procedure > > . . . ](./RE_UN_INSTALLATION.md)

You can learn all documents that present the necessary details and illustrations.

## GitLab Continious Integration and building Project Installation archives

GitLab Continious Integration (CI) process is initiated by `git push` commit to `master` branch of this repo.

Succesfully performed CI jobs build the installation archives for Windows and Linux / macOS.
- the archives are named as `dtop_inst_windows_<VERSION>.zip` and `dtop_inst_linux_<VERSION>.tar.gz`
- for example `dtop_inst_windows_1.0.0.zip` or `dtop_inst_linux_1.0.0.tar.gz`
- the built archives are published and available for donload in Project Installation Package Registry
- `<VERSION>` value is automatically and semantically incremented

## Project Installation and Re/Un-Installation implementation

### Bash Scripts using whiptail UI

To provide simple and unified DTOP Project Installation on different HWs and Operative Systems,
Project Installation and Re/Un-Installation is implemented basing on Bash scripts with using "whiptail" program for user-friendly UI.

The Environment Checking, Installation and Un-Installation Procedures are provided with the next scripts :

- `dtop_inst.sh`
```
#  The script provides Installation of DTOceanPlus (DTOP)
#  It uses the variables and functions of "./dtop_basic.sh".
#  The environment variables are read from "./dtop_inst.env".
```
- `dtop_redeploy.sh`
```
#  The script provides (re)deployment of the necessary DTOP modules
#  It uses the variables and functions of "./dtop_basic.sh".
#  The environment variables are read from "./dtop_inst.env".
```
- `dtop_uninst.sh`
```
#  The script provides Un-Installation of DTOceanPlus (DTOP)
#  It uses the file "latest_selected_modules.txt" created during the previous installation.
```
- `dtop_basic.sh`
```
#  The script defines the basic variables and functions
#  used in main DTOP Modules' Installation - "./dtop_inst.sh" and "./dtop_redeploy.sh".
#  The environment variables are read from "./dtop_inst.env".
```
- `cleanup_unused_containers.sh`
```
#  The auxiliary script removes unused docker containers
```

You can have a look these scripts for implementation details.

### Additional tools

- `dtop_mon.exe` (Windows) is a small tool to monitor the status of the DTOceanPlus services, give information
and statistics, check for updates, ...

## Main DTOP Installation steps

Only the installation archive `dtop_inst_windows_<VERSION>.zip` or `dtop_inst_linux_<VERSION>.tar.gz` is required, not the entire repository. For example `dtop_inst_windows_1.0.0.zip` or `dtop_inst_linux_1.0.0.tar.gz`.

- Download and unarchive the required DTOP installation archive on a local workstation or on a company server from [Installation Package Registry](https://gitlab.com/dtoceanplus/dtop_inst/-/packages)
- Create the directory for DTOP installation and extract the content of the archive to this directory.
- For Windows create the `cygwin` environment by running  `cygwin_inst.bat` and open its `bash` terminal by `dtop_inst.bat`.
- Execute the installation script `./dtop_inst.sh` from a bash terminal.
- When it is necessary to (re)deploy module(s) execute the script `./dtop_redeploy.sh` from a bash terminal.

Open an installed DTOP module using URL `<module nickname>.dtop.localhost` or `<module nickname>.dtop.<company server domain name>` in a browser.

To uninstall run the script `./dtop_uninst.sh`.

You can the find the necessary details and illustrations of these main steps in other documentation files mentioned above.

**NOTE**:

DTOP SC module needs and uses the set of own basic data files.
These SC data files are provided in the quite large archive `dtop_inst_sc_databases_v1.x.tar.gz`.
The archive should be downloaded separately from [DTOceanPlus Site Characterisation (SC) module databases repo](https://gitlab.com/dtoceanplus/dtop_sc_databases) and copied into DTOP installation directory to be used by the installation procedure.

The exact version of the used SC data files archive is defined in `dtop_inst.env` with the variable `SC_databases_archive_version`, for example :
```
SC_databases_archive_version=dtop_inst_sc_databases_v1.5.tar.gz
```
