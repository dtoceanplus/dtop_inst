#!/bin/bash
#
# Script Name: dtop_basic.sh
# Version: 0.1
# TermsOfService: 'https://www.dtoceanplus.eu/'
#
# Description:
#  The script defines the basic variables and functions
#  used in main DTOP Modules' Installation - "./dtop_inst.sh" and "./dtop_redeploy.sh".
#  The environment variables are read from "./dtop_inst.env".
#
# Run Information:
#  it is called (source) from dtop_inst.sh
#  check that the file has executable permissions.
#  if no, set it
#  chmod +x ./dtop_basic.sh
#

# ------------------------------------------
# Set variables for installation directory.
# ------------------------------------------

__dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

export INST_ROOT="${__dir}"

# ----------------------------------------------------------
# Set environment variables from ${INST_ROOT}/dtop_inst.env
# ----------------------------------------------------------
if [ ! -f "${INST_ROOT}/dtop_inst.env" ]; then
    echo ""
    echo "${INST_ROOT}/dtop_inst.env" is not found
    exit 1
fi

export $(grep -v "^#" "${INST_ROOT}/dtop_inst.env" | xargs)

echo ""
#if [ -z ${DTOP_DATA+x} ]; then
if [ -z "${DTOP_DATA}" ]; then
    DTOP_DATA="${INST_ROOT}/_volume/data"
    echo "The detault value DTOP_DATA is set to '${INST_ROOT}/_volume/data'"
else
    echo "DTOP_DATA value defined in 'dtop_inst.env' is equal to '${DTOP_DATA}'"
fi

if [ -d "${DTOP_DATA}" ]; then
    echo "The directory ${DTOP_DATA} exists."
else
    mkdir -p "${DTOP_DATA}"
    echo "The directory '${DTOP_DATA}' for sharing and keeping modules' databases and data has been created."
fi

export DTOP_DATA="${DTOP_DATA}"

s=$(echo "ZHRvY2VhbnBsdXNfaW5zdDpaOXhfc2RfWWZ5TGRza1pMc3llNw==" | base64 --decode)
IFS=':'
GL=($s)
IFS=' '

# ------------------------
# Set whiptail variables
# ------------------------

export PATH=cygwin64/bin:$PATH

export NEWT_COLORS='
    root=white,blue
    border=black,lightgray
    window=lightgray,lightgray
    shadow=black,gray
    title=black,green
    button=black,cyan
    actbutton=white,cyan
    compactbutton=black,lightgray
    checkbox=black,lightgray
    actcheckbox=lightgray,cyan
    entry=black,cyan
    disentry=gray,lightgray
    label=black,lightgray
    listbox=black,lightgray
    actlistbox=black,cyan
    sellistbox=lightgray,black
    actsellistbox=lightgray,black
    textbox=black,lightgray
    acttextbox=black,cyan
    emptyscale=,gray
    fullscale=,cyan
    helpline=white,black
    roottext=lightgrey,black
'

# -------------------------------------
# Step 1/5 - Licence Acceptance
# Functions and variables
# -------------------------------------

# Set the dialog boxes titles
STEP_1_TITLE_1=" DTOceanPlus (DTOP) Project License "
STEP_1_TITLE_2=" Step 1/5 -${STEP_1_TITLE_1}Acceptance "

# Set the location of the license file
LICFILE="${INST_ROOT}/LICENSE.txt"

function step_1 {

    echo "${STEP_1_TITLE_2}"

    if (whiptail --title "${STEP_1_TITLE_1}" --textbox "${LICFILE}" --scrolltext 26 84 --ok-button " Proceed to confirm " --fullbuttons); then
        if(whiptail --title "${STEP_1_TITLE_2}" --yesno \
         "\n\n\n             Please confirm that you have fully read, understand, \n\n               and accept ${STEP_1_TITLE_1}." \
        26 84 --yes-button Accept --no-button Reject --fullbuttons); then
            echo ""
            echo " DTOP License is Accepted"
        else
            echo " DTOP License is Rejected"
            exit 1
        fi
    else
        echo " Rejected"
        exit 1
    fi

    echo ""
}


# ----------------------------------------------------------------------
# Step 2/5 - Selection of the DTOP modules to be installed and deployed
# Functions and variables
# ----------------------------------------------------------------------

ALL_MODULES_NICKNAMES='"sc" "mc" "ec" "et" "ed" "sk" "lmo" "spey" "rams" "slc" "esa" "si" "sg" "cm" "mm"'

# Set the dialog boxes titles
STEP_2_TITLE_1=" Step 2/5 - Selection of DTOP modules "
STEP_2_TITLE_2=" \n   Please select the modules to be installed and deployed "

MODULES=""
MODULES_NICKNAMES=""

function step_2 {

    echo "${STEP_2_TITLE_1}"

    MODULES=$(whiptail --title "${STEP_2_TITLE_1}" --checklist "${STEP_2_TITLE_2}" 26 75 16 --fullbuttons \
        "SC" "Site Characterisation" OFF \
        "MC" "Machine Characterization" OFF \
        "EC" "Energy Capture" OFF \
        "ET" "Energy Transformation" OFF \
        "ED" "Energy Delivery" OFF \
        "SK" "Station Keeping" OFF \
        "LMO" "Logistics and Marine Operations" OFF \
        "SPEY" "System Performance and Energy Yield" OFF \
        "RAMS" "Reliability Availability Maintainability Survivability" OFF \
        "SLC" "System Lifetime Costs" OFF \
        "ESA" "Environmental and Social Acceptance" OFF \
        "SI" "Structured Innovation" OFF \
        "SG" "Stage Gate" OFF \
        "CM" "Catalogs Module" OFF \
        "MM" "Main Module" OFF \
        "ALL" "all modules" ON \
         3>&1 1>&2 2>&3)

    exitstatus=$?

    if [ $exitstatus = 0 ]; then
        MODULES_NICKNAMES=$(echo "${MODULES}" | tr '[:upper:]' '[:lower:]')
        echo " The selected modules nicknames: ${MODULES_NICKNAMES}"

    allflag=0

    if [ -z "${MODULES_NICKNAMES}" ]; then
        echo " No modules were selected (you hit Cancel or unselected all options)"
    else
        for MODULE in ${MODULES_NICKNAMES}; do
            if [ "${MODULE}" = \"all\" ]; then
                echo " ALL is selected !"
                allflag=1
            fi
        done
    fi

    if [ "${allflag}" = "1" ]; then
        MODULES_NICKNAMES=${ALL_MODULES_NICKNAMES}
        MODULES=$(echo "${MODULES_NICKNAMES}" | tr '[:lower:]' '[:upper:]')
        echo " The modules nicknames taking into account that \"ALL\" is selected : ${MODULES_NICKNAMES}"

        echo " MODULES_NICKNAMES = ${MODULES_NICKNAMES}"
    fi

    echo "${MODULES_NICKNAMES}" > "${INST_ROOT}/latest_selected_modules.txt"

        echo " You chose Ok"
    else
        echo " You chose Cancel"
        exit 1
    fi

    echo ""
}


# --------------------------------------------------------
# Step 3/5 - DTOP Modules' Docker Images Pulling
# Functions and variables
# --------------------------------------------------------

export DOCKER_CLI_EXPERIMENTAL=enabled

# Set the dialog boxes titles
STEP_3_TITLE_1=" Step 3/5 - DTOP Modules' Docker Images Downloading "


pull_module_images() {

  echo ${GL[1]} | docker login -u ${GL[0]} --password-stdin ${!CI_REGISTRY} > /dev/null 2>&1

  docker pull ${!CI_REGISTRY}/${MODULE_NICKNAME}_backend:${!MODULE_TAG}

  docker pull ${!CI_REGISTRY}/${MODULE_NICKNAME}_frontend:${!MODULE_TAG}

  docker logout ${!CI_REGISTRY} > /dev/null 2>&1

}


function step_3 {

  echo "${STEP_3_TITLE_1}"

  not_accessible_modules=""

  for MODULE in ${MODULES}; do

    MODULE=$(echo "${MODULE}" | tr -d '"')

    echo ""
    echo " MODULE = ${MODULE}"

    MODULE_IMAGE_TAG=${MODULE}_IMAGE_TAG

    MODULE_NICKNAME=$(echo "${MODULE}" | tr '[:upper:]' '[:lower:]' | tr -d '"')

    CI_REGISTRY=${MODULE}_CI_REGISTRY
    MODULE_TAG=${MODULE}_IMAGE_TAG

    echo " Module BE GitLab production Image = ${!CI_REGISTRY}/${MODULE_NICKNAME}_backend:${!MODULE_TAG}"
    echo " Module FE GitLab production Image = ${!CI_REGISTRY}/${MODULE_NICKNAME}_frontend:${!MODULE_TAG}"

    echo ${GL[1]} | docker login -u ${GL[0]} --password-stdin ${!CI_REGISTRY} > /dev/null 2>&1
    docker manifest inspect ${!CI_REGISTRY}/${MODULE_NICKNAME}_frontend:${!MODULE_TAG} > /dev/null 2>&1

    if [ $? -eq 1 ]; then
      echo " Image ${!CI_REGISTRY}/${MODULE_NICKNAME}_frontend:${!MODULE_TAG} is not accessible ";
      echo " The module ${MODULE_NICKNAME} is skipped ";
      not_accessible_modules="${not_accessible_modules} ${MODULE_NICKNAME}"
      continue
    else
      echo " The module ${MODULE_NICKNAME} is accessible ";
    fi
    docker logout ${!CI_REGISTRY} > /dev/null 2>&1

    STEP_3_TITLE_2="\n       ${MODULE_NICKNAME} ${!MODULE_TAG}  -  please wait while pulling ... "

    {
      i=1
      while read -r line; do
        i=$(( $i + 1 ))
        echo $i
      done < <(pull_module_images)
    } | whiptail --title "${STEP_3_TITLE_1}" --gauge "${STEP_3_TITLE_2}" 8 60 0

  done

  echo "${not_accessible_modules}" > "${INST_ROOT}/latest_not_accessible_modules.txt"
  cur_images=$(docker images --format "table {{.CreatedAt}}\t{{.Repository}}\t{{.Tag}}\t{{.Size}}")

  echo "${cur_images}" > "${INST_ROOT}/latest_modules_images.txt"
  echo ""
  echo "${cur_images}"

  cur_modules_images="$(echo "${cur_images}" | sed 's/^/  /')"

  STEP_3_TITLE_3="\n\n                                       The List of Modules' Docker Images pulled from GilLab DTOP registries :
                  \n\n${cur_modules_images}"

  if (whiptail --title "${STEP_3_TITLE_1}" --scrolltext --yes-button "Continue" --no-button "Cancel" --yesno "${STEP_3_TITLE_3}" 32 156 --fullbuttons)  then
     echo " You chose Continue"
  else
     echo " You chose Cancel"
     exit 1
  fi

  echo ""

}


# --------------------------------------------------
# Step 4/5 - DTOP Modules' Docker Stacks Deployment
# Functions and variables
# --------------------------------------------------

# Set the dialog boxes titles
STEP_4_TITLE_1=" Step 4/5 - DTOP Modules' Docker Stacks Deployment "

check_swarm_mode() {
  echo ""
  if docker node ls > /dev/null 2>&1; then
    echo " The node is already in a swarm mode"
  else
    echo " The node is a in standalone mode"
    docker swarm init
  fi
}

check_traefik_network() {
  echo ""
  docker network inspect traefik-public > /dev/null 2>&1

  if [ $? -eq 1 ]; then
    echo " Creating Docker network - \"traefik-public\" ";
    docker network create --driver overlay --attachable traefik-public
  else
    echo " Docker network \"traefik-public\" already exists";
  fi
}

check_volume() {
  echo ""

  docker volume inspect $1 > /dev/null 2>&1

  if [ $? -eq 1 ]; then
    echo " Docker volume \"$1\" - is creating for database data storage ... ";
    docker volume create $1
  else
    echo " Docker volume \"$1\" already exists - is not being created ";
  fi
}

basic_stack_deploy() {

  echo ""

  docker stack ps tra > /dev/null 2>&1

  if [ $? -eq 1 ]; then
    docker stack deploy --compose-file ./stacks/tra_stack.yml tra --with-registry-auth --prune
  else
    echo " The service \"traefik\" already exists";
  fi

  echo ""

  mkdir -p "${DTOP_DATA}/portainer"

  docker stack ps portainer > /dev/null 2>&1

  if [ $? -eq 1 ]; then
    env DTOP_DATA=$(echo "${DTOP_DATA}" | sed 's:^/cygdrive:/host_mnt:') docker stack deploy --compose-file ./stacks/portainer_stack.yml portainer --with-registry-auth --prune
  else
    echo " The service \"portainer\" already exists";
  fi

}

dtop_data_prepare () {

  mkdir -p "${DTOP_DATA}/${MODULE_NICKNAME}/dumps"
  cp -a "${INST_ROOT}/init_data/${MODULE_NICKNAME}/dumps/." "${DTOP_DATA}/${MODULE_NICKNAME}/dumps/"

  if [[ "${MODULE_NICKNAME}" = "sc" || "${MODULE_NICKNAME}" = "sk" ||  "${MODULE_NICKNAME}" = "esa" ]]; then
    mkdir -p "${DTOP_DATA}/${MODULE_NICKNAME}/storage"
  else
    mkdir -p "${DTOP_DATA}/${MODULE_NICKNAME}/db"
  fi

  if [ "${MODULE_NICKNAME}" = "sc" ]; then
    mkdir -p "${DTOP_DATA}/sc/databases"
    if [ -f "${DTOP_DATA}/sc/databases/SiteCharacterisation_Main-Database/readme.md" ]
    then
      echo " The basic data files of DTOP SC module seem to be already installed into ${DTOP_DATA}/sc/databases and can be used"
    else
      if [ ! -f "${INST_ROOT}/${SC_databases_archive_version}" ]
      then
        echo " It seems that the archive '${SC_databases_archive_version}' of the basic data files for DTOP SC module was not downloaded in advance."
        echo " It is not found in ${INST_ROOT} and therefore cannot be installed into ${DTOP_DATA}/sc/databases"
      else
        echo " The DTOP SC module data files archive '${SC_databases_archive_version}' is found in ${INST_ROOT}"
        echo " Installing the archive into ${DTOP_DATA}/sc/databases ..."
        tar -pxzf "${INST_ROOT}/${SC_databases_archive_version}" -C "${DTOP_DATA}/sc"
      fi
    fi
  fi

  if [ "${MODULE_NICKNAME}" = "mm" ]; then
    mkdir -p "${DTOP_DATA}/${MODULE_NICKNAME}/scripts"

    cp -a "${INST_ROOT}/init_data/${MODULE_NICKNAME}/scripts/." "${DTOP_DATA}/${MODULE_NICKNAME}/scripts/"

    check_volume mm_mysql_data
  fi

  if [ "${MODULE_NICKNAME}" = "cm" ]; then
    mkdir -p "${DTOP_DATA}/${MODULE_NICKNAME}/scripts"
    mkdir -p "${DTOP_DATA}/${MODULE_NICKNAME}/db"

    cp -a "${INST_ROOT}/init_data/${MODULE_NICKNAME}/scripts/." "${DTOP_DATA}/${MODULE_NICKNAME}/scripts/"

    check_volume cm_pg_data
  fi

}

module_stack_deploy() {

  echo ${GL[1]} | docker login -u ${GL[0]} --password-stdin ${!CI_REGISTRY} > /dev/null 2>&1

  env DTOP_DATA=$(echo "${DTOP_DATA}" | sed 's:^/cygdrive:/host_mnt:') docker stack deploy --compose-file ./stacks/${MODULE_NICKNAME}_stack.yml ${MODULE_NICKNAME} --with-registry-auth --prune

  docker logout ${!CI_REGISTRY} > /dev/null 2>&1

}

function step_4 {

  echo "${STEP_4_TITLE_1}"

  check_swarm_mode

  check_traefik_network

  basic_stack_deploy

  MODULES=$(echo "${MODULES_NICKNAMES}" | tr '[:lower:]' '[:upper:]')

  for MODULE in ${MODULES}; do

    MODULE=$(echo "${MODULE}" | tr -d '"')

    MODULE_NICKNAME=$(echo "${MODULE}" | tr '[:upper:]' '[:lower:]' | tr -d '"')

    if [[ ${not_accessible_modules} == *${MODULE_NICKNAME}* ]]; then
      echo ""
      echo "The module ${MODULE} is not accesible."
      continue
    fi

    CI_REGISTRY=${MODULE}_CI_REGISTRY

    MODULE_TAG=${MODULE}_IMAGE_TAG

    echo ""
    echo " Deploying MODULE = ${MODULE}"

    dtop_data_prepare

    STEP_4_TITLE_2="\n       ${MODULE_NICKNAME} ${!MODULE_TAG}  -  please wait while deploying ... "

    {
      i=1
      while read -r line; do
        i=$(( $i + 1 ))
        echo $i
      done < <(module_stack_deploy)
    } | whiptail --title "${STEP_4_TITLE_1}" --gauge "${STEP_4_TITLE_2}" 8 60 0

  done

  sleep 10

  cur_services=$(docker service ls --format "table {{.Image}}\t{{.Name}}\t{{.Replicas}}")
  echo "${cur_services}" > "${INST_ROOT}/latest_modules_services.txt"
  echo ""
  echo "${cur_services}"

  cur_modules_services="$(echo "${cur_services}" | sed 's/^/         /')"

  STEP_4_TITLE_3="\n\n                             The List of Docker Modules' Services :
                  \n\n${cur_modules_services}"

  if (whiptail --title "${STEP_4_TITLE_1}" --scrolltext --yes-button "Continue" --no-button "Cancel" --yesno "${STEP_4_TITLE_3}" 32 156 --fullbuttons)  then
     echo " You chose Continue"
  else
     echo " You chose Cancel"
     exit 1
  fi

  echo ""

}


# --------------------------------------------------------
# Step 5/5 - DTOP Modules' Running in browser
# Functions and variables
# --------------------------------------------------------

# Set the dialog boxes titles
STEP_5_TITLE_1=" Step 5/5 - Checking deployed infrastructure and DTOP Running "
STEP_5_TITLE_2="\n            To check deployment you can use Portainer web service and GUI
                \n              URLs to open Portainer service GUI in the browser :
                \n                - http://portainer.dtop.localhost
                \n                - http://portainer.dtop.<company domain name>
                \n
                \n            To run DTOP Modules in the browser use the correspondent URLs :
                \n                - http://<module nickname>.dtop.localhost
                \n                - http://<module nickname>.dtop.<company domain name>
                \n              For example, to open Main Module (mm) :
                \n                - http://mm.dtop.localhost
                \n                - http://mm.dtop.<company domain name>
                \n              Admin credentials - name: admin@dtop.com and pass: adminadmin"
function step_5 {

  echo "${STEP_5_TITLE_1}"

  if (whiptail --msgbox --ok-button "  Finish  " --title "${STEP_5_TITLE_1}" "${STEP_5_TITLE_2}" 32 84 --fullbuttons) then
     echo " You chose Finish"
  fi

  echo ""
}
